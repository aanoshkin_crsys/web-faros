//Поиск объектов
$('body').on('change', '#find', function(){
    var
        aText = $(this).val(),
        keyGroup = 0;
    $('#objList tr').addClass('invisible-table-row');
    $('#objList tr:icontains("'+aText+'")').removeClass('invisible-table-row');

    if (aText == '') {
        $('#objList .row-group').each(function (index, element) {
            keyGroup = $(element).data('key');
            if ($(element).hasClass('closed')) {
                $("#objList .row-item[data-group='" + keyGroup + "']").addClass('invisible-table-row');
            }else{
                $("#objList .row-item[data-group='" + keyGroup + "']").removeClass('invisible-table-row');
            }
        });
    }
});

$('#groupNew').click(function(){
    $.post('/obj/ajax_newGroup', {}, function(data){
        if (data != ''){
            showError('Создание группы', data);
        }else {
            location.reload();
        }
    })
});

//Сворачивание/разворачивание групп у объектов
$('#objList').on('click', '.row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.expand.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.expand.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#objList .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//Сворачивание/разворачивание групп у свойств
$('#itemDetails').on('click', '#objProps .row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#objProps .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//Нажатие на строку таблицы
$('#objList .row-item').click(function(event){
    //Получаем ключ
    var
        key = $(this).data('key');
    if (!event.ctrlKey){
        $('#objList .row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }
    //Переклчаем чекбокс
    if ($(this).find('.row-checkbox').hasClass('fa-square-o')){
        $(this).find('.row-checkbox').removeClass('fa-square-o').addClass('fa-check-square-o').closest('tr').addClass('active');
    }else{
        $(this).find('.row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }

    //Устанавливаем наименование
    $('#itemName').html($(this).find('.displayName').html());
    $('#itemProperty').data('key', key);
    //Отображем детальную информацию
    $.post('/obj/ajax_details', {keyItem: key}, function(data){
        $('#itemDetails').html(data);
    });

    return false;
})

//Нажатие на редактирование группы
$('#objList').on('click', '.changeGroup', function(){
    //Получаем ключ
    var
        key = $(this).closest('tr').data('key');
    //Устанавливаем наименование
    $('#itemName').html($(this).closest('tr').find('.displayName').html());
    $('#itemProperty').data('key', key);
    //Отображем детальную информацию по группе
    $.post('/obj/ajax_detailsGroup', {keyItem: key}, function(data){
        $('#itemDetails').html(data);
    });

    return false;
})

//Нажатие на редактирование группы
$('#objList').on('click', '.moveUp', function(){
    //Получаем ключ
    var
        key = $(this).closest('tr').data('key');
    $.post('/obj/ajax_moveup', {keyItem: key}, function(){
        location.reload();
    })

    return false;
})

//Нажатие на редактирование группы
$('#objList').on('click', '.moveDown    ', function(){
    //Получаем ключ
    var
        key = $(this).closest('tr').data('key');
    $.post('/obj/ajax_movedown', {keyItem: key}, function(){
        location.reload();
    })

    return false;
})

//Нажатие на удаление группы
$('#objList').on('click', '.removeGroup', function(){
    //Получаем ключ
    var
        key = $(this).closest('tr').data('key');
    $.post('/obj/ajax_removeGroup', {keyItem: key}, function(data){
        if (data != ''){
            showError('Удаление группы', data);
        }else{
            location.reload();
        }
    })

    return false;
})

//Поиск в низпадающем пункте Переместить
//Поиск объектов
$('body').on('change', '#groupFind', function(){
    var
        aText = $(this).val();
    $('#objMove a').addClass('hidden');
    $('#objMove a:icontains("'+aText+'")').removeClass('hidden');
});

//Нажатие на пункт кнопки "Переместить"
$('#objMove').on('click', 'a', function(){
    var
        newId = $(this).data('key'),
        keys = '-1';

    $('#objList .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/obj/ajax_changeGroup', {keyItems: keys, keyGroup: newId}, function(data){
        if (data != ''){
            showError('Перемещение', data);
        }else{
            location.reload();
        }
    })
})

//Нажатие на пункт кнопки "Удалить"
$('#objDelete').click(function(){
    var
        keys = '-1';

    $('#objList .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/obj/ajax_removeObjects', {keyItems: keys}, function(data){
        if (data != ''){
            showError('Удаление', data);
        }else{
            location.reload();
        }
    })
})

//Поиск пользователей
$('body').on('change', '#controlFind', function(){
    var
        aText = $(this).val();
    $('#objControl a').addClass('hidden');
    $('#objControl a:icontains("'+aText+'")').removeClass('hidden');
});

//Нажатие на пункт кнопки Контролирующий
$('#controlFind').on('click', 'a', function(){
    var
        newId = $(this).data('key'),
        keys = '-1';

    $('#controlFind .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/obj/ajax_changeController', {idObjs: keys, newId: newId}, function(data){
        if (data != ''){
            showError('Установка контроля', data);
        }else{
            location.reload();
        }
    })
})

//При изменении поля
$('#itemProperty').on('change', '.editeble', function(){
    var
        keyItem    = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this),
        aName      = '';
    $.post('/obj/ajax_update', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование объекта', data);
            $(aItem).val(oldValue);
        }else{
            aName = $('#objName').val();
            $('#itemName').html(aName);
            $('#objList .row-item[data-key='+keyItem+'] .displayName').html(aName);
            $('#objList .row-item[data-key='+keyItem+'] .displayComment').html($('#objComment').val());
        }
    })
})

//При изменении поля
$('#itemProperty').on('change', '.editebleGroup', function(){
    var
        keyItem    = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this),
        aName      = '';
    $.post('/obj/ajax_updateGroup', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование объекта', data);
            $(aItem).val(oldValue);
        }else{
            aName = $('#groupName').val();
            if ($('#groupControl').val() != -1){
                aName = aName + ' (Контроль: '+$('#groupControl>option:selected').html()+')';
            }
            $('#itemName').html(aName);
            $('#objList .row-group[data-key='+keyItem+'] .displayName').html(aName);
        }
    })
})
