//Выбор роли
$('#roleList').on('click', 'tr', function(){
    var
        id = $(this).data('key'),
        aItem = $(this);
    $('#roleList tr.active').removeClass('active');
    $(aItem).addClass('active');
    $('#itemName').html($(aItem).find('td').html());
    $.post('/roles/ajax_details', {keyItem: id}, function(data){
        $('#itemDetails').html(data);
    })

})

//Поиск ролей
$('body').on('change', '#find', function(){
    var
        aText = $(this).val();
    $('#roleList tr').addClass('invisible-table-row');
    $('#roleList tr:icontains("'+aText+'")').removeClass('invisible-table-row');
});

//Включение / выключение ролей
$('#itemDetails').on('click', '.btn', function(){
    var
        aItem = $(this),
        isOn = !$(this).hasClass('btn-success'),
        idRole = $('#roleList tr.active').data('key'),
        idRight = $(this).closest('tr').data('key');
    $.post('/roles/ajax_setEnabled', {keyItem: idRole, keyRight: idRight, isOn: isOn}, function(data){
        if (data == ''){
            if (isOn == true){
                $(aItem).removeClass('btn-outline-dark').addClass('btn-success').html('Вкл');
            }else{
                $(aItem).removeClass('btn-success').addClass('btn-outline-dark').html('Выкл');
            }
        }else{
            showError('Установка прав роли', data);
        }
    })
})
