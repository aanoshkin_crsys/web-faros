/**
 * Created by Dima on 10.12.2016.
 */

jQuery.expr[':'].icontains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

var prfx = ["webkit", "moz", "ms", "o", ""]; 
function RunPrefixMethod(obj, method) { 
var p = 0, m, t; 
while (p < prfx.length && !obj[m]) { 
m = method; 
if (prfx[p] == "") { 
m = m.substr(0,1).toLowerCase() + m.substr(1); 
} 
m = prfx[p] + m; 
t = typeof obj[m]; 
if (t != "undefined") { 
prfx = [prfx[p]]; 
return (t == "function" ? obj[m]() : obj[m]); 
} 
p++; 
} 
} 

$('#fullscreen').on('click', function(){
    var 
        elem = $('body'); 
    if (RunPrefixMethod(document, "FullScreen") || RunPrefixMethod(document, "IsFullScreen")) { 
        RunPrefixMethod(document, "CancelFullScreen"); 
    } else { 
        RunPrefixMethod(document.documentElement, "RequestFullScreen"); 
    } 
});

/******************************************Окно ожидания****************************************************************
 /**
 *  Фунция отображения окна ожидания
 */

function showWaiting(caption, text){
    $('#myWaitLabel').html(caption);
    $('#myWaitBody').html(text);
    $('#myWaitWindow').modal({keyboard: false});
}

function changeWaiting(text){
    $('#myWaitBody').html(text);
}

/**
 * Функция скрытия окна ожидания
 */
function hideWaiting(){
    $('#myWaitWindow').modal('hide');
}

/******************************************Окно ошибки******************************************************************
 */

function showDialog(caption, text){
    $('#myModalLabel').html(caption);
    $('#myModalBody').html(text);
    $('#myModalWindow').modal();
}
/**
 * Отобразить окно ошибки
 * @param caption - Заголовок ошибки
 * @param text - расшифровка ошибки
 */
function showError(caption, text){
    $('#myModalWindow .modal-header').removeClass('text-success').addClass('text-danger');
    $('#myModalWindow .modal-footer .btn').removeClass('btn-success').addClass('btn-danger');
    showDialog(caption, text);
}

function showSuccess(caption, text){
    $('#myModalWindow .modal-header').removeClass('text-danger').addClass('text-success');
    $('#myModalWindow .modal-footer .btn').removeClass('btn-danger').addClass('btn-success');
    showDialog(caption, text);
}

/***********************************************************************************************************************
 * Функция динамического считывания CSS стиля
 * @param url
 */
jQuery.loadCSS = function(url) {
    if (!$('link[href="/assets/css/' + url + '"]').length)
        $('head').append('<link rel="stylesheet" type="text/css" href="/assets/css/' + url + '">');
}

