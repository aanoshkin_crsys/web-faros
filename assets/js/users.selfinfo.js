$('body').on('click', '.change-avatar', function(){
    $('#select-avatar').trigger('click');
})

$(document).ready(function(){
    $('#fileupload').fileupload({
        type:'post',
        dataType: 'json',
        done: function (e, data) {
            if(data.result.error != undefined){
                showError('Загрузка аватара', data.result.error);
            }else{
                location.reload();
            }
        }
    })
});