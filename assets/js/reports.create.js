//Первоначальная инициализация
var
    dateRangeStart = moment().subtract(1, 'years'),
    dateRangeFinish = moment();

//При загрузке страницы
$(function(){
    //Считываем значения фильтра
    $('#destUser').select2({
        placeholder: "-Нет-",
        dropdownAutoWidth: true,
    });
    $('#objectList').select2({
        placeholder: "-Нет-",
        dropdownAutoWidth: true,
    });
    $('#reportType').select2({
        placeholder: "-Нет-",
        dropdownAutoWidth: true,
        closeOnSelect: false,
    });

    $('#range').daterangepicker({
            autoApply: true,
            startDate: dateRangeStart,
            endDate: dateRangeFinish,
            buttonClasses: 'btn btn-primary',
            cancelClass: 'btn-dark',
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Текущий месяц': [moment().startOf('month'), moment().endOf('month')],
                'Предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Последний год': [moment().subtract(1, 'year'), moment()]
            },
            locale: {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Принять",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Выборочно",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Новябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            },
        }, function (start, end) {
            dateRangeStart = start;
            dateRangeFinish = end;
            $('#range').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        }
    )

    $('#objectList').trigger('change');
})

$('#objectList').change(function(){
    var
        idObject = $('#objectList').val();
    $.post('/reports/ajax_fillDest', {idObject: idObject}, function(data){
        $('#destUser').html(data);
    })
})

$('#create').click(function(){
    var
        //Получим значения по фильтру
        idObject    = $('#objectList').val(),
        nameObject  = $('#objectList option:selected').html(),
        idDest      = $('#destUser').val(),
        linkReports = $('#reportType').val(),
        message     = $('#reportMessage').val(),
        aRangeStart = '',
        aRangeFinish= '',
        aCurrentDate =moment().format('YYYYMMDD'),
        aCurrentMoment=moment().format('DDMMYYYY_HHMMss'),
        idReport,
        reportFile,
        reportFiles =[],
        reportName,
        reportNames = [],
        total = 0,
        completed   = 0;

    //Если период установлен, то получаем значения
    if ($('#range').val() != '-Нет-'){
        aRangeStart = dateRangeStart.format('DD.MM.YYYY');
        aRangeFinish= dateRangeFinish.format('DD.MM.YYYY');
    }else{
        //В противном случае передаются пустые значения
        aRangeStart = '';
        aRangeFinish= '';
    }

    completed = linkReports.length;
    total = completed;
    showWaiting('Формирование отчета(ов)', 'Осталось сформировать <b>' + completed + '</b> отчет(а,ов).');
    for (var idx = 0; idx < linkReports.length; idx ++) {
        idReport = $('#reportType option[value="'+linkReports[idx]+'"]:selected').data('key');
        reportName = $('#reportType option[value="'+linkReports[idx]+'"]:selected').html();
        reportFile = idReport + '_' + idObject + '_' + aRangeStart + '-' + aRangeFinish + '_' + aCurrentMoment + '.pdf';
        reportFiles.push(aCurrentDate + '/' + reportFile);
        reportNames.push(reportName);
        $.post(linkReports[idx], {
            pathname: 'REPORTS/' + aCurrentDate + '/',
            filename: reportFile,
            idObject: idObject,
            nameObject: nameObject,
            idDest: idDest,
            message: message,
            dateStart: aRangeStart,
            dateFinish: aRangeFinish
        }, function (data) {
            completed --;
            if (completed == 0){
                changeWaiting('Сохранение и отправка отчетов.');
                $.post('/reports/addReport', {idObject: idObject, idDest: idDest, theme: ''+total+' отчет(а,ов) за период '+$('#range').val(), message: message, files: reportFiles, displays: reportNames}, function(data){
                    hideWaiting();
                    if (data != ''){
                        showError("Отправка отчета", data);
                    }else{
                        showSuccess('Формирование отчета(ов)', 'Отчет успешно сформирован');
                    }
                })

            }else{
                changeWaiting('Осталось сформировать <b>' + completed + '</b> отчет(а,ов).');
            }
        })
    }
})