//Функция загрузки списка общения
function loadCommunity(idItem){
    var
        access = $('#'+idItem).data('access'),
        tableName = $('#'+idItem).data('table-name'),
        key = $('#'+idItem).data('key');

    $.post('/common/ajax_getCommunityList', {access:access, tableName: tableName, keyItem: key}, function(data){
        $('#'+idItem).html(data);

        $('#community-send').bind('click', function(){
            var
                sendText = $('#community-text').val();
            if (!sendText){
                showError('Отправка сообщения', 'Нельзя отправить пустое сообщение');
            }else{
                $.post('/common/ajax_sendCommunityText', {tableName: tableName, keyItem: key, sendText: sendText}, function(dataSend){
                    if (dataSend != ''){
                        showError('Отправка сообщения', dataSend);
                    }else {
                        loadCommunity(idItem);
                    }
                });
            }
        })
        $('#community-text').keydown(function (e) {
            if (e.ctrlKey && e.keyCode == 13) {
                $('#community-send').trigger('click');
            }
        });
    });
}
