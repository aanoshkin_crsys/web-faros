$('#btnRestore').click(function(){
    var
        login = $('#login').val();
    if (login == ''){
        showError('Восстановление пароля', 'Необходимо указать логин (email).');
    }else{
        $.post('/auth/ajax_restore', {login: login}, function(data){
            if (data != ''){
                showError('Восстановление пароля', data);
            } else {
                location.href = '/auth/change_password?login='+login;
            }
        })
    }
});