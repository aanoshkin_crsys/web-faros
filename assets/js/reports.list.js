//Первоначальная инициализация
var
    dateRangeStart = moment().subtract(1, 'years'),
    dateRangeFinish = moment();

function doRefreshRow(keyItem){
    $.post('/reports/ajax_rowRefresh', {keyItem: keyItem}, function(data){
        $("#reportList .row-item[data-key='"+keyItem+"']").replaceWith(data);
    })
}

//При загрузке страницы
$(function(){
    //Считываем значения фильтра
    $.post('/reports/ajax_getFilter', {}, function(data){
        $('#filter-panel').html(data);
        $('#authorFilter').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
        });
        $('#destFilter').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
        });
        $('#objectFilter').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
        });
        $('#do-filter').trigger("click");
    })
})

//При нажатии на кнопку мши на период, отккрываем календарь
$('body').on('mousedown', '#range', function() {
    $('#range').daterangepicker({
            autoApply: true,
            startDate: dateRangeStart,
            endDate: dateRangeFinish,
            buttonClasses: 'btn btn-primary',
            cancelClass: 'btn-dark',
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Текущий месяц': [moment().startOf('month'), moment().endOf('month')],
                'Предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Последний год': [moment().subtract(1, 'year'), moment()]
            },
            locale: {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Принять",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Выборочно",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Новябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            },
        }, function (start, end) {
            dateRangeStart = start;
            dateRangeFinish = end;
            $('#range').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        }
    )
})

//При нажатии на кнопку отмены в выборе периода
$('body').on('#range cancel.daterangepicker', function(ev, picker) {
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
});

//При нажатии на кнопку "Очистить период"
$('body').on('click', '#range-clear', function(){
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
})

//При нажатии на кнопку "Очистить фильтр"
$('body').on('click', '#do-clear-filter', function (){
    //Очищаем значения
    $('#authorFilter').val(null).trigger("change");
    $('#destFilter').val(null).trigger("change");
    $('#objectFilter').val(null).trigger("change");
    $('#acceptFilter').val(-1);
    $('#communityFilter').val(-1);
    $('#textFilter').val('');
    //Нажимаем кнопку "Очистить период"
    $('#range-clear').trigger("click");
    //Нажимаем кнопку "Фильтровать"
    $('#do-filter').trigger("click");
})

//При нажатии на кнопку "Фильтровать"
$('body').on('click', '#do-filter', function (){
    var
        //Получим значения по фильтру
        authorFilter    = $('#authorFilter').val().join(","),
        destFilter      = $('#destFilter').val().join(","),
        objectFilter    = $('#objectFilter').val().join(","),
        acceptFilter    = $('#acceptFilter').val(),
        communityFilter = $('#communityFilter').val(),
        textFilter      = $('#textFilter').val(),
        aRangeStart = '',
        aRangeFinish= '';

    //Если период установлен, то получаем значения
    if ($('#range').val() != '-Нет-'){
        aRangeStart = dateRangeStart.format('DD.MM.YYYY');
        aRangeFinish= dateRangeFinish.format('DD.MM.YYYY');
    }else{
        //В противном случае передаются пустые значения
        aRangeStart = '';
        aRangeFinish= '';
    }
    //Получаем список событий по переданным параметрам
    $.post('/reports/ajax_getList', {authorFilter: authorFilter, destFilter: destFilter, objectFilter: objectFilter, acceptFilter: acceptFilter, communityFilter: communityFilter, textFilter: textFilter, dateStart: aRangeStart, dateFinish: aRangeFinish}, function(data){
        $('#reportList').html(data);
    })
})

//При нажатии на строку
$('#reportList').on('click', '.row-item', function(){
    //Получаем ключ
    var
        key = $(this).data('key');

    if (!event.ctrlKey){
        $('#reportList .row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }
    //Переклчаем чекбокс
    if ($(this).find('.row-checkbox').hasClass('fa-square-o')){
        $(this).find('.row-checkbox').removeClass('fa-square-o').addClass('fa-check-square-o').closest('tr').addClass('active');
    }else{
        $(this).find('.row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }

    //Устанавливаем наименование
    $('#itemName').html($(this).data('view'));
    $('#itemProperty').data('key', key);
    //Отображем детальную информацию
    $.post('/reports/ajax_details', {keyItem: key}, function(data){
        $('#itemDetails').html(data);

        $('#destUser').select2();
        $('#objectList').select2();

        loadCommunity('community');
        loadAttaches('attachFiles');

        $('#community-tab').bind('click', function(){
            $("#reportList .row-item[data-key='"+key+"'] .fa-comment").removeClass('fa-comment').addClass('fa-comment-o');
        })
    });

    return false;
})

//Редактирование полей
$('#itemProperty').on('change', '.editeble', function () {
    var
        keyItem    = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this),
        aName      = '';
    $.post('/reports/ajax_update', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование отчета', data);
            $(aItem).val(oldValue);
        }else{
            doRefreshRow(keyItem);
        }
    })
});

//Нажатие на кнопку "Проверено"
$('#itemDetails').on('click', '#do-accept', function(){
    //Получаем ключ
    var
        key = $('#itemProperty').data('key');
    $.post('/reports/ajax_accept', {keyItem: key}, function(data){
        if (data != ''){
            showError('Подтверждение отчета', data);
        }else{
            $('#acceptInfo').html('Подтверждено').addClass('text-success');
            doRefreshRow(key);
        }
    })
})

//Создание отчета
$('#createReport').click(function() {
    window.open('/reports/createReport', '_self', '');
})

//Удаление отчетов
$('#removeReport').click(function(){
    var
        keys = '-1';

    $('#reportList .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/reports/ajax_remove', {keyItems: keys}, function(data){
        if (data != ''){
            showError('Удаление', data);
        }else{
            $('#reportList .row-item.active').remove();
        }
    })
})