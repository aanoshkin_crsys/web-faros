//Первоначальная инициализация
var
    dateRangeStart = moment().subtract(1, 'year'),
    //dateRangeStart = moment().subtract(1, 'days'),
    dateRangeFinish = moment().subtract(1, 'days');

//При загрузке страницы
$(function(){
    //Считываем значения фильтра
    $.post('/events/ajax_getFilter', {}, function(data){
        $('#filter-panel').html(data);
        $('#eventTypeGroup').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#eventType').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#analitic').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#object').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });

        $('#range').val(dateRangeStart.format('DD.MM.YYYY') + ' - ' + dateRangeFinish.format('DD.MM.YYYY'));

        $('#do-filter').trigger("click");
    })
})

//При нажатии на кнопку "Фильтровать"
$('body').on('click', '#do-filter', function (){
    var
        //Получим значения по фильтру
        idAnalitic = $('#analitic').val().join(","),
        idEvent    = $('#eventType').val().join(","),
        idEventGroups= $('#eventTypeGroup').val().join(","),
        idObject   = $('#object').val().join(","),
        control    = $('#control').val(),
        commFilter = $('#community-filter').val(),
        viewText   = $('#view-text').val(),
        aRangeStart = '',
        aRangeFinish= '';

    //Если период установлен, то получаем значения
    if ($('#range').val() != '-Нет-'){
        aRangeStart = dateRangeStart.format('DD.MM.YYYY');
        aRangeFinish= dateRangeFinish.format('DD.MM.YYYY');
    }else{
        //В противном случае передаются пустые значения
        aRangeStart = '';
        aRangeFinish= '';
    }
    //Получаем список событий по переданным параметрам
    $.post('/events/ajax_getList', {rangeStart: aRangeStart, rangeFinish: aRangeFinish, idAnalitic: idAnalitic, idEvent: idEvent, idEventGroups: idEventGroups, idObject: idObject, control: control, commFilter: commFilter, viewText: viewText}, function(data){
        $('#eventList').html(data);
    })
})

//При нажатии на кнопку "Очистить фильтр"
$('body').on('click', '#do-clear-filter', function (){
    //Очищаем значения
    $('#analitic').val(null).trigger("change");
    $('#eventType').val(null).trigger("change");
    $('#object').val(null).trigger("change");
    $('#control').val(-1);
    $('#community-filter').val(-1);
    $('#view-text').val('');
    //Нажимаем кнопку "Очистить период"
    $('#range-clear').trigger("click");
    //Нажимаем кнопку "Фильтровать"
    $('#do-filter').trigger("click");
})

//Поиск объектов
$('body').on('change', '#find', function(){
    var
        //Получаем значения
        aText = $(this).val(),
        keyGroup = 0;
    //Скрываем все строки
    $('#eventList tr').addClass('invisible-table-row');
    //Для найденных с указанным текстом строк убираем класс скрытия
    $('#eventList tr:icontains("'+aText+'")').removeClass('invisible-table-row');
    //Скрываем дополнительно строки, которые находятся в свернутой группе
    if (aText == '') {
        $('#obeventListjList .row-group').each(function (index, element) {
            keyGroup = $(element).data('key');
            if ($(element).hasClass('closed')) {
                $("#eventList .row-item[data-group='" + keyGroup + "']").addClass('invisible-table-row');
            }else{
                $("#eventList .row-item[data-group='" + keyGroup + "']").removeClass('invisible-table-row');
            }
        });
    }
});

//Сворачивание/разворачивание групп у объектов
$('body').on('click', '#eventList .row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.expand.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.expand.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#eventList .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//При нажатии на кнопку мыши на период, отккрываем календарь
$('body').on('mousedown', '#range', function() {
    $('#range').daterangepicker({
            autoApply: true,
            startDate: dateRangeStart,
            endDate: dateRangeFinish,
            buttonClasses: 'btn btn-primary',
            cancelClass: 'btn-dark',
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Текущий месяц': [moment().startOf('month'), moment().endOf('month')],
                'Предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Последний год': [moment().subtract(1, 'year'), moment()]
            },
            locale: {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Принять",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Выборочно",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Новябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            },
        }, function (start, end) {
            dateRangeStart = start;
            dateRangeFinish = end;
            $('#range').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        }
    )
})

//При нажатии на кнопку отмены в выборе периода
$('body').on('#range cancel.daterangepicker', function(ev, picker) {
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
});

//При нажатии на кнопку "Очистить период"
$('body').on('click', '#range-clear', function(){
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
})

//При нажатии на строку
$('#eventList').on('click', '.row-item', function(){
    //Получаем ключ
    var
        key = $(this).data('key');

    if (!event.ctrlKey){
        $('#eventList .row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }
    //Переклчаем чекбокс
    if ($(this).find('.row-checkbox').hasClass('fa-square-o')){
        $(this).find('.row-checkbox').removeClass('fa-square-o').addClass('fa-check-square-o').closest('tr').addClass('active');
    }else{
        $(this).find('.row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }

    //Устанавливаем наименование
    $('#itemName').html('Событие '+$(this).data('view'));
    $('#itemProperty').data('key', key);
    //Отображем детальную информацию
    $.post('/events/ajax_details', {keyItem: key}, function(data){
        $('#itemDetails').html(data);
        $('#eventTypeDetail').select2({
            dropdownParent: $('body'),
        });
        loadCommunity('community');

        loadAttaches('attach-images', true);

        $('#community-tab').bind('click', function(){
            $("#eventTable .row-item[data-key='"+key+"'] .fa-comment").removeClass('fa-comment').addClass('fa-comment-o');
        })
    });

    return false;
})

//Нажатие на кнопку "Проверено"
$('#itemDetails').on('click', '#do-control', function(){
    //Получаем ключ
    var
        key = $('#itemProperty').data('key');
    $.post('/events/ajax_control', {keyItem: key}, function(data){
        if (data != ''){
            showError('Установка контроля', data);
        }else{
            $.post('/events/ajax_getControlInfo', {toggle: 1}, function(data){
                $('#controlInfo').html(data).addClass('text-success');
            })
            $.post('/events/ajax_getControlTDInfo', {toggle: 1}, function(data){
                $("#eventTable .row-item[data-key='"+key+"'] .control-row").html(data);
            })
        }
    })
})

//Нажатие на кнопку "Отмена проверки"
$('#itemDetails').on('click', '#do-cancel-control', function(){
    //Получаем ключ
    var
        key = $('#itemProperty').data('key');
    $.post('/events/ajax_cancelcontrol', {keyItem: key}, function(data){
        if (data != ''){
            showError('Снятие контроля', data);
        }else{
            $.post('/events/ajax_getControlInfo', {toggle: 0}, function(data){
                $('#controlInfo').html(data).removeClass('text-success');
            })
            $.post('/events/ajax_getControlTDInfo', {toggle: 0}, function(data){
                $("#eventTable .row-item[data-key='"+key+"'] .control-row").html(data);
            })
        }
    })
})

//Изменение значения для замечания
$('#itemProperty').on('change', '#eventMessageControl', function(){
    if ($(this).val() != ''){
        $(this).addClass('is-invalid');
    }else{
        $(this).removeClass('is-invalid');
    }
})
//Редактирование полей
$('#itemProperty').on('change', '.editeble', function () {
    var
        keyItem    = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this),
        aName      = '';
    $.post('/events/ajax_update', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование события', data);
            $(aItem).val(oldValue);
        }else{
            aName = '<b>'+$('#eventTypeDetail option:selected').html()+'</b> '+$('#eventMessage').val();
            $('#main-data .row-item[data-key='+keyItem+'] .displayName').html(aName);
        }
    })
});

//Нажатие на удаление группы
$('#main-data').on('click', '#eventDelete', function(){
    var
        keys = '-1';

    $('#eventTable .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/events/ajax_removeEvents', {keyItems: keys}, function(data){
        if (data != ''){
            showError('Удаление', data);
        }else{
            $('#eventTable .row-item.active').remove();
        }
    })

})

//Нажатие на кнопку "Замечание исправлено"
$('#itemProperty').on('click', '#completed', function(){
    var
        keyItem    = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = 1,
        oldValue   = 0,
        aItem      = $(this);
    $.post('/events/ajax_update', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data == ''){
            $('#eventMessageControl').removeClass('is-invalid').addClass('is-valid');
            $(aItem).closest('.input-group-append').remove();
        }
    })
})