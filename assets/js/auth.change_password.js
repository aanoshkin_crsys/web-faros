$('#btnRestore').click(function(){
   var
       code  = $('#code').val(),
       pass  = $('#pass').val(),
       passc = $('#passconfirm').val();

   if (code == ''){
       showError('Восстановление пароля', 'Необходимо указать код.');
   }else if (pass == ''){
       showError('Восстановление пароля', 'Необходимо указать пароль.');
   }else if (pass != passc){
       showError('Восстановление пароля', 'Пароли должны совпадать.');
   }else{
       $.post('/auth/ajax_changepassword', {code: code, pass: pass}, function(data){
           if (data != ''){
               showError('Восстановление пароля', data);
           }else{
               location.href = '/auth/';
           }
       })
   }
});