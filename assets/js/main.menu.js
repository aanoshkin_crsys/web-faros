//Активирование пункта меню
$('#left-menu').on('click', '.nav-link', function(){
    $(this).closest('#left-menu').find('.active').removeClass('active');
    $(this).addClass('active');
})

//Нажатие на просмотр картинки
$('body').on('click', '.preview', function(){
    var
        aSrc = $(this).attr('src'),
        aComment = $(this).data('comment');
    $('#fullsize').removeClass('invisible');
    $('#fullsize img').attr('src', aSrc);
    $('#fullsize p').html(aComment);
})

//Нажатие на просмотр картинки
$('body').on('click', '.preview-full', function(){
    var
        aSrc = $(this).attr('src'),
        aComment = $(this).data('comment');
    $('#fullsize').removeClass('invisible');
    $('#fullsize img').attr('src', aSrc);
    $('#fullsize p').html(aComment);
})

//Нажатие на ESC
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('#closeFullSize').click();
    }
});

//Нажатие на кнопку закрытия просмотра изображения
$('#closeFullSize').click(function(){
    $('#fullsize').addClass('invisible');
});

//При загрузке автоматически выбирается первый пункт меню
$(function(){
    if ($("#main-data").length == 0) {
        if ($('#left-menu .nav-link.active')){
            $('#left-menu .nav-link.active').click();
        }else{
            $('#left-menu .nav-link').first().click();
        }
    }
    $('#filter-panel').height($('#left-panel').height() - $('#left-menu-panel').height() - 26);
})

$(window).on('resize', function(){
    $('#filter-panel').height($('#left-panel').height() - $('#left-menu-panel').height() - 26);
})

//При нажатии на кнопку "Роли"
$('#GROUP_ROLES').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/roles/getList', '_self', '');
        }else{
            showError('Список ролей', data);
        }
    })
    return false;
})

//При нажатии на кнопку "Пользователи"
$('#GROUP_USERS').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/users/getList', '_self', '');
        }else{
            showError('Список пользователей', data);
        }
    })
    return false;
})

//При нажатии на кнопку "Объекты"
$('#GROUP_OBJECTS').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/obj/index', '_self', '');
        }else{
            showError('Список объектов', data);
        }
    })
    return false;
})

//При нажатии на кнопку "События"
$('#GROUP_EVENTS').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/events/index', '_self', '');
        }else{
            showError('Список событий', data);
        }
    })
    return false;
})

//При нажатии на кнопку "Контроль"
$('#GROUP_EVENT_CONTROL').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/events/checkEvents', '_self', '');
        }else{
            showError('Контроль событий', data);
        }
    })
    return false;
})

//При нажатии на кнопку "Отчеты"
$('#GROUP_REPORTS').on('click', function(){
    $.post('/main/ajax_saveCache',{lastMenu:$(this).data('menu-item'), lastId: 0, lastUrl: window.location.href}, function(data){
        if (data == ''){
            window.open('/reports/index', '_self', '');
        }else{
            showError('Список отчетов', data);
        }
    })
    return false;
})
