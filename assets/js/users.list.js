//Поиск пользователей
$('body').on('change', '#find', function(){
    var
        aText = $(this).val(),
        keyGroup = 0;
    $('#userList tr').addClass('invisible-table-row');
    $('#userList tr:icontains("'+aText+'")').removeClass('invisible-table-row');

    if (aText == '') {
        $('#userList .row-group').each(function (index, element) {
            keyGroup = $(element).data('key');
            if ($(element).hasClass('closed')) {
                $("#userList .row-item[data-group='" + keyGroup + "']").addClass('invisible-table-row');
            }else{
                $("#userList .row-item[data-group='" + keyGroup + "']").removeClass('invisible-table-row');
            }
        });
    }
});

//Поиск объектов
$('body').on('change', '#findobject', function(){
    var
        aText = $(this).val(),
        keyGroup = 0;
    $('#linkedObjects tr').addClass('invisible-table-row');
    $('#linkedObjects tr:icontains("'+aText+'")').removeClass('invisible-table-row');

    if (aText == '') {
        $('#linkedObjects .row-group').each(function (index, element) {
            keyGroup = $(element).data('key');
            if ($(element).hasClass('closed')) {
                $("#linkedObjects .row-item[data-group='" + keyGroup + "']").addClass('invisible-table-row');
            }else{
                $("#linkedObjects .row-item[data-group='" + keyGroup + "']").removeClass('invisible-table-row');
            }
        });
    }
});

//Сворачивание/разворачивание групп у пользователя
$('#userList').on('click', '.row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#userList .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//Сворачивание/разворачивание групп у объектов
$('#itemDetails').on('click', '.row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#linkedObjects .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//Нажатие на кнопку Включения/Выключения пользователей
$('#userList .row-item .btn').click(function(){
    var
        key     = $(this).closest('tr').data('key'),
        enValue = $(this).hasClass('btn-success'),
        aItem   = $(this);
    $.post('/users/ajax_changeEnabled', {keyItem: key, enabled: 1 - enValue}, function(data){
        if (data == ''){
            if (enValue == 0)
                $(aItem).removeClass('btn-outline-dark').addClass('btn-success');
            else
                $(aItem).removeClass('btn-success').addClass('btn-outline-dark');
        }else
            showError('Настройка пользователей', data);
    });
    return false;
})

//Нажатие на кнопку Включения/Выключения объектов
$('#itemDetails').on('click', '.row-item .btn', function(){
    var
        idUser  = $('#itemProperty').data('key'),
        key     = $(this).closest('tr').data('key'),
        enValue = $(this).hasClass('btn-success'),
        aItem   = $(this);

    if (enValue){
        $.post('/users/ajax_removeLinkedObject', {keyItem: idUser, keyObject: key}, function(data){
            if (data == ''){
                $(aItem).removeClass('btn-success').addClass('btn-outline-dark');
            }else
                showError('Настройка пользователей', data);
        });
    }else{
        $.post('/users/ajax_addLinkedObject', {keyItem: idUser, keyObject: key}, function(data){
            if (data == ''){
                $(aItem).removeClass('btn-outline-dark').addClass('btn-success');
            }else
                showError('Настройка пользователей', data);
        });
    }
    return false;
})

//Нажатие на строку таблицы
$('#userList .row-item').click(function(event){
    //Получаем ключ пользователя
    var
        key = $(this).data('key');
    if (!event.ctrlKey){
        $('#userList .row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }
    //Переклчаем чекбокс
    if ($(this).find('.row-checkbox').hasClass('fa-square-o')){
        $(this).find('.row-checkbox').removeClass('fa-square-o').addClass('fa-check-square-o').closest('tr').addClass('active');
    }else{
        $(this).find('.row-checkbox').removeClass('fa-check-square-o').addClass('fa-square-o').closest('tr').removeClass('active');
    }
    //Устанавливаем наименование
    $('#itemName').html($(this).find('.displayName').html());
    $('#itemProperty').data('key', key);
    $.post('/users/ajax_details', {keyItem: key}, function(data){
        $('#itemDetails').html(data);
    });

    return false;
})

//При изменении поля
$('#itemProperty').on('change', '.editeble', function(){
    var
        idUser     = $('#itemProperty').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this),
        fio        = '';
    $.post('/users/ajax_updateUserField', {keyItem: idUser, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование пользователя', data);
            $(aItem).val(oldValue);
        }else{
            if ($('#fio').val()==''){
                fio = $('#login').val();
            }else{
                fio = $('#fio').val() + ' (' + $('#login').val() + ')';
            }
            $('#itemName').html(fio);
            $('#userList .row-item[data-key='+idUser+'] .displayName').html(fio);
            $('#userList .row-item[data-key='+idUser+'] .displayRole').html($('#role>option:selected').html());
        }
    })
})

//Нажатие на кнопку "Сменить пароль"
$('#itemProperty').on('click', '#dochangepassword', function(){
    var
        idUser      = $('#itemProperty').data('key'),
        newPass     = $('#pass').val(),
        confirmPass = $('#passconfirm').val();
    $.post('/users/ajax_changePassword', {keyItem: idUser, newPassword: newPass, confirmPassword: confirmPass}, function(data){
        if (data != ''){
            showError('Смена пароля', data);
        }
        $('#pass').val('');
        $('#passconfirm').val('');
    })
})

//Нажатие на пункт кнопки "Переместить"
$('#moveUsers').on('click', 'a', function(){
    var
        newId = $(this).data('key'),
        keys = '-1';

    $('#userList .row-item.active').each(function(index, item){
       keys = keys + ','+$(item).data('key');
    });

    $.post('/users/ajax_changeGroup', {keyItems: keys, keyGroup: newId}, function(data){
        if (data != ''){
            showError('Перемещение', data);
        }else{
            location.reload();
        }
    })
})

//Нажатие на пункт кнопки "Удалить"
$('#deleteUsers').click(function(){
    var
        keys = '-1';

    $('#userList .row-item.active').each(function(index, item){
        keys = keys + ','+$(item).data('key');
    });

    $.post('/users/ajax_removeUsers', {keyItems: keys}, function(data){
        if (data != ''){
            showError('Удаление', data);
        }else{
            location.reload();
        }
    })
})