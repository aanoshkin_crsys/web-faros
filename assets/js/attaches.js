//Функция вставки информации по прикрепленным файлам
function loadAttaches(idItem, useImages){
    var
        access      = $('#'+idItem).data('access'),
        tableName   = $('#'+idItem).data('table-name'),
        key         = $('#'+idItem).data('key');

    $.post('/common/ajax_getAttachList', {access:access, tableName: tableName, keyItem: key, images: useImages}, function(data){
        $('#'+idItem).html(data);

        $('.attach-remove').bind('click', function(){
            var
                keyItem = $(this).closest('li').data('key'),
                aItem  = $(this);
            $.post('/common/ajax_removeAttaches', {keyItem: keyItem, access: access}, function(dataRemove){
                if (dataRemove != ''){
                    showError('Удаление файла', dataRemove);
                }else {
                    $(aItem).closest('li').remove();
                }
            });
        })

        $('.image-remove').bind('click', function(){
            var
                keyItem = $(this).closest('.d-inline-flex').data('key'),
                aItem  = $(this);

            $.post('/common/ajax_removeAttaches', {keyItem: keyItem, access: access}, function(dataRemove){
                if (dataRemove != ''){
                    showError('Удаление файла', dataRemove);
                }else {
                    $(aItem).closest('.d-inline-flex').remove();
                }
            });
        })

        $('#add-attach').bind('click', function(){
            $('#fileupload').fileupload({
                type:'post',
                dataType: 'json',
                start: function(e){
                    $('#attach-progress').removeClass('invisible');
                    $('#attach-progress .progress-bar').css('width', '0%').attr('aria-valuenow', 0);
                },
                progress: function (e, data) {
                    var
                        progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#attach-progress .progress-bar').css('width', progress+'%').attr('aria-valuenow', progress);
                },
                always:function (e, data) {
                    $('#attach-progress').addClass('invisible');
                },
                done: function (e, data) {
                    if(data.result.error != undefined){
                        showError('Загрузка файла', data.result.error);
                    }else{
                        loadAttaches(idItem, useImages);
                    }
                }
            });

            $('#select-attach').trigger('click');
        })
    });

}

