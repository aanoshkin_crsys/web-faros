
$('#main-data').on('click', '#do-register', function() {
    var
        login = $('#login').val(),
        fio = $('#fio').val(),
        phone = $('#phone').val(),
        comment = $('#comment').val(),
        pass = $('#pass').val(),
        passc = $('#passconfirm').val(),
        error = 0;

    if (login == '') {
        $('#login').addClass('is-invalid');
        error = 1;
    } else {
        $('#login').removeClass('is-invalid');
    }

    if (fio == '') {
        $('#fio').addClass('is-invalid');
        error = 1;
    } else {
        $('#fio').removeClass('is-invalid');
    }

    if (pass == '') {
        $('#pass').addClass('is-invalid');
        error = 1;
    } else {
        $('#pass').removeClass('is-invalid');
    }

    if ((pass != passc)||(passc == '')) {
        $('#passconfirm').addClass('is-invalid');
        error = 1;
    } else {
        $('#passconfirm').removeClass('is-invalid');
    }

    if (error == 1){
        showError('Регистрация нового пользователя', 'Необходимо заполнить все обязательные поля');
    }else{
        $.post('/auth/ajax_register', {login: login, fio: fio, phone: phone, comment: comment, pass: pass, passc: passc}, function(data){
           if  (data != ''){
               showError('Регистрация нового пользователя', data);
           }else{
               location.href = '/auth/';
           }
        });
    }

});