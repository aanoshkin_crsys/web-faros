/**
 * Нажатие на кнопку Войти
 */
$('#btnLogin').on('click', function(){

    showWaiting('Вход в систему', 'Пожалуйста подождите, идет проверка...');

    findLogin = $('#login').val();
    findPassword = $('#password').val();
    $.post('/auth/ajax_doLogin', {inLogin: findLogin, inPassword: findPassword}, function(data){
        if (data !==''){
            showError('Ошибка авторизации', data);
            hideWaiting();
        }else{
            window.location.replace("/");
        }
    });
});
