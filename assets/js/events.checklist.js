//Первоначальная инициализация
var
    //dateRangeStart = moment().subtract(1, 'year'),
    dateRangeStart = moment().subtract(1, 'days'),
    dateRangeFinish = moment().subtract(1, 'days');

//При загрузке страницы
$(function(){
    //Считываем значения фильтра
    $.post('/events/ajax_getFilter', {}, function(data){
        $('#filter-panel').html(data);
        $('#eventTypeGroup').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#eventType').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#analitic').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });
        $('#object').select2({
            placeholder: "-Нет-",
            dropdownAutoWidth: true,
            closeOnSelect: false,
        });

        $('#range').val(dateRangeStart.format('DD.MM.YYYY') + ' - ' + dateRangeFinish.format('DD.MM.YYYY'));

        $('#do-filter').trigger("click");
    })
})

//При нажатии на кнопку "Очистить фильтр"
$('body').on('click', '#do-clear-filter', function (){
    //Очищаем значения
    $('#analitic').val(null).trigger("change");
    $('#eventType').val(null).trigger("change");
    $('#object').val(null).trigger("change");
    $('#control').val(-1);
    $('#community-filter').val(-1);
    $('#view-text').val('');
    //Нажимаем кнопку "Очистить период"
    $('#range-clear').trigger("click");
    //Нажимаем кнопку "Фильтровать"
    $('#do-filter').trigger("click");
})

//Поиск объектов
$('body').on('change', '#find', function(){
    var
        //Получаем значения
        aText = $(this).val(),
        keyGroup = 0;
    //Скрываем все строки
    $('#eventList tr').addClass('invisible-table-row');
    //Для найденных с указанным текстом строк убираем класс скрытия
    $('#eventList tr:icontains("'+aText+'")').removeClass('invisible-table-row');
    //Скрываем дополнительно строки, которые находятся в свернутой группе
    if (aText == '') {
        $('#obeventListjList .row-group').each(function (index, element) {
            keyGroup = $(element).data('key');
            if ($(element).hasClass('closed')) {
                $("#eventList .row-item[data-group='" + keyGroup + "']").addClass('invisible-table-row');
            }else{
                $("#eventList .row-item[data-group='" + keyGroup + "']").removeClass('invisible-table-row');
            }
        });
    }
});

//Сворачивание/разворачивание групп у объектов
$('body').on('click', '#eventList .row-group td', function(){
    var
        item = $(this).closest('tr'),
        key = $(this).closest('tr').data('key');

    $(item).toggleClass('closed');
    if ($(item).hasClass('closed')){
        $(item).find('.expand.fa').removeClass('fa-folder-open-o').addClass('fa-folder-o');
    }else{
        $(item).find('.expand.fa').removeClass('fa-folder-o').addClass('fa-folder-open-o');
    }
    $("#eventList .row-item[data-group='"+key+"']").toggleClass('invisible-table-row');
})

//При нажатии на кнопку мыши на период, отккрываем календарь
$('body').on('mousedown', '#range', function() {
    $('#range').daterangepicker({
            autoApply: true,
            startDate: dateRangeStart,
            endDate: dateRangeFinish,
            buttonClasses: 'btn btn-primary',
            cancelClass: 'btn-dark',
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                'Текущий месяц': [moment().startOf('month'), moment().endOf('month')],
                'Предыдущий месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Последний год': [moment().subtract(1, 'year'), moment()]
            },
            locale: {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Принять",
                "cancelLabel": "Отменить",
                "fromLabel": "от",
                "toLabel": "до",
                "customRangeLabel": "Выборочно",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Новябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            },
        }, function (start, end) {
            dateRangeStart = start;
            dateRangeFinish = end;
            $('#range').val(start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        }
    )
})

//При нажатии на кнопку отмены в выборе периода
$('body').on('#range cancel.daterangepicker', function(ev, picker) {
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
});

//При нажатии на кнопку "Очистить период"
$('body').on('click', '#range-clear', function(){
    dateRangeStart = 0;
    dateRangeFinish = 0;
    $('#range').val('-Нет-');
})

//При нажатии на кнопку "Фильтровать"
$('body').on('click', '#do-filter', function (){
    var
        //Получим значения по фильтру
        idAnalitic = $('#analitic').val().join(","),
        idEvent    = $('#eventType').val().join(","),
        idEventGroups= $('#eventTypeGroup').val().join(","),
        idObject   = $('#object').val().join(","),
        control    = $('#control').val(),
        commFilter = $('#community-filter').val(),
        viewText   = $('#view-text').val(),
        aRangeStart = '',
        aRangeFinish= '';

    //Если период установлен, то получаем значения
    if ($('#range').val() != '-Нет-'){
        aRangeStart = dateRangeStart.format('DD.MM.YYYY');
        aRangeFinish= dateRangeFinish.format('DD.MM.YYYY');
    }else{
        //В противном случае передаются пустые значения
        aRangeStart = '';
        aRangeFinish= '';
    }
    //Получаем список событий по переданным параметрам
    $.post('/events/ajax_getCheckList', {rangeStart: aRangeStart, rangeFinish: aRangeFinish, idAnalitic: idAnalitic, idEvent: idEvent, idEventGroups: idEventGroups, idObject: idObject, control: control, commFilter: commFilter, viewText: viewText}, function(data){
        $('#eventList').html(data);
        $('#eventList .eventTypeDetail').select2();
    })
})

//Нажатие на кнопку "Удалить"
$('#eventList').on('click', '.eventRemove', function(){
    var
        row = $(this).closest('.row-item'),
        keyItem = $(row).data('key');
    $.post('/events/ajax_removeEvents', {keyItems: keyItem}, function(data){
        if (data != ''){
            showError('Удаление события', data);
        }else{
            $(row).remove();
        }
    })
})

//Нажатие на кнопку "Проверено"
$('#eventList').on('click', '.eventControl', function(){
    var
        row = $(this).closest('.row-item'),
        btn = $(this),
        keyItem = $(row).data('key');
    $.post('/events/ajax_control', {keyItem: keyItem}, function(data){
        if (data != ''){
            showError('Контроль', data);
        }else{
            $(btn).removeClass('eventControl').addClass('eventControlCancel').removeClass('btn-outline-success').addClass('btn-outline-secondary').html('Отменить');
        }
    })
})

//Нажатине на кнопку "Отменить"
$('#eventList').on('click', '.eventControlCancel', function(){
    var
        row = $(this).closest('.row-item'),
        btn = $(this),
        keyItem = $(row).data('key');
    $.post('/events/ajax_cancelcontrol', {keyItem: keyItem}, function(data){
        if (data != ''){
            showError('Контроль', data);
        }else{
            $(btn).addClass('eventControl').removeClass('eventControlCancel').addClass('btn-outline-success').removeClass('btn-outline-secondary').html('Проверено');
        }
    })
})

//Нажатие на кнопку "Проверить все"
$('#eventCheck').click(function(){
    var
        keyItems = '-1';
    $('.row-item .eventControl').each(function (index, element) {
        if ($(element).closest('.row-item').find('.is-invalid').length == 0){
            keyItems = keyItems + ',' + $(element).closest('.row-item').data('key');
        }
    });

    if (keyItems != '-1'){
        $.post('/events/ajax_control', {keyItem: keyItems}, function(data){
            if (data != ''){
                showError('Контроль', data);
            }else{
                $('.row-item .eventControl').each(function (index, element) {
                    if ($(element).closest('.row-item').find('.is-invalid').length == 0){
                        $(element).removeClass('eventControl').addClass('eventControlCancel').removeClass('btn-outline-success').addClass('btn-outline-secondary').html('Отменить');
                    }
                });

            }
        })
    }
})

$('#eventList').on('change', '.eventTypeDetail', function(){
    var
        key = $(this).val(),
        item = $(this);
    $.post('/common/ajax_getItemComment', {keyItem:key}, function(data){
        if (data != ''){
            $(item).closest('.row-item').find('.eventMessage').val(data);
            $(item).closest('.row-item').find('.eventMessage').trigger('change');
        }
    })
})

$('#eventList').on('change', '.eventMessageControl', function(){
    if ($(this).val() == ''){
        $(this).removeClass('is-invalid');
    }else {
        $(this).addClass('is-invalid');
    }
})

//При изменении поля
$('#eventList').on('change', '.editeble', function(){
    var
        keyItem    = $(this).closest('.row-item').data('key'),
        field      = $(this).data('field'),
        value      = $(this).val(),
        oldValue   = $(this).data('old'),
        aItem      = $(this);

    $.post('/events/ajax_update', {keyItem: keyItem, field: field, value: value}, function(data){
        if (data != ''){
            showError('Редактирование объекта', data);
            $(aItem).val(oldValue);
        }
    })
})