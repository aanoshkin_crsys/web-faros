<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

	<!-- Modal -->
	<div class="modal fade" id="myModalWindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-danger">
					<h4 class="modal-title" id="myModalLabel">Modal title</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="myModalBody">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Waiting form -->
    <div class="modal fade" id="myWaitWindow" tabindex="-1" role="dialog" aria-labelledby="myWaitLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-danger">
                    <h4 class="modal-title" id="myWaitLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td><img src="/assets/images/load.gif" height="100px"/> </td>
                            <td><div id="myWaitBody"></div></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    Пожалуйста подождите...
                </div>
            </div>
        </div>
    </div>
    <!-- Для отображения изображения на весь экран -->
    <div id="fullsize" class="invisible">
        <div id="closeFullSize"><i class="fa d-inline fa-times fa-2x"></i></div>
        <img class="img-fluid d-block" src="" />
        <p></p>
    </div>

	<script src="/assets/js/jquery-3.2.1.min.js"></script>
	<script src="/assets/js/jquery.cookie.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/tools.js"></script>
	  
	<?php
		//Подключение дополнительных скриптов
		if (isset($scripts)){
			foreach($scripts as $value){
				echo "<script src='/assets/js/{$value}.js?sum=".md5_file(base_url("/assets/js/{$value}.js"))."'></script>
				";
			}
		}
	?>

	</body>
</html>