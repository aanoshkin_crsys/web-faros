<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <table class="table p-0 m-0 table-sm" id="roleList">
        <?php
        foreach($groupList as $group){
            echo "<tr><td colspan='2' class='row-group'><b>{$group['NAME']}</b></td></tr>";
            foreach($details[$group['NAME']] as $right){
                if ($right['HASPERMISSION'] == 0){
                    $aclass = "btn-outline-dark";
                    $atext ="Выкл";
                }else{
                    $aclass = "btn-success";
                    $atext = "Вкл";
                };

                echo "<tr data-key='{$right['ID_ITEM']}'>
                        <td width='50px'>
                          <button style='width:50px;' class='btn {$aclass} btn-sm'>{$atext}</button>
                        </td>
                        <td>{$right['NAME']}</td>
                      </tr>";
            }
        }
        ?>
        </table>
    </div>
</div>
