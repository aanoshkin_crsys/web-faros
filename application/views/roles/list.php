<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="col-10 p-1">
    <h3>Настройка ролей</h3>
    <div class="container-fluid container-full-height scrollbox" id="main-data">
        <div class="row">
            <div class="col-4 p-1">
                <div class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <div class="row">
                            <div class="col-6">
                                <h5>Роли</h5>
                            </div>
                            <div class="col-6">
                                <input type="text" id="find" class="form-control form-control-sm" placeholder="Поиск" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-2 object-card">
                        <div class="container-fluid">
                            <div class="row">
                                <table class="table table-hover p-0 m-0 table-sm" id="roleList">
                                <?php
                                    foreach ($roleList as $row){
                                        echo "<tr data-key='{$row['ID_USER_ROLE']}'>";
                                        echo "<td>{$row['NAME']}</td>";
                                        echo "</tr>";
                                    }
                                ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8 p-1">
                <div id="itemProperty" class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <h5><span id="itemName">Не выбрано</span></h5>
                    </div>
                    <div class="card-body py-2 object-card" id="itemDetails">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
