<?php defined('BASEPATH') OR exit('No direct script access allowed');

echo "
<div class='attach-list'>
<label class='col-form-label lbl-header'>Прикрепленные файлы</label>
<ul>";

if ($canEdit){
    $fileTools = " [<i class='fa fa-lg fa-trash-o cursor-pointer attach-remove' title='Удалить'></i>]";
}else{
    $fileTools = "";
}

foreach ($attachList as $row){
    echo "<li data-key='{$row['ID_ATTACHMENT']}'>
              <i class='fa fa-lg fa-paperclip'></i> <a href='{$row['FNAME']}' target='_blank' title='Автор: {$row['AUTHOR_NAME']}'>{$row['DISPLAYNAME']}</a> ({$row['FILESIZE']} КБ) {$fileTools}
          </li>";
}
echo "</ul>";

if ($canEdit){
    echo "<hr><div class='col-12 p-0'>";
    echo form_open_multipart('/common/ajax_uploadAttach', array('id' => 'fileupload'));
    echo "<input type='hidden' name='tableName' value='{$tableName}'>
          <input type='hidden' name='keyItem' value='{$keyItem}'>
          <span class='fileinput-button'>
            <input id='select-attach' type='file' name='userfile'>
          </span>
          <div id='add-attach' class='btn btn-outline-primary'><i class='fa fa-lg fa-plus'></i> Прикрепить файл</input>
              <div id='attach-progress' class='progress my-1 invisible' style='height: 3px;'>
                  <div class='progress-bar' role='progressbar' style='width: 0%;' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'></div>
              </div>
          </div>";
    echo form_close();
    echo "</div>";
}

echo "</div>";
