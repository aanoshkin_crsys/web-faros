<?php defined('BASEPATH') OR exit('No direct script access allowed');
    echo "<table class='community-list'>";
  foreach ($commList as $row){
      if ($row['ID_AUTHOR'] != $currentIdUser){
          $mess = "{$row['MESSAGE']}";
          if ($row['DATE_READ'] == ''){
              $mess = "<b>{$mess}</b>";
          }
          echo "<tr class='align-top'>
                    <td width='75'>".$this->Auth->getAvatar($row['ID_AUTHOR'], $row['AVATARNAME'], 'community-avatar')."</td>
                    <td colspan='2'><div class='from-them'><small>{$row['DATE_CREATE']}</small><br>{$mess}</div></td>
                </tr>";
      }else{
          echo "<tr class='align-top'>
                    <td colspan='2'><div class='from-me'><small>{$row['DATE_CREATE']}</small><br>{$row['MESSAGE']}</div></td>
                    <td class='text-right ' width='75'>".$this->Auth->getAvatar($row['ID_AUTHOR'], $row['AVATARNAME'], 'community-avatar')."</td>
                </tr>";
      }
  }
  echo "</table>";

  if ($canEdit){
      echo "<hr>
            <div class='col-12 p-0'>
                <textarea id='community-text' class='form-control' rows='3'></textarea>
                <div id='community-send' class='btn btn-block btn-primary'><i class='fa fa-lg fa-send-o'></i> Отправить</div>
            </div>
            ";
  }
?>


