<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<div class="container container-full-height scrollbox" id="main-data">
    <div class="row">
        <div class="col-8 offset-2 p-1">
            <div id="itemProperty" class="card">
                <div class="card-header p-2" style="min-height: 48px;">
                    <h5><span id="itemName">Регистрация нового пользователя</span></h5>
                </div>
                <div class="card-body p-2 object-card" id="itemDetails" >
                    <div class="container container-full-height scrollbox" id="main-data">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Логин*</label>
                                    <div class="col-sm-9">
                                        <input type="email" id="login" class="form-control editeble" placeholder="Укажите логин" data-field="LOGIN">
                                        <div class="invalid-feedback">
                                            Необходимо указать логин (e-mail).
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label">Ф.И.О.*</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="fio" class="form-control editeble" data-field="NAME" placeholder="Укажите Фамлию, Имя, Отчество">
                                        <div class="invalid-feedback">
                                            Необходимо указать Фамилию, Имя, Отчество.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label">Телефон</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="phone" class="form-control editeble" data-field="PHONE" placeholder="Укажите телефон">
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label">Примечание</label>
                                    <div class="col-sm-9">
                                        <textarea id="comment" class="form-control editeble" rows="3"data-field="COMMENT"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <h5>Пароль</h5>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label">Пароль*</label>
                                    <div class="col-sm-9">
                                        <input type="password" id="pass" class="form-control" placeholder="Укажите пароль">
                                        <div class="invalid-feedback">
                                            Необходимо указать пароль.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label">Подтвердите пароль*</label>
                                    <div class="col-sm-9">
                                        <input type="password" id="passconfirm" class="form-control" placeholder="Повторите пароль">
                                        <div class="invalid-feedback">
                                            Пароль должен совпадать.
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row"> <label class="col-sm-3 col-form-label"> </label>
                                    <div class="col-sm-9">
                                        <button id="do-register" class="btn btn-primary btn-block">Зарегистрировать</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
