<div class="modal" style="display:block; top: 20%;" tabindex="-1" role="dialog">
    <div class="modal-dialog px-5" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Восстановление пароля</h5>
            </div>
            <div class="modal-body px-5">
                <div class="form-group">
                    <label>Логин</label>
                    <label><b>[<?php echo $login?>]</b></label>
                </div>
                <div class="form-group">
                    <label>Секретный код</label>
                    <input type="text" id="code" class="form-control" placeholder="Введите секретный код" value="<?php echo $code;?>" />
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input type="password" id="pass" class="form-control" placeholder="Введите пароль" />
                </div>
                <div class="form-group">
                    <label>Подтверждение пароля</label>
                    <input type="password" id="passconfirm" class="form-control" placeholder="Подтвердите пароль" />
                </div>
            </div>
            <div class="modal-footer px-5">
                <a href="#" id="btnRestore" class="btn btn-primary btn-block">Сменить пароль</a>
            </div>
        </div>
    </div>
</div>