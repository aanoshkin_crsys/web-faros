<div class="modal" style="display:block; top: 20%;" tabindex="-1" role="dialog">
    <div class="modal-dialog px-5" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Восстановление пароля</h5>
            </div>
            <div class="modal-body px-5">
                <div class="form-group">
                    <label>Логин</label>
                    <input type="email" name="login" id="login" class="form-control" placeholder="Введите логин" value="<?php echo get_cookie(COOKIE_LOGIN);?>" />
                </div>
                <p class="py-3 m-0">
                    После восстановления, Вам на электронный адрес придет письмо с дальнейшими инструкциями.
                </p>
            </div>
            <div class="modal-footer px-5">
                <a href="#" id="btnRestore" class="btn btn-primary btn-block">Восстановить</a>
            </div>
        </div>
    </div>
</div>