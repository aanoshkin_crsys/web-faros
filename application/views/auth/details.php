<?php defined('BASEPATH') OR exit('No direct script access allowed');
    if ($canEdit == true){
        $disabled = "";
    }else{
        $disabled = "disabled";
    }
?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
            <i class="fa fa-lg fa-info-circle"></i> Информация
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="obj-access-tab" data-toggle="tab" href="#obj-access" role="tab" aria-controls="obj-access" aria-selected="false">
            <i class="fa fa-lg fa-user-o"></i> Доступ к объектам
        </a>
    </li>
</ul>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="container container-full-height scrollbox" id="main-data">

            <div class="row">
                <div class="col-12 p-0">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Аватар</label>
                        <div class="col-sm-9">
                            <?=$this->Auth->getAvatar($userInfo['ID_USER'], $userInfo['AVATARNAME'], ''); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Логин*</label>
                        <div class="col-sm-9">
                            <input type="text" id="login" <?=$disabled?> class="form-control editeble" placeholder="Укажите логин" data-old="<?=$userInfo['LOGIN']?>" data-field="LOGIN" value="<?=$userInfo['LOGIN']?>">
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Роль</label>
                        <div class="col-sm-9">
                            <select id="role" <?=$disabled?> class="form-control editeble" data-old="<?=$userInfo['ID_USER_ROLE']?>" data-field="ID_USER_ROLE">
                                <?
                                foreach($roleList as $role){
                                    if ($userInfo['ID_USER_ROLE'] == $role['ID_USER_ROLE']){
                                        $selected = 'selected';
                                    }else{
                                        $selected = '';
                                    }
                                    echo "<option {$selected} value='{$role['ID_USER_ROLE']}'>{$role['NAME']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Ф.И.О.</label>
                        <div class="col-sm-9">
                            <input type="text" id="fio" <?=$disabled?> class="form-control editeble" data-old="<?=$userInfo['NAME']?>" data-field="NAME" placeholder="Укажите Фамлию, Имя, Отчество" value="<?=$userInfo['NAME']?>">
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Телефон</label>
                        <div class="col-sm-9">
                            <input type="text" id="phone" <?=$disabled?> class="form-control editeble" data-old="<?=$userInfo['PHONE']?>" data-field="PHONE" placeholder="Укажите телефон" value="<?=$userInfo['PHONE']?>">
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Примечание</label>
                        <div class="col-sm-9">
                            <textarea id="comment" class="form-control editeble" <?=$disabled?> rows="3" data-old="<?=$userInfo['COMMENT']?>" data-field="COMMENT"><?=$userInfo['COMMENT']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Дата регистрации</label>
                        <div class="col-sm-9">
                            <input type="text" id="phone" class="form-control" disabled value="<?=$userInfo['DATE_REGISTER']?>">
                        </div>
                    </div>
                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Дата последнего посещения</label>
                        <div class="col-sm-9">
                            <input type="text" id="phone" class="form-control" disabled value="<?=$userInfo['LAST_VISIT']?>">
                        </div>
                    </div>
                    <?
                    if ($canEdit == true){
                        ?>
                        <hr>
                        <h5>Смена пароля</h5>
                        <div class="form-group row"> <label class="col-sm-3 col-form-label">Пароль</label>
                            <div class="col-sm-9">
                                <input type="password" id="pass" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group row"> <label class="col-sm-3 col-form-label">Подтвердите пароль</label>
                            <div class="col-sm-9">
                                <input type="password" id="passconfirm" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group row"> <label class="col-sm-3 col-form-label"> </label>
                            <div class="col-sm-9">
                                <button id="dochangepassword" class="btn btn-primary btn-block">Сменить пароль</button>
                            </div>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="obj-access" role="tabpanel" aria-labelledby="obj-access-tab">
        <div class="row">
            <div class="col-6 offset-6 py-1">
                <input type="text" class="form-control form-control-sm" id="findobject" placeholder="Поиск">
            </div>
        </div>
        <table class="table table-hover p-0 m-0 table-sm" id="linkedObjects">
            <?
            $group = 0;
            foreach($linkedObjects as $row){
                if ($group != $row['ID_OBJECT_GROUP']) {
                    $group = $row['ID_OBJECT_GROUP'];
                    echo "<tr class='row-group' data-key='{$group}'><td colspan='3'>
                            <span class='fa d-inline fa-folder-open-o fa-lg'></span>
                            {$row['GROUP_NAME']}</td>
                        </tr>";
                }

                if ($row['HASACCESS'] == 0){
                    $aclass = "btn-outline-dark";
                    $atext ="Выкл";
                }else{
                    $aclass = "btn-success";
                    $atext = "Вкл";
                };

                echo "<tr class='row-item' data-key='{$row['ID_OBJECT']}' data-group='{$row['ID_OBJECT_GROUP']}'>
                        <td width='25px'> </td> <td width='50px'> <button style='width:50px;' class='btn {$aclass} btn-sm'>{$atext}</button></td>
                        <td class='displayName'>{$row['OBJECT_NAME']}</td></tr>";
            }
            ?>
        </table>
    </div>
</div>
