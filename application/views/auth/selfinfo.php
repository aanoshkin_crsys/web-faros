<?php defined('BASEPATH') OR exit('No direct script access allowed');?>


<div class="col-10 p-1">
    <h3>Настройка личных данных</h3>
    <div class="container container-full-height scrollbox" id="main-data">
        <div class="row">
            <div class="col-12 p-1">
                <div id="itemProperty" class="card" data-key="<?=$userInfo['ID_USER'];?>">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <h5><span id="itemName"><?=$this->Auth->getDisplayName();?></span></h5>
                    </div>
                    <div class="card-body p-2 object-card" id="itemDetails" >
                        <div class="container container-full-height scrollbox" id="main-data">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Аватар</label>
                                        <div class="col-sm-9">
                                            <div class="d-flex flex-row">
                                                <div id="visible-avatar" class="p-2"><?=$this->Auth->currentAvatar('cursor-pointer change-avatar');?></div>
                                                <div class="p-2">
                                                <?php
                                                    echo form_open_multipart('users/ajax_uploadAvatar', array('id' => 'fileupload')); ?>
                                                    <span class="fileinput-button">
				                                        <input id="select-avatar" type="file" name="userfile" accept="image/jpeg,image/png,image/gif" hidden>
                                        			</span>
                                                <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Логин*</label>
                                        <div class="col-sm-9">
                                            <input type="email" id="login" class="form-control editeble" placeholder="Укажите логин" data-old="<?=$userInfo['LOGIN']?>" data-field="LOGIN" value="<?=$userInfo['LOGIN']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Ф.И.О.</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="fio" class="form-control editeble" data-old="<?=$userInfo['NAME']?>" data-field="NAME" placeholder="Укажите Фамлию, Имя, Отчество" value="<?=$userInfo['NAME']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Телефон</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="phone" class="form-control editeble" data-old="<?=$userInfo['PHONE']?>" data-field="PHONE" placeholder="Укажите телефон" value="<?=$userInfo['PHONE']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Примечание</label>
                                        <div class="col-sm-9">
                                            <textarea id="comment" class="form-control editeble" rows="3" data-old="<?=$userInfo['COMMENT']?>" data-field="COMMENT"><?=$userInfo['COMMENT']?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Дата регистрации</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?=$userInfo['DATE_REGISTER']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Дата последнего посещения</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?=$userInfo['LAST_VISIT']?>">
                                        </div>
                                    </div>
                                    <hr>
                                    <h5>Смена пароля</h5>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Пароль</label>
                                        <div class="col-sm-9">
                                            <input type="password" id="pass" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label">Подтвердите пароль</label>
                                        <div class="col-sm-9">
                                            <input type="password" id="passconfirm" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row"> <label class="col-sm-3 col-form-label"> </label>
                                        <div class="col-sm-9">
                                            <button id="dochangepassword" class="btn btn-primary btn-block">Сменить пароль</button>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6 py-2">
                                            <h5>Доступ к объектам</h5>
                                        </div>
                                        <div class="col-6 py-1">
                                            <input type="text" class="form-control form-control-sm" id="findobject" placeholder="Поиск">
                                        </div>
                                    </div>
                                    <table class="table table-hover p-0 m-0 table-sm" id="linkedObjects">
                                        <?
                                        $group = 0;
                                        foreach($linkedObjects as $row){
                                            if ($group != $row['ID_OBJECT_GROUP']) {
                                                $group = $row['ID_OBJECT_GROUP'];
                                                echo "<tr class='row-group' data-key='{$group}'><td colspan='3'>
                                <span class='fa d-inline fa-folder-open-o fa-lg'></span>
                                {$row['GROUP_NAME']}</td>
                            </tr>";
                                            }

                                            if ($row['HASACCESS'] == 0){
                                                $aclass = "btn-outline-dark";
                                                $atext ="Выкл";
                                            }else{
                                                $aclass = "btn-success";
                                                $atext = "Вкл";
                                            };

                                            echo "<tr class='row-item' data-key='{$row['ID_OBJECT']}' data-group='{$row['ID_OBJECT_GROUP']}'>
                            <td width='25px'> </td> <td width='50px'> <button style='width:50px;' class='btn {$aclass} btn-sm'>{$atext}</button></td>
                            <td class='displayName'>{$row['OBJECT_NAME']}</td></tr>";
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
