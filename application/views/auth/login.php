<div class="modal" style="display:block; top: 20%;" tabindex="-1" role="dialog">
    <div class="modal-dialog px-5" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Вход в личный кабинет</h5>
            </div>
            <div class="modal-body px-5">
                <div class="form-group">
                    <label>Логин</label>
                    <input type="email" name="login" id="login" class="form-control" placeholder="Введите логин" value="<?=$login;?>" />
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Введите Password" value="<?=$pass; ?>">
                </div>
                <p class="py-3 m-0">
                    <a href="/auth/restore_password">Забыли пароль?</a><br>
                    <a href="/auth/registration">Регистрация</a>
                </p>
            </div>
            <div class="modal-footer px-5">
                <a href="#" id="btnLogin" class="btn btn-primary btn-block">Войти</a>
            </div>
        </div>
    </div>
</div>
