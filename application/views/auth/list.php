<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="col-10 p-1">
    <h3>Настройка пользователей</h3>
    <div class="container-fluid container-full-height scrollbox" id="main-data">
        <div class="row">
            <div class="col-6 p-1">
                <div class="card">
                    <div class="card-header p-2">
                        <div class="row">
                            <div class="col-6">
                                <div class="dropdown">
                                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="fa fa-share-square-o fa-lg" title="Переместить пользователей в группу"></span></button>
                                    <div id="moveUsers" class="dropdown-menu" aria-labelledby="moveUsers">
                                    <?php
                                        foreach ($userGroups as $group){
                                            echo "<a class='dropdown-item' href='#' data-key='{$group['ID_USER_GROUPS']}'>{$group['NAME']}</a>";
                                        }
                                    ?>
                                    </div>
                                    <button class="btn btn-outline-primary btn-sm" type="button" id="deleteUsers" title="Удалить вбранных пользователей"><span class="fa fa-trash-o fa-lg"></button>
                                </div>
                            </div>
                            <div class="col-6">
                                <input type="text" id="find" class="form-control form-control-sm" placeholder="Поиск" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-2 object-card">
                        <div class="container-fluid">
                            <div class="row">
                                <table class="table table-hover p-0 m-0 table-sm" id="userList">
                                <?php
                                    $group = '';
                                    foreach ($userList as $user){
                                        if ($group != $user['GROUP_NAME']){
                                            $group = $user['GROUP_NAME'];
                                            echo "<tr class='row-group' data-key='{$user['ID_USER_GROUPS']}'><td colspan='6'>
                                                <span class='fa d-inline fa-folder-open-o fa-lg'></span>
                                                {$user['GROUP_NAME']}</td>
                                            </tr>";
                                        }
                                        if ($user['ENABLED'] == 0){
                                            $aclass = "btn-outline-dark";
                                            $atext ="Выкл";
                                        }else{
                                            $aclass = "btn-success";
                                            $atext = "Вкл";
                                        };
                                        if ($user['USER_NAME'] != '') {
                                            echo "<tr class='row-item' data-key='{$user['ID_USER']}' data-group='{$user['ID_USER_GROUPS']}'>
                                                <td width='25px'> </td>
                                                <td width='25px'><span class='row-checkbox fa d-inline fa-square-o'></span></td>
                                                <td width='50px'> <button style='width:50px;' class='btn {$aclass} btn-sm'>{$atext}</button></td>
                                                <td width='30px'>".$this->Auth->getAvatar($user['ID_USER'], $user['AVATARNAME'], 'row-avatar')."</td> 
                                                <td class='displayName'>";
                                            if ($user['USER_NAME'] != '')
                                                echo "{$user['USER_NAME']} ({$user['LOGIN']})";
                                            else
                                                echo $user['LOGIN'];
                                            echo "</td><td class='displayRole'>{$user['ROLE_NAME']}</td>
                                                </tr>";
                                        }
                                    }
                                ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 p-1">
                <div id="itemProperty" class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <h5><span id="itemName">Не выбрано</span></h5>
                    </div>
                    <div class="card-body py-2 object-card" id="itemDetails">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>