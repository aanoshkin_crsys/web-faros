<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="col-10 p-1">
    <h3>Список событий</h3>
    <div class="container-fluid container-full-height scrollbox" id="main-data">
        <div class="row">
            <div class="col-7 p-1">
                <div class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <div class="row">
                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <input type="text" id="find" class="form-control form-control-sm" placeholder="Поиск" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-2 object-card">
                        <div class="container-fluid">
                            <div class="row" id="eventList">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-5 p-1">
                <div id="itemProperty" class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <h5><span id="itemName">Не выбрано</span></h5>
                    </div>
                    <div class="card-body p-2 object-card" id="itemDetails">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
