<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<table class="table p-0 m-0 table-sm" id="eventTable">
    <?php
    $obj = "-1";
    foreach ($eventList as $row){

        if ($obj != $row['ID_OBJECT']){
            $obj = $row['ID_OBJECT'];
            echo "<tr class='row-group' data-key='{$row['ID_OBJECT']}'>
                    <td colspan='6'><span class='fa fa-folder-open-o fa-lg expand'></span> {$row['OBJECT_NAME']}</td>
                </tr>";
        }
        $imgs = $this->Attach->getList(TABLE_OBJECT_EVENTS, $row['ID_OBJECT_EVENT']);
        echo "<tr class='row-item' data-group='{$row['ID_OBJECT']}' data-key='{$row['ID_OBJECT_EVENT']}'><td>
                <table class='red-table table table-sm' width='100%'>
                    <tr><td>Событие №<b>{$row['ID_OBJECT_EVENT']}</b>  от <b>".$this->Common->strtodatetime($row['DATE_CREATE'])."</b></td>
                    <td width='215px'>";
        if ($row['ID_CONTROLLER'] == '') {
            echo "<button class='eventControl btn btn-outline-success btn-sm' type='button'><span class='fa fa-check-circle-o fa-lg' ></span> Проверено</button>";
        }else{
            echo "<button class='eventControlCancel btn btn-outline-secondary btn-sm' type='button'><span class='fa fa-check-circle-o fa-lg' ></span> Отменить</button>";
        }
         echo "         <button class='eventRemove btn btn-outline-dark btn-sm' type='button' title='Удалить'><span class='fa fa-trash-o fa-lg' ></span> Удалить</button>
                    </td></tr>    
                    <tr><td colspan='2'>";

        foreach ($imgs as $img){
            echo "<img src='{$img['FNAME']}' class='preview-full' width='100%' />";
        }

        echo "</b></td></tr>    
                    <tr><td colspan='2'>
            <table class='none-table' width='100%'>
            <tr>
                <td width='66%'>
                <div class='form-group'>
                    <label class='col-form-label lbl-header'>Тип события</label>
                    <div class='edt-item'>
                        <select class='eventTypeDetail form-control form-control-sm editeble' style='width: 100%' data-field='ID_EVENT' data-old='{$row['ID_EVENT']}'>";
                        $group = "";
                        foreach($eventTypes as $subrow){
                            if ($group != $subrow['GROUP_NAME']){
                                $group = $subrow['GROUP_NAME'];
                                echo "<optgroup label='{$group}'>";
                            }

                            if ($row['ID_EVENT'] == $subrow['ID_EVENT']){
                                $selected = "selected";
                            }else{
                                $selected = "";
                            }
                            echo "<option {$selected} value='{$subrow['ID_EVENT']}'>{$subrow['EVENT_NAME']}</option>";
                        }
            echo "          </optgroup>
                        </select>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-form-label lbl-header'>Текст события</label>
                    <div class='edt-item'>
                        <textarea class='eventMessage form-control editeble' data-old='{$row['MESSAGE']}' data-field='MESSAGE' placeholder='Введите примечание' rows='3'>{$row['MESSAGE']}</textarea>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-form-label lbl-header'>Замечание</label>
                    <div class='edt-item'>";
                    if ($row['COMPLETED']) {
                        echo "<textarea class='eventMessageControl form-control is-valid' readonly placeholder='Введите замечание' rows='3'>{$row['MESSAGE_CONTROL']}</textarea>";
                    }else if ($row['MESSAGE_CONTROL'] != ''){
                        echo "<textarea class='eventMessageControl form-control editeble is-invalid' data-old='{$row['MESSAGE_CONTROL']}' data-field='MESSAGE_CONTROL' placeholder='Введите замечание' rows='3'>{$row['MESSAGE_CONTROL']}</textarea>";
                    }else{
                        echo "<textarea class='eventMessageControl form-control editeble' data-old='{$row['MESSAGE_CONTROL']}' data-field='MESSAGE_CONTROL' placeholder='Введите замечание' rows='3'>{$row['MESSAGE_CONTROL']}</textarea>";
                    }
            echo "  </div>
                </div>
                </td>
                <td width='34%'>
                    <div class='form-group'>
                        <label class='col-form-label lbl-header'>Аналитик</label>
                        <div class='edt-item'>{$row['USER_NAME']}</div>
                    </div>
                    <div class='form-group'>
                        <label class='col-form-label lbl-header'>Инициатор</label>
                        <div class='edt-item'>{$row['PERSON']}</div>
                    </div>
                    <div class='form-group'>
                        <label class='col-form-label lbl-header'>Дата события</label>
                        <div class='edt-item'>{$row['DATE_EVENT']}</div>
                    </div>
                </td>
            </tr>
            </table>
                    </td></tr>    
                </table>
              </td></tr>";
    }
    ?>
</table>