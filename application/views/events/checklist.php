<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-10 p-1">
    <div class="container-fluid container-full-height scrollbox" id="main-data">
        <h3>Контроль событий</h3>
        <div class="row">
            <div class="col-12 p-1">
                <div class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-outline-success btn-sm" type="button" id="eventCheck" title="Проверка всех событий"><span class="fa fa-check-square-o fa-lg" ></span> Проверить события</button>
                            </div>
                            <div class="col-6">
                                <input type="text" id="find" class="form-control form-control-sm" placeholder="Поиск" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-2 object-card">
                        <div class="container-fluid">
                            <div class="row" id="eventList">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>