<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<table class="table table-hover p-0 m-0 table-sm" id="eventTable">
    <thead>
        <tr>
            <th style="width: 25px;"></th>
            <th>Дата</th>
            <th>Тип события</th>
            <th>Аналитик</th>
            <th style="width: 35px"> </th>
        </tr>
    </thead>
    <?php
        $obj = "-1";
        foreach ($eventList as $row){
            if ($obj != $row['ID_OBJECT']){
                $obj = $row['ID_OBJECT'];
                echo "<tr class='row-group' data-key='{$row['ID_OBJECT']}'>
                    <td colspan='6'><span class='fa fa-folder-open-o fa-lg expand'></span> {$row['OBJECT_NAME']}</td>
                </tr>";
            }
            echo "<tr class='row-item' data-group='{$row['ID_OBJECT']}' data-key='{$row['ID_OBJECT_EVENT']}' data-view='#{$row['ID_OBJECT_EVENT']}, ".$this->Common->strtodatetime($row['DATE_CREATE'])."'>
                <td style='width: 25px;'><span class='fa fa-square-o row-checkbox'></span></td>
                <td>".$this->Common->strtodatetime($row['DATE_CREATE'])."</td>
                <td class='displayName'><b>{$row['EVENT_NAME']}</b> {$row['MESSAGE']}</td>
                <td>{$row['USER_NAME']}</td><td class='control-row'>";
            if ($row['CONTROLLER_USER_NAME'] != ""){
                echo "<span title='Контролирующий: {$row['CONTROLLER_USER_NAME']}\n Дата: ".$this->Common->strtodatetime($row['DATE_CONTROLL'])."' 
                          class='fa fa-lg text-success fa-check-circle-o align-middle'></span>";
            };

            if ($row['COMMUNITY'] == $currentIdUser){
                echo "<i class='fa fa-lg fa-comment-o text-info'></i>";
            }elseif ($row['COMMUNITY'] != ''){
                echo "<i class='fa fa-lg fa-comment text-info'></i>";
            }
            echo "</td>";
        }
    ?>
</table>