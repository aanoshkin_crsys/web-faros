<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
            <i class="fa fa-lg fa-info-circle"></i> Информация
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="community-tab" data-toggle="tab" href="#community" role="tab" aria-controls="community" aria-selected="false">
            <i class="fa fa-lg fa-comment-o"></i> Переписка
        </a>
    </li>
</ul>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="container-fluid p-0">
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Контроль</label>
                <?php
                if ($eventInfo['DATE_CONTROLL'] == ''){
                    if ($canControl){
                        echo "<div class='edt-item' id='controlInfo'>Без контроля <div id='do-control' class='btn btn-sm btn-outline-success cursor-pointer'>Проверено</div></div>";
                    }else{
                        echo "<div class='edt-item' id='controlInfo'>Без контроля</div>";
                    }
                }else{
                    if ($canControl){
                        echo "<div class='edt-item text-success' id='controlInfo'>".$this->Auth->getAvatar($eventInfo['ID_CONTROLLER'], $eventInfo['CONTROLLER_AVATARNAME'], 'row-avatar')." {$eventInfo['CONTROLLER_USER_NAME']}, {$eventInfo['DATE_CONTROLL']} <div id='do-cancel-control' class='btn btn-sm btn-outline-secondary cursor-pointer'>Отмена</div></div>";
                    }else{
                        echo "<div class='edt-item text-success' id='controlInfo'>".$this->Auth->getAvatar($eventInfo['ID_CONTROLLER'], $eventInfo['CONTROLLER_AVATARNAME'], 'row-avatar')." {$eventInfo['CONTROLLER_USER_NAME']}, {$eventInfo['DATE_CONTROLL']}</div>";
                    }
                }
                ?>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Тип события</label>
                <div class="edt-item">
                    <select id="eventTypeDetail" class="form-control form-control-sm editeble" style="width: 100%" data-field="ID_EVENT" data-old="<?=$eventInfo['ID_EVENT'];?>">
                        <?
                        $group = "";
                        foreach($eventList as $row){
                            if ($group != $row['GROUP_NAME']){
                                $group = $row['GROUP_NAME'];
                                echo "<optgroup label='{$group}'>";
                            }

                            if ($eventInfo['ID_EVENT'] == $row['ID_EVENT']){
                                $selected = "selected";
                            }else{
                                $selected = "";
                            }
                            echo "<option {$selected} value='{$row['ID_EVENT']}'>{$row['EVENT_NAME']}</option>";
                        }
                        ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Текст события</label>
                <div class="edt-item">
                    <textarea id='eventMessage' class='form-control editeble' data-old="<?=$eventInfo['MESSAGE']?>" data-field="MESSAGE" placeholder='Введите примечание' rows='2'><?=$eventInfo['MESSAGE'];?></textarea>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Текст замечания</label>
                <div class="edt-item">
                    <?php
                        if (($eventInfo['COMPLETED']==1)){
                            $isInvalid = 'is-valid';
                        }else if ($eventInfo['MESSAGE_CONTROL'] != ''){
                            $isInvalid = 'is-invalid';
                        }else{
                            $isInvalid = '';
                        }

                        if (($eventInfo['COMPLETED']==1)){
                            echo "<textarea id='eventMessageControl' class='form-control {$isInvalid} editeble' data-old='{$eventInfo['MESSAGE_CONTROL']}' data-field='MESSAGE_CONTROL' placeholder='Введите замечание' rows='2' readonly>{$eventInfo['MESSAGE_CONTROL']}</textarea>";
                        }else if ($eventInfo['ID_USER'] == $currentIdUser){
                            echo "<div class='input-group'>
                                    <textarea id='eventMessageControl' class='form-control {$isInvalid} editeble' data-old='{$eventInfo['MESSAGE_CONTROL']}' data-field='MESSAGE_CONTROL' placeholder='Введите замечание' rows='2' readonly>{$eventInfo['MESSAGE_CONTROL']}</textarea>";
                            if ($eventInfo['MESSAGE_CONTROL'] != ''){
                                echo "<div class='input-group-append'>
                                        <span id='completed' class='input-group-text btn btn-success' data-field='COMPLETED'>Замечание<br>исправлено</span>
                                      </div>";
                            }
                            echo "</div>";
                        }else{
                            echo "<textarea id='eventMessageControl' class='form-control {$isInvalid} editeble' data-old='{$eventInfo['MESSAGE_CONTROL']}' data-field='MESSAGE_CONTROL' placeholder='Введите замечание' rows='2'>{$eventInfo['MESSAGE_CONTROL']}</textarea>";
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class='form-group'>
            <div id="attach-images" data-access="EVENT_EDIT" data-table-name="OBJECT_EVENTS" data-key="<?=$eventInfo['ID_OBJECT_EVENT'];?>">
            </div>
        </div>
        <div class='form-group'>
                <label class='col-form-label lbl-header'>Объект</label>
                <div class='edt-item'><?=$eventInfo['OBJECT_NAME'];?></div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Аналитик</label>
                <div class='edt-item'><?=$this->Auth->getAvatar($eventInfo['ID_USER'], $eventInfo['USER_AVATARNAME'], 'row-avatar')." ".$eventInfo['USER_NAME'];?></div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Дата обнаружения</label>
                <div class='edt-item'><?=$eventInfo['DATE_CREATE'];?></div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Дата события</label>
                <div class='edt-item'><?=$eventInfo['DATE_EVENT'];?></div>
            </div>
            <?php
            if ($eventInfo['PERSON'] != ''){
                echo "<div class='form-group'>
            <label class='col-form-label lbl-header'>Инициатор</label>
            <div class='edt-item'>{$eventInfo['PERSON']}</div>
        </div>";
            }
            ?>
        <?php
        if ($eventInfo['VIEW'] != ''){
            echo "<div class='form-group'>
            <label class='col-form-label lbl-header'>Представление события</label>
            <div class='edt-item'>{$eventInfo['VIEW']}</div>
        </div>";
        }
        ?>
    </div>
    <div class="tab-pane fade py-2" id="community" data-access="EVENT_EDIT" data-table-name="OBJECT_EVENTS" data-key="<?=$eventInfo['ID_OBJECT_EVENT'];?>" role="tabpanel" aria-labelledby="community-tab">
    </div>
</div>