<?php defined('BASEPATH') OR exit('No direct script access allowed');

echo "<tr class='row-item active' data-key='{$row['ID_REPORT']}' data-view='{$row['OBJECT_NAME']}, ".$this->Common->strtodatetime($row['DATE_CREATE'])."'>
        <td style='width: 25px;'><span class='fa fa-check-square-o row-checkbox'></span></td>
        <td>".$this->Auth->getAvatar($row['ID_AUTHOR'], $row['AUTHOR_AVATARNAME'], 'row-avatar-report')."</td>
        <td>{$row['AUTHOR_NAME']}</td>
        <td>".$this->Auth->getAvatar($row['ID_USER'], $row['USER_AVATARNAME'], 'row-avatar-report')."</td>
        <td>{$row['USER_NAME']}</td>
        <td>".$this->Common->strtodatetime($row['DATE_CREATE'])."</td>
        <td><b>{$row['OBJECT_NAME']}.</b> {$row['THEME']}</td><td class='control-row'>";
if ($row['ACCEPTED'] == "1"){
    echo "<span class='fa fa-lg text-success fa-check-circle-o align-middle'></span>";
};
     echo "<i class='fa fa-lg fa-comment-o text-info'></i>";
echo "</td>";
