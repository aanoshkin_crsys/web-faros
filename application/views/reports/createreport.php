<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<form class="container py-2">
    <div class="col-6 offset-3 card p-0">
        <div class="card-header">
            <h5>Формирование отчета</h5>
        </div>
        <div class="card-body">
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Объект</label>
                <select id="objectList" style="width: 100%" class="form-control editeble">
                    <?php
                    $group = "";
                    $selected = 'selected';
                    foreach($objectList as $row){
                        if ($group != $row['GROUP_NAME']){
                            $group = $row['GROUP_NAME'];
                            echo "<optgroup label='{$group}'>";
                        }
                        if ($selected != ''){
                            $selected = '';
                        }
                        echo "<option {$selected} value='{$row['ID_OBJECT']}'>{$row['OBJECT_NAME']}</option>";
                    }
                    ?>
                    </optgroup>
                </select>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Получатель</label>
                <select id="destUser" style="width: 100%" class="form-control editeble">
                </select>
            </div>
            <div class="form-group">
                <label class="col-form-label lbl-header">Период</label>
                <div class="input-group" id="date-range">
                    <input id="range" type="text" class="form-control form-control-sm" value="-Нет-" />
                    <div class="input-group-append">
                        <button id="range-clear" type="button" class="btn btn-sm btn-outline-secondary"><span class="fa fa-lg fa-times"></span></button>
                    </div>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Выберите отчет</label>
                <select id="reportType" style="width: 100%" class="form-control editeble" multiple="multiple">
                    <?php
                    $group = "";
                    $selected = 'selected';
                    foreach($reportList as $row){
                        if ($group != $row['GROUP_NAME']){
                            $group = $row['GROUP_NAME'];
                            echo "<optgroup label='{$group}'>";
                        }
                        if ($selected != ''){
                            $selected = '';
                        }
                        echo "<option {$selected} data-key='{$row['ID_REPORT_BUILDER']}' value='{$row['LINK']}'>{$row['NAME']}</option>";
                    }
                    ?>
                    </optgroup>
                </select>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Сопроводительный текст</label>
                <textarea id='reportMessage' class='form-control editeble' placeholder='Введите описание' rows='4'></textarea>
            </div>
            <a href="#" id="create" class="btn btn-primary btn-sm">Сформировать отчет</a>
            <a href="/reports/" id="back" class="btn btn-outline-dark btn-sm">В отчеты</a>
        </div>
    </div>
</form>
