<?php defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="container-fluid px-3">
    <b>Фильтр</b>
    <div class='form-group'>
        <label class='col-form-label p-0'>Период</label>
        <div class="input-group" id="date-range">
            <input id="range" type="text" class="form-control form-control-sm" value="-Нет-" />
            <div class="input-group-append">
                <button id="range-clear" type="button" class="btn btn-sm btn-outline-secondary"><span class="fa fa-lg fa-times"></span></button>
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Автор</label>
        <select id="authorFilter" class="form-control form-control-sm editeble" multiple="multiple" style="width: 100%">
            <?
            foreach($authorList as $row){
                echo "<option value='{$row['ID_ITEM']}'>{$row['ITEM_NAME']}</option>";
            }
            ?>
        </select>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Получатель</label>
        <select id="destFilter" class="form-control form-control-sm editeble" multiple="multiple" style="width: 100%">
            <?
            foreach($destList as $row){
                echo "<option value='{$row['ID_ITEM']}'>{$row['ITEM_NAME']}</option>";
            }
            ?>
            </optgroup>
        </select>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Объект</label>
        <select id="objectFilter" class="form-control form-control-sm editeble"  multiple="multiple" style="width: 100%">
            <?
            $group = "";
            foreach($objectList as $row){
                if ($group != $row['GROUP_NAME']){
                    $group = $row['GROUP_NAME'];
                    echo "<optgroup label='{$group}'>";
                }
                echo "<option value='{$row['ID_ITEM']}'>{$row['ITEM_NAME']}</option>";
            }
            ?>
            </optgroup>
        </select>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Подтверждение</label>
        <select id="acceptFilter" class="form-control form-control-sm editeble">
            <option selected value='-1'>-Нет-</option>
            <option value='0'>Не подтвержденные</option>
            <option value='1'>Подтвержденные</option>
        </select>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Сообщения</label>
        <select id="communityFilter" class="form-control form-control-sm editeble">
            <option selected value='-1'>-Нет-</option>
            <option value='0'>Без сообщений</option>
            <option value='1'>С сообщениями</option>
            <option value='2'>С непросмотренными сообщениями</option>
        </select>
    </div>
    <div class='form-group'>
        <label class='col-form-label p-0'>Отчет содержит</label>
        <div class="input-group">
            <input id="textFilter" type="text" class="form-control form-control-sm" />
        </div>
    </div>

</form>
<div class="row m-3">
    <button id="do-filter" class="btn btn-block btn-primary">
        <span class="fa fa-lg fa-filter"></span> Фильтровать
    </button>
    <button id="do-clear-filter" class="btn btn-block btn-outline-dark">
        <span class="fa fa-lg fa-trash-o"></span> Очистить
    </button>
</div>