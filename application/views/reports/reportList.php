<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<table class="table table-hover p-0 m-0 table-sm" id="reportTable">
    <thead>
    <tr>
        <th style="width: 25px"> </th>
        <th colspan="2">Автор</th>
        <th colspan="2">Получатель</th>
        <th>Дата</th>
        <th>Тема</th>
        <th style="width: 35px"> </th>
    </tr>
    </thead>
    <?php
    foreach ($reportList as $row){
        echo "<tr class='row-item' data-key='{$row['ID_REPORT']}' data-view='{$row['OBJECT_NAME']}, ".$this->Common->strtodatetime($row['DATE_CREATE'])."'>
                <td style='width: 25px;'><span class='fa fa-square-o row-checkbox'></span></td>
                <td>".$this->Auth->getAvatar($row['ID_AUTHOR'], $row['AUTHOR_AVATARNAME'], 'row-avatar-report')."</td>
                <td>{$row['AUTHOR_NAME']}</td>
                <td>".$this->Auth->getAvatar($row['ID_USER'], $row['USER_AVATARNAME'], 'row-avatar-report')."</td>
                <td>{$row['USER_NAME']}</td>
                <td>".$this->Common->strtodatetime($row['DATE_CREATE'])."</td>
                <td><b>{$row['OBJECT_NAME']}.</b> {$row['THEME']}</td><td class='control-row'>";
        if ($row['ACCEPTED'] == "1"){
            echo "<span class='fa fa-lg text-success fa-check-circle-o align-middle'></span>";
        };
        if ($row['COMMUNITY'] == $currentIdUser){
            echo "<i class='fa fa-lg fa-comment-o text-info'></i>";
        }elseif ($row['COMMUNITY'] != ''){
            echo "<i class='fa fa-lg fa-comment text-info'></i>";
        }
        echo "</td>";
    }
    ?>
</table>
