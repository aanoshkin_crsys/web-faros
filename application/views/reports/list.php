<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="col-10 p-1">
    <h3>Отчеты</h3>
    <div class="container-fluid container-full-height scrollbox" id="main-data">
        <div class="row">
            <div class="col-7 p-1">
                <div class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <div class="row">
                            <div class="col-6">
<!--                                <div class="btn-group">-->
<!--                                    <button class="btn btn-outline-success btn-sm" type="button" id="groupNew" title="Создать группу"><span class="fa fa-folder-o fa-lg"></button>-->
<!--                                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle" id="dropdownMove" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Переместить объект в группу"> <span class="fa fa-share-square-o fa-lg"></span></button>-->
<!--                                    <div id="objMove" class="dropdown-menu" aria-labelledby="dropdownMove">-->
<!--                                        <input class="form-control form-control-sm dropdown-filter" type="text" id="groupFind" placeholder="Быстрый поиск"/>-->
<!--                                        -->
<!--//                                        foreach ($objGroups as $group){-->
<!--//                                            echo "<a class='dropdown-item' href='#' data-key='{$group['ID_OBJECT_GROUP']}'>{$group['NAME']}</a>";-->
<!--//                                        }-->
<!--//                                        -->
<!--                                    </div>-->
<!--                                </div>-->
                                <?php
                                if ($this->Auth->userHasAccessRight(ACCESS_REPORT_BUILDER)){
                                    echo '<button class="btn btn-outline-success btn-sm" type="button" id="createReport" title="Создать"><span class="fa fa-plus fa-lg"></span> Создать</button>';
                                    echo ' <button class="btn btn-outline-primary btn-sm" type="button" id="removeReport" title="Удалить"><span class="fa fa-trash-o fa-lg"></span> </button>';
                                };
                                ?>
                            </div>
                            <div class="col-6">
                                <input type="text" id="find" class="form-control form-control-sm" placeholder="Поиск" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-2 object-card">
                        <div class="container-fluid">
                            <div class="row">
                                <table class="table table-hover p-0 m-0 table-sm" id="reportList">
                                    <?php
//                                    $group = -1;
//                                    foreach ($objList as $row){
//                                        if ($group != $row['ID_OBJECT_GROUP']){
//                                            $group = $row['ID_OBJECT_GROUP'];
//                                            $control = $row['CONTROLLER_NAME'];
//                                            if ($control != '') {
//                                                $control = " (Контроль: {$control})";
//                                            }
//                                            echo "<tr class='row-group' data-key='{$group}'>
//                                                    <td width='25px'><span class='expand fa d-inline fa-folder-open-o fa-lg'></span></td>
//                                                    <td colspan='3'><span class='displayName'>{$row['GROUP_NAME']} {$control}</span>";
//                                            if ($canEdit){
//                                                echo " [ <i class='fa fa-pencil changeGroup text-primary' title='Изменить'></i> |
//                                                             <i class='fa fa-arrow-up moveUp text-primary' title='Переместить выше'></i>
//                                                             <i class='fa fa-arrow-down moveDown text-primary' title='Переместить ниже'></i> |
//                                                             <i class='fa fa-trash removeGroup text-primary' title='Удалить'></i> ]
//                                                    ";
//                                            }
//                                            echo"</td></tr>";
//                                        }
//                                        if ($row['ID_OBJECT'] != '') {
//                                            echo "<tr class='row-item' data-key='{$row['ID_OBJECT']}' data-group='{$row['ID_OBJECT_GROUP']}'>
//                                                    <td width='25px'> </td>
//                                                    <td width='25px'><span class='row-checkbox fa d-inline fa-square-o'></span></td>
//                                                    <td class='displayName'>{$row['OBJECT_NAME']}</td>
//                                                    <td class='displayComment'>{$row['COMMENT']}</td>
//                                                    </tr>";
//                                        }
//                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-5 p-1">
                <div id="itemProperty" class="card">
                    <div class="card-header p-2" style="min-height: 48px;">
                        <h5><span id="itemName">Не выбрано</span></h5>
                    </div>
                    <div class="card-body p-2 object-card" id="itemDetails">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
