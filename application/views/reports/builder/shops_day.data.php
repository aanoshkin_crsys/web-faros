<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/assets/css/report.builder.css" rel="stylesheet">
</head>
<body>
<?php
foreach ($data as $row){
    $json    = json_decode($row['DATA'], true);
    $kassa   = $this->Common->getArrayValue($json, JSON_KASSA, '');
    $cheknum = $this->Common->getArrayValue($json, JSON_CHECKNUM, '');
    $imgs = $this->Attach->getList('OBJECT_EVENTS', $row['ID_OBJECT_EVENT']);
    echo "
            <table width='100%' class='red-table'>
                <tr>
                    <td>Событие #{$row['ID_OBJECT_EVENT']}</td>
                </tr>
                <tr>
                    <td>";

    foreach ($imgs as $img){
        echo "<img src='{$img['FNAME']}'/>";
    }

    echo "          </td>
                </tr>
                <tr>
                    <td>
                        <table width='100%' class='none-table'>
                            <tr>
                                <td width='10cm' rowspan='2'>{$row['MESSAGE']}</td>
                                <td width='3cm'><img src='/assets/images/reports/shopping.svg' height='5mm'/> {$kassa}</td>
                                <td><img src='/assets/images/reports/calendar.svg'  height='5mm'/> {$row['DATE_EVENT']}</td>
                            </tr>
                            <tr>
                                <td><img src='/assets/images/reports/barcode.svg' height='5mm'/> {$cheknum}</td>
                                <td><img src='/assets/images/reports/user.svg' height='5mm' /> {$row['PERSON']}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><br>
        ";
}
?>
</body>
</html>