<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <link href="/assets/css/report.builder.css" rel="stylesheet">
    </head>
    <body>
        <htmlpagefooter name="firstFooter">
            <div class="first-footer">
                Санкт-Петербург<br>
                ООО "СИ АНД АР СИСТЕМС"
            </div>
        </htmlpagefooter>
        <div class='first-page'>
            <table width="100%">
                <tr>
                    <td height="13cm"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td class='report-title'>
                        <?=$reportTitle; ?>
                    </td>
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td height="4cm"></td>
                </tr>
            </table>
            <div class="first-page-header">Объект:</div>
            <div class="first-page-value"><?=$nameObject;?></div>
            <div class="first-page-header">Дата составления:</div>
            <div class="first-page-value"><?=$currentDate;?></div>
            <div class="first-page-header">Период анализа:</div>
            <div class="first-page-value"><?=$period;?></div>
            <div class="first-page-header">Аналитик:</div>
            <div class="first-page-value"><?=$currentUser;?></div>
        </div>
        <sethtmlpagefooter name="firstFooter" value="on" />
    </body>
</html>