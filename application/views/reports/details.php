<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
            <i class="fa fa-lg fa-info-circle"></i> Информация
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="community-tab" data-toggle="tab" href="#community" role="tab" aria-controls="community" aria-selected="false">
            <i class="fa fa-lg fa-comment-o"></i> Переписка
        </a>
    </li>
</ul>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <form class="container-fluid p-0">
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Контроль</label>
                <?php
                if ($reportInfo['ACCEPTED'] == '0'){
                    if ($reportInfo['ID_USER'] == $currentIdUser){
                        echo "<div class='edt-item' id='acceptInfo'>Не подтвержден <div id='do-accept' class='btn btn-sm btn-outline-success'>Подтвердить</div></div>";
                    }else{
                        echo "<div class='edt-item' id='acceptInfo'>Не подтвержден</div>";
                    }
                }else{
                    echo "<div class='edt-item text-success' id='acceptInfo'>Подтверждено</div>";
                }
                ?>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Автор</label>
                <div class='edt-item'><?=$this->Auth->getAvatar($reportInfo['ID_AUTHOR'], $reportInfo['AUTHOR_AVATARNAME'], 'row-avatar')." ".$reportInfo['AUTHOR_NAME'];?></div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Объект</label>
                <?php
                if ($reportInfo['ID_AUTHOR'] == $currentIdUser){
                    ?>
                    <div class="edt-item">
                        <select id="objectList" style="width: 100%" class="form-control editeble" data-old="<?=$reportInfo['ID_OBJECT']?>" data-field="ID_OBJECT">
                            <?php
                            $group = "";
                            foreach($objectList as $row){
                                if ($group != $row['GROUP_NAME']){
                                    $group = $row['GROUP_NAME'];
                                    echo "<optgroup label='{$group}'>";
                                }
                                if ($reportInfo['ID_OBJECT'] == $row['ID_OBJECT']){
                                    $selected = 'selected';
                                }else{
                                    $selected = '';
                                }
                                echo "<option {$selected} value='{$row['ID_OBJECT']}'>{$row['OBJECT_NAME']}</option>";
                            }
                            ?>
                            </optgroup>
                        </select>
                    </div>
                    <?php
                }else{
                    echo "<div class='edt-item'>{$reportInfo['OBJECT_NAME']}</div>";
                }
                ?>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Получатель</label>
                <?php
                    if ($reportInfo['ID_AUTHOR'] == $currentIdUser){
                ?>
                    <div class="edt-item">
                        <select id="destUser" style="width: 100%" class="form-control editeble" data-old="<?=$reportInfo['ID_USER']?>" data-field="ID_USER">
                            <?php
                            foreach($clientList as $row){
                                if ($reportInfo['ID_USER'] == $row['ID_USER']){
                                    $selected = 'selected';
                                }else{
                                    $selected = '';
                                }
                                echo "<option {$selected} value='{$row['ID_USER']}'>{$row['USER_NAME']}</option>";
                            }
                            ?>
                        </select>
                    </div>
                <?php
                    }else{
                        echo "<div class='edt-item'>".$this->Auth->getAvatar($reportInfo['ID_USER'], $reportInfo['USER_AVATARNAME'], 'row-avatar')." {$reportInfo['USER_NAME']}</div>";
                    }
                ?>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Тема (заголовок)</label>
                <div class="edt-item">
                    <textarea id='reportTheme' class='form-control editeble' data-old="<?=$reportInfo['THEME']?>" data-field="THEME" placeholder='Введите тему' rows='2'><?=$reportInfo['THEME'];?></textarea>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Сопроводительный текст</label>
                <div class="edt-item">
                    <textarea id='reportMessage' class='form-control editeble' data-old="<?=$reportInfo['MESSAGE']?>" data-field="MESSAGE" placeholder='Введите описание' rows='4'><?=$reportInfo['MESSAGE'];?></textarea>
                </div>
            </div>
        </form>
        <div id="attachFiles" data-access="REPORT_EDIT" data-table-name="REPORTS" data-key="<?=$reportInfo['ID_REPORT'];?>">
        </div>
    </div>
    <div class="tab-pane fade py-2" id="community" data-access="REPORT_VIEW" data-table-name="REPORTS" data-key="<?=$reportInfo['ID_REPORT'];?>" role="tabpanel" aria-labelledby="community-tab">
    </div>
</div>