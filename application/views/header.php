<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
		<?php
			if (isset($title)){
				echo "<title>$title</title>";
			}else{
				echo "<title>CRSYS</title>";
			}
		?>

		<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/css/custom-css-bootstrap.css" rel="stylesheet">
        <link href="/assets/css/faros-office.css" rel="stylesheet">
		<link rel="icon" href="/favicon.ico">
		<?php
			//Подключение дополнительных ссылок
			if (isset($styles)){
				foreach($styles as $value){
					echo "<link href='/assets/css/{$value}.css?sum=".md5_file(base_url("/assets/css/{$value}.css"))."' rel='stylesheet'>";
				}
			}
		?> 
	</head>

<body class="bg-light">
  <div class="bg-light container-fluid">
    <div class="row ">
      <div class="col-12 bg-light p-0">
        <nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top p-0">
		  <a class="navbar-brand" href="/" style="margin-left: 30px;"><img src="/assets/images/logo.svg" height="45px" /></a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navHeader" aria-controls="navHeader" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span> </button>
          <div class="collapse navbar-collapse justify-content-end" id="navHeader">
			<div class="form-inline m-0"> 
            <?
                //Выводим верхнее меню
                if (isset($topmenu)){
                    foreach($topmenu as $menuItem){
                        echo "<a class='btn btn-outline-secondary' href='{$menuItem['href']}'>";
                        if ((isset($menuItem['icon'])) && ($menuItem['icon'] != '')){
                            echo "<i class='fa d-inline fa-lg {$menuItem['icon']}'></i>";
                        }
                        echo " {$menuItem['caption']}</a>";
                    }
                }

                if ($this->Auth->isAuth())
                    echo $this->Auth->currentAvatar('avatar topmenu-avatar')." <a class='btn btn-outline-secondary' href='/users/selfinfo'>".$this->Auth->getDisplayName()."</a>
                          <a class='btn btn-outline-secondary' href='/auth/dologout'><i class='fa d-inline fa-lg fa-power-off'></i>Выйти</a>";
                else
                    echo "<a class='btn btn-outline-secondary' href='#'><i class='fa d-inline fa-lg fa-user-circle-o'></i>".$this->Auth->getDisplayName()."</a>";
            ?>
			</div>		  
		  </div>
        </nav>
      </div>
    </div>
    <div class="row full-height">