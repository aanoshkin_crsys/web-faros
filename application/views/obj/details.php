<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
            <i class="fa fa-lg fa-info-circle"></i> Информация
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="obj-access-tab" data-toggle="tab" href="#obj-access" role="tab" aria-controls="obj-access" aria-selected="false">
            <i class="fa fa-lg fa-user-o"></i> Доступ к объекту
        </a>
    </li>
</ul>


<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

    <form class="container-fluid p-0">
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Уникальный номер</label>
                <div class='edt-item'><?=$infoObj['GUID'];?></div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Наименование</label>
                <div class="edt-item">
                    <textarea id='objName' class='form-control editeble' data-old="<?=$infoObj['NAME']?>" data-field="NAME" placeholder='Введите наименование объекта' rows='2'><?=$infoObj['NAME'];?></textarea>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-form-label lbl-header'>Примечание</label>
                <div class="edt-item">
                    <textarea id='objComment' class='form-control editeble' data-old="<?=$infoObj['COMMENT']?>" data-field="COMMENT"  placeholder='Введите примечание' rows='2'><?=$infoObj['COMMENT'];?></textarea>
                </div>
            </div>
    </form>
    <?php if (sizeof($infoProps)>0){ ?>
    <hr>
    <h5>Свойства объекта</h5>
    <table class="table table-hover p-0 m-0 table-sm" id="objProps">
    <?php
        $group = -1;
        foreach($infoProps as $row){
            if ($group != $row['ID_ITEM_GROUP']){
                $group = $row['ID_ITEM_GROUP'];
                echo "<tr class='row-group' data-key='{$group}'><td colspan='3'><span class='fa d-inline fa-folder-open-o fa-lg'></span> {$row['GROUP_NAME']}</td></tr>";
            }
            $value = $row['VALUE'];
            if ($row['COMMENT'] != ''){
                $value = "{$value} ({$row['COMMENT']})";
            }
            echo "<tr class='row-item' data-group='{$row['ID_ITEM_GROUP']}'>
                <td width='25px'> </td>
                <td>{$row['PROPERTY_NAME']}</td>
                <td title='Последнее обновление {$row['LAST_UPDATE']}'>{$value}</td>
                </tr>";
        }
    ?>
    </table>
    <?php }?>
    </div>
    <div class="tab-pane fade" id="obj-access" role="tabpanel" aria-labelledby="obj-access-tab">
        <table class="table table-hover p-0 m-0 table-sm" id="objUsers">
        <?php
            foreach ($infoUsers as $row) {
                echo "<tr><td width='25px'> </td><td width='30px'>".$this->Auth->getAvatar($row['ID_USER'], $row['AVATARNAME'], 'row-avatar')."</td><td>{$row['USERNAME']}</td></tr>";
            }
        ?>
        </table>
    </div>
</div>