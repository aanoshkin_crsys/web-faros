<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<form class="container-fluid p-0">
    <div class='form-group'>
        <label class='col-form-label lbl-header'>Наименование</label>
        <div class="edt-item">
            <textarea id='groupName' class='form-control editebleGroup' data-old="<?=$infoGroup['NAME']?>" data-field="NAME" placeholder='Введите наименование группы' rows='2'><?=$infoGroup['NAME'];?></textarea>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-form-label lbl-header'>Контроллирующий</label>
        <div class="edt-item">
            <select id="groupControl" class="form-control editebleGroup" data-old="<?=$infoGroup['ID_CONTROLLER']?>" data-field="ID_CONTROLLER">
                <?php
                echo "<option  value='-1'>-Нет-</option>";
                foreach($controlList as $row){
                    if ($infoGroup['ID_CONTROLLER'] == $row['ID_USER']){
                        $selected = 'selected';
                    }else{
                        $selected = '';
                    }
                    echo "<option {$selected} value='{$row['ID_USER']}'>{$row['USER_NAME']}</option>";
                }
                ?>
            </select>
        </div>
    </div>
</form>
