<div class="col-2 p-0" id="left-panel">
    <div id="left-menu-panel">
        <ul class="nav nav-pills flex-column left-menu" id='left-menu'>
        <?
            //Если есть значения последнего меню, то записваем его
            if ($this->session->userdata(COOKIE_SAVECACHE) && array_key_exists(COOKIE_LASTMENU, $this->session->userdata(COOKIE_SAVECACHE))){
                $lastMenu = $this->session->userdata(COOKIE_SAVECACHE)[COOKIE_LASTMENU];
            }else{
                //В противном случае, указываем пустую страницу
                $lastMenu = '';
            }
            $menuItems = $this->Auth->getUserPermissionGroups(TRUE);

            //Признак того, что необходимо установить первый пункт меню в качестве активного
            $first = ($lastMenu == '');
            //Проходим по всем элементам меню
            foreach($menuItems as $item){
                if ($this->Auth->userHasAccessGroup($item['SYSNAME']) ){
                    if (($lastMenu == $item['SYSNAME']) || ($first == true)){
                        $active = "active";
                        $first  = false;
                    }else{
                        $active = "";
                    }
                    echo "<li class='nav-item'><a href='#' id='{$item['SYSNAME']}' class='nav-link {$active}' data-menu-item='{$item['SYSNAME']}'>{$item['NAME']}</a></li>";
                }
            }
        ?>
        </ul>
        <hr>
    </div>
    <div id="filter-panel">
    </div>
</div>
