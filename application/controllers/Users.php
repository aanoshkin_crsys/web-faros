<?php

class Users extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Функция отображения окна со списком ролей, а также быстрый редактор
     */
    public function getList(){
        if ($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Настройка ролей',
                'styles' => array(),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu', 'users.list'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            if ($this->Auth->userHasAccessRight(ACCESS_USER_VIEW)){
                //Формируем данные для основной страницы
                $viewData['userList']   = $this->Auth->getList();
                $viewData['userGroups'] = $this->Auth->getUserGroups();
                $this->load->view('auth/list', $viewData);
            }else{
                echo ACCESS_DENIED;
            }
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }


    /**
     * Функция для выполнения в фоновом режиме, предназначена для отображения информации по объекту
     */
    public function selfInfo(){
        //Проверка на ошибку, если не авторизирована или нет прав на просмотр и не текущий пользователь
        if ($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Настройка личной информации',
                'styles' => array('jquery.fileupload'),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu', 'users.list', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload', 'users.selfinfo'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            //Формируем данные для основной страницы
            //Информация о пользователе
            $userData['userInfo']      = $this->Auth->getUserInfo($this->Auth->currentIdUser());
            //Получаем список прикрепленных объектов
            $userData['linkedObjects'] = $this->Auth->getLinkedObjects($this->Auth->currentIdUser());
            //Отображаем информацию
            $this->load->view('auth/selfinfo', $userData);
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }

    /*===============================Фоновые процедуры========================================*/

    /**
     * Функция для выполнения в фоновом режиме, предназначен для смены состояния активности пользователя
     */
    public function ajax_changeEnabled(){
        //Если у текущего пользователя была не авторизация или не имеются права на редактирование пользователей
        if ($this->Auth->isAuth()==false) {
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==false){
            //Сообщаем об отсутствии прав доступа
            echo ACCESS_DENIED;
        } else {
            //Получаем значения ПОСТ значений
            $keyItem = $this->input->post('keyItem');
            $status  = $this->input->post('enabled');
            $info    = $this->Auth->getUserInfo($keyItem);
            //В противном случае устанавливаем нужное значение
            $this->Auth->update($keyItem, 'ENABLED', $status);

            if ($status){
                $this->Common->sendMail($info['LOGIN'], "Активация учетной записи", "
 <b>Уважаемый {$info['NAME']}!</b><br>
 Ваша учетная запись на сайте CRSYS.TECH успешно активирована.<br>
 Для входа в личный кабинет Вы можете воспользоваться <a href='".BASE_URL('auth')."'>ссылкой</a>.<br>
 <br>
 С уважением, <br>
 Администрация CRSYS.TECH.");
            }
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для смены пароля
     */
    public function ajax_changePassword(){
        //Получаем ПОСТ значения, ключ пользователя, новый пароль и пароль для подтверждения
        $keyItem  = $this->input->post('keyItem');
        $newPass  = $this->input->post('newPassword');
        $confPass = $this->input->post('confirmPassword');
        if ($newPass == ''){
            //Проверка на пустой пароль
            echo "Нельзя указывать пустой пароль.";
        }else if ($newPass != $confPass) {
            //Проверка на совпадение паролей
            echo "Пароли не совпадают";
        }else if ($this->Auth->isAuth()==false){
            echo ACCESS_NOAUTH;
        }else if ($newPass == $this->session->userdata(COOKIE_AUTHDATA)['PASSWORD']){
            //Проверка на совпадение пароля со старым
            echo "Новый пароль совпадает со старым.";
        }else if (($keyItem != $this->session->userdata(COOKIE_AUTHDATA)['ID_USER']) && ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==false)){
            //Если ключ пользователя не равен текущему, и есть право на редактирование пользователей
            echo ACCESS_DENIED;
        }else{
            //Меняем пароль
            $this->Auth->changePassword($keyItem, $newPass);
            echo "Проль успешно изменен.";
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для отображения информации по объекту
     */
    public function ajax_details(){
        //Получае ПОСТ значение - ключ пользователя
        $idUser = $this->input->post('keyItem');
        //Проверка на ошибку, если не авторизирована или нет прав на просмотр и не текущий пользователь
        if ($this->Auth->isAuth()==false){
            echo ACCESS_NOAUTH;
        } else if (($this->Auth->userHasAccessRight(ACCESS_USER_VIEW)==false) && ($idUser != $this->session->userdata(COOKIE_AUTHDATA)['ID_USER'])){
            echo ACCESS_DENIED;
        }else{
            //Заполняем переменные
            //Информация о пользователе
            $userData['userInfo']      = $this->Auth->getUserInfo($idUser);
            //Право редактирования пользовтаеля
            $userData['canEdit']       = $this->Auth->userHasAccessRight(ACCESS_USER_EDIT) || ($idUser == $this->session->userdata(COOKIE_AUTHDATA)['ID_USER']);
            //Право на редактирование объектов
            $userData['canEditObject'] = $this->Auth->userHasAccessRight(ACCESS_USER_EDIT);
            //Получаем список ролей
            $userData['roleList']      = $this->Roles->getList();
            //Получаем список прикрепленных объектов
            $userData['linkedObjects'] = $this->Auth->getLinkedObjects($idUser);
            //Отображаем информацию
            $this->load->view('auth/details', $userData);
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для добавления объекта к пользователю
     */
    public function ajax_addLinkedObject(){
        //Проверяем на права доступа
        if ($this->Auth->isAuth()==false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_USER_OBJECTSET)==false){
            echo ACCESS_DENIED;
        }else{
            //Получаем ПОСТ значения
            $idUser   = $this->input->post('keyItem');
            $idObject = $this->input->post('keyObject');
            $this->Auth->addLinkedObject($idUser, $idObject);
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для удаления объекта у пользователя
     */
    public function ajax_removeLinkedObject(){
        //Проверяем на права доступа
        if ($this->Auth->isAuth()==false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_USER_OBJECTSET)==false){
            echo ACCESS_DENIED;
        }else{
            //Получаем ПОСТ значения
            $idUser   = $this->input->post('keyItem');
            $idObject = $this->input->post('keyObject');
            $this->Auth->removeLinkedObject($idUser, $idObject);
        }
    }

    /**
     * Функция обновления полей пользователя
     */
    public function ajax_updateUserField(){
        //Получаем ПОСТ значения
        $idUser = $this->input->post('keyItem');
        $field  = $this->input->post('field');
        $value  = $this->input->post('value');
        //Проверяем, чтобы поле было одно из тех, что можно менять, и ключ пользователя равен текущему или
        //имеется право на редактирование пользователя
        if ($this->Auth->isAuth()==false) {
            echo ACCESS_NOAUTH;
        }else if (in_array($field, array('LOGIN', 'NAME', 'PHONE', 'COMMENT')) &&
            (
                ($idUser == $this->session->userdata(COOKIE_AUTHDATA)['ID_USER']) ||
                ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==true)
            )
        ){
            $this->Auth->update($idUser, $field, $value);
            //Отдельно проверяем возможность установки роли, но для этого нужны соответствующие права
        }else if (($field == 'ID_USER_ROLE') && ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==true)){
            $this->Auth->update($idUser, $field, $value);
        }else{
            echo ACCESS_DENIED;
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для перемещения в другую группу
     */
    public function ajax_changeGroup(){
        //Получаем ПОСТ значения
        $idUsers = $this->input->post('keyItems');
        $newId   = $this->input->post('keyGroup');
        //Проверка на наличие прав редактирования
        if ($this->Auth->isAuth()==false) {
            echo ACCESS_NOAUTH;
        }else if ($idUsers == '-1'){
            echo DLG_NOSELECTION;
        }else if ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==true){
            $this->Auth->changeGroup($idUsers, $newId);
        }
    }

    /**
     * Функция для выполнения в фоновом режиме, предназначена для удаления (логического) пользователя
     */
    public function ajax_removeUsers(){
        //Получаем ПОСТ значения
        $idUsers = $this->input->post('keyItems');
        //Проверка на наличие прав редактирования
        if ($this->Auth->isAuth()==false) {
            echo ACCESS_NOAUTH;
        }else if ($idUsers == '-1'){
            echo DLG_NOSELECTION;
        }else if ($this->Auth->userHasAccessRight(ACCESS_USER_EDIT)==true){
            $this->Auth->remove($idUsers);
        }
    }

    public function ajax_uploadAvatar(){
        $config['upload_path']      = $_SERVER["DOCUMENT_ROOT"]."/avatars/".$this->Auth->currentIdUser()."/";
        $config['allowed_types']    = "jpg|jpeg|png";
        $config['max_size']	        = 2048;
        $config['max_width']        = 2500;
        $config['max_height']       = 2500;
        $config['encrypt_name']     = TRUE;

        if (!file_exists($config['upload_path'])){
            mkdir($config['upload_path'], 0777, true);
        }

        $this->load->library('upload', $config);

        if ($this->upload->do_upload() == false) {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = $this->upload->data();
            echo json_encode($data);
            $this->Auth->update($this->Auth->currentIdUser(), 'AVATARNAME', "{$data['file_name']}");
            $session = $this->session->userdata(COOKIE_AUTHDATA);
            $session['AVATARNAME'] = $data['file_name'];
            $this->session->set_userdata(COOKIE_AUTHDATA, $session);
        }
    }

    public function ajax_getCurrentAvatar(){
        $newClass = $this->input->post('newClass');
        echo $this->Auth->currentAvatar('cursor-pointer '.$newClass);
    }

}
