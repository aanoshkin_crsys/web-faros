<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth Описывает класс контроллера авторизации
 */
class Obj extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Базовая функция которая выполняется автоматически, при указании контроллера Auth
     */
    public function index()
    {
        if($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Настройка объектов',
                'styles' => array(),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu', 'obj.list'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_VIEW_LIST)) {
                //Формируем данные для основной страницы
                $viewData['objList']   = $this->Objects->getList();
                $viewData['objGroups'] = $this->Objects->getGroupList();
                $viewData['canEdit']   = $this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT);
                $this->load->view('obj/list', $viewData);
            }else{
                echo ACCESS_DENIED;
            }
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }

    /*===============================Фоновые процедуры========================================*/

    /**
     * Функция предназначена для установки свойств объекта
     */
    public function post_setProperty(){
        $idObject    = $this->input->post('id_object');
        $nameObject  = $this->input->post('name_object');
        $propGroup   = $this->input->post('prop_group');
        $propName    = $this->input->post('prop_name');
        $propValue   = $this->input->post('prop_value');
        $comment     = $this->input->post('comment');
        $ip          = $this->input->ip_address();

        $idObject = $this->Objects->updateObjectInfo($idObject, $nameObject, $ip);

        $this->Objects->setProperty($idObject, $propGroup, $propName, $propValue, $comment);
    }

    /**
     * Функия для выполнения в фоне, предназначена для получения информацции по объекту
     */
    public function ajax_details(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $key = $this->input->post('keyItem');
            //Получаем информацию об объекте
            $userData['infoObj']   = $this->Objects->getInfo($key);
            $userData['infoProps'] = $this->Objects->getInfoProps($key);
            $userData['infoUsers'] = $this->Objects->getInfoUsers($key);
            //Отображаем информацию
            $this->load->view('obj/details', $userData);
        }
    }

    /**
     * Функия для выполнения в фоне, предназначена для получения информацции по группе
     */
    public function ajax_detailsGroup(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $key = $this->input->post('keyItem');
            //Получаем информацию об объекте
            $userData['infoGroup'] = $this->Objects->getInfoGroup($key);
            $userData['controlList'] = $this->Auth->getUsersByRole(ROLE_CONTROLLER);
            //Отображаем информацию
            $this->load->view('obj/detailsGroup', $userData);
        }
    }

    /**
     * Функция для выполнения в фоне, предназначена смены группы у выделенных объектов
     */
    public function ajax_changeGroup(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keys  = $this->input->post('keyItems');
            $newId = $this->input->post('keyGroup');
            //Проверка на наличие прав редактирования
            if ($keys == '-1') {
                echo DLG_NOSELECTION;
            } else {
                $this->Objects->changeGroup($keys, $newId);
            }
        }
    }

    /**
     * Функция для выполнения в фоне, предназначена для логического удаления объектов
     */
    public function ajax_removeObjects(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false) {
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keys = $this->input->post('keyItems');
            //Проверка на наличие прав редактирования
            if ($keys == '-1') {
                echo DLG_NOSELECTION;
            } else {
                $this->Objects->remove($keys);
            }
        }
    }

    /**
     * Функция для выполнения в фоне, предназначена для обновления информации
     */
    public function ajax_update(){
        //Получаем ПОСТ значения
        $keyItem = $this->input->post('keyItem');
        $field   = $this->input->post('field');
        $value   = $this->input->post('value');
        //Выполняем оновление
        $this->Common->update(TABLE_OBJECTS, 'ID_OBJECT', $keyItem, $field, $value, ACCESS_OBJECT_EDIT);
    }

    /**
     * Функция для выполнения в фоне, предназначена для обновления информации
     */
    public function ajax_updateGroup(){
        //Получаем ПОСТ значения
        $keyItem = $this->input->post('keyItem');
        $field   = $this->input->post('field');
        $value   = $this->input->post('value');
        //Выполняем оновление
        $this->Common->update(TABLE_OBJECT_GROUPS, 'ID_OBJECT_GROUP', $keyItem, $field, $value, ACCESS_OBJECT_EDIT);
    }

    /**
     * Функция для выполнения в фоне, предназначена для перемещения группы вверх
     */
    public function ajax_moveUp(){
        if($this->Auth->isAuth() == false){
            redirect('/auth/index');
        }if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false) {
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $key   = $this->input->post('keyItem');
            //Выполняем оновление
            $this->Objects->groupMoveUp($key);
        }
    }

    /**
     * Функция для выполнения в фоне, предназначена для перемещения группы вниз
     */
    public function ajax_moveDown(){
        if($this->Auth->isAuth() == false){
            redirect('/auth/index');
        }if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false) {
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $key   = $this->input->post('keyItem');
            //Выполняем оновление
            $this->Objects->groupMoveDown($key);
        }
    }

    /**
     * Функция для вполнения в фоне, предназначена для удаления группы
     */
    public function ajax_removeGroup(){
        if($this->Auth->isAuth() == false){
            redirect('/auth/index');
        }if ($this->Auth->userHasAccessRight(ACCESS_OBJECT_EDIT) == false) {
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $key   = $this->input->post('keyItem');
            //Выполняем оновление
            echo $this->Objects->removeGroup($key);
        }
    }

    public function ajax_newGroup(){
        $this->Objects->newGroup();
    }
}
