<?php

class Roles extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Функция отображения окна со списком ролей, а также быстрый редактор
     */
    public function getList(){
        if ($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Настройка ролей',
                'styles' => array(),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu', 'roles.list'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            if ($this->Auth->userHasAccessRight(ACCESS_ROLE_VIEW)) {
                //Формируем данные для основной страницы
                $viewData['roleList'] = $this->Roles->getList();
                $this->load->view('roles/list', $viewData);
            }else{
                echo ACCESS_DENIED;
            }
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }

    /*===============================Фоновые процедуры========================================*/

    /**
     * Функция для выполнения в фоне, предназначена для отображения детальной информации по ролям
     */
    public function ajax_details(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_ROLE_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $idRole = $this->input->post('keyItem');
            //Заполняем переменные для передачи в страницу
            $userData['details']   = $this->Roles->getRoleRights($idRole);
            $userData['groupList'] = $this->Roles->getGroups();
            $userData['canEdit']   = $this->Auth->userHasAccessRight(ACCESS_ROLE_EDIT);

            $this->load->view('roles/details', $userData);
        }
    }

    /**
     * Функция для выполнения в фоне, предназначена для установки доступности прав у роли
     */
    public function ajax_setEnabled(){
        //Проверяем на наличие соответствующих прав
        if($this->Auth->isAuth() == false) {
            echo ACCESS_NOAUTH;
        }else if ($this->Auth->userHasAccessRight(ACCESS_ROLE_EDIT) == false){
            //Сообщаем об отсутствии прав
            echo ACCESS_DENIED;
        }else{
            //Получаем ПОСТ значения
            $idRole  = $this->input->post('keyItem');
            $idRight = $this->input->post('keyRight');
            $isOn    = $this->input->post('isOn') == 'true';
            //В зависимости от переключателя устанавливаем или снимаем указанное право у роли
            if ($isOn){
                $this->Roles->setRight($idRole, $idRight);
            }else{
                $this->Roles->unsetRight($idRole, $idRight);
            }
        }
    }

}
