<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth Описывает класс контроллера авторизации
 */
class Auth extends CI_Controller {

    /**
     * Базовая функция которая выполняется автоматически, при указании контроллера Auth
     */
    public function index()
    {
        //Считываем помощника для работы с куки
        $this->load->helper('cookie');
        //Если был сохранен и логин и пароль
        //Если нет сохраненных данных, то отображаем окно авторизации
        $this->showLoginForm();
    }

    /**
     * Функция отображения формы авторизации
     */
    public function showLoginForm(){
        $headerData = array(
            'title'=>'Вход в личный кабинет',
            'styles' => array(),
            'topmenu' => array()
        );

        $footerData = array(
            'scripts' => array('auth.login')
        );

        //Получаем сохраненные данные для автоматического входа
        $login = get_cookie(COOKIE_LOGIN);
        $pass  = get_cookie(COOKIE_PASSWORD);
        $viewData = array('login'=>$login, 'pass'=>$pass);

        $this->load->view('header', $headerData);
        $this->load->view('auth/login', $viewData);
        $this->load->view('footer', $footerData);
    }

    /**
     * Функция предназначена для выхода пользователя из системы
     */
    public function doLogout(){
        //Выполняем выход
        $this->Auth->doLogout();
        //Переходим на форму авторизации
        redirect('/auth/index');
    }

    public function registration(){
        $headerData = array(
            'title'=>'Регистрация нового пользователя',
            'styles' => array(),
            'topmenu' => array()
        );

        $footerData = array(
            'scripts' => array('auth.registration')
        );

        $viewData = array();

        $this->load->view('header', $headerData);
        $this->load->view('auth/registration', $viewData);
        $this->load->view('footer', $footerData);
    }

    public function restore_password(){
        $headerData = array(
            'title'=>'Восстановление пароля',
            'styles' => array(),
            'topmenu' => array()
        );

        $footerData = array(
            'scripts' => array('auth.restore')
        );

        $viewData = array();

        $this->load->view('header', $headerData);
        $this->load->view('auth/restore', $viewData);
        $this->load->view('footer', $footerData);
    }

    public function change_password(){
        $headerData = array(
            'title'=>'Восстановление пароля',
            'styles' => array(),
            'topmenu' => array()
        );

        $footerData = array(
            'scripts' => array('auth.change_password')
        );

        $viewData = array(
            'login' => $this->input->get('login'),
            'code'  => $this->input->get('code'),
        );

        $this->load->view('header', $headerData);
        $this->load->view('auth/change_password', $viewData);
        $this->load->view('footer', $footerData);
    }
    /*===============================Фоновые процедуры========================================*/

    /**
     * Функция для выполнения в фоновом режиме, предназначена для проверки авторизации
     */
    public function ajax_doLogin(){
        //Получаем  ПОСТ-значения логина и пароля
        $inLogin    = $this->input->post('inLogin');
        $inPassword = $this->input->post('inPassword');
        //Проверяем корректность авторизации
        if ($this->Auth->doAuth($inLogin, $inPassword)==True){
            //Если ошибок нет, то возвращаем пустую строку
            echo '';
        }else{
            //В противном случае возвращаем сообщение об ошибке
            echo 'Неверная пара Логин/Пароль';
        }
    }

    /**
     * Функция регистрации нового пользователя
     */
    public function ajax_register(){
        //Получаем  ПОСТ-значения логина и пароля
        $login   = $this->input->post('login');
        $fio     = $this->input->post('fio');
        $phone   = $this->input->post('phone');
        $comment = $this->input->post('comment');
        $pass    = $this->input->post('pass');
        $passc   = $this->input->post('passc');
        if ($login == ''){
            echo "Необходимо указать логин (e-mail).";
        } else if ($fio == ''){
            echo "Необходимо указать Фамилию, Имя, Отчество.";
        }else if ($pass == ''){
            echo "Необходимо указать пароль.";
        }else if ($pass != $passc){
            echo "Подтверждающий пароль должен совпадать.";
        }else{
            echo $this->Auth->register($login, $fio, $phone, $comment, $pass);
        }
    }

    public function ajax_restore(){
        //Получае значение ПОСТ запроса
        $login     = $this->input->post('login');
        $randomStr = $this->Common->generateRandomString();
        $info      = $this->Auth->getUserInfo($this->Auth->getUserIdByLogin($login), false);

        echo $this->Auth->restore($login, $randomStr);
        $this->Common->sendMail($login, 'Восстановление пароля', "
<b>Уважаемый {$info['NAME']}!</b><br>
От имени Вашей учетной записи был произведен запрос на смену(восстановления) пароля.<br>
Если Вы не инициировали данную функцию, ничего делать не нужно. <br> 
Для продолжения восстановления пароля перейдите по <a href='".BASE_URL("/auth/change_password?login={$login}&code={$randomStr}")."'>этой ссылке</a> или укажите секретный код указанный ниже в поле <b>[Секретный код]</b>: <br>
{$randomStr} <br>
Далее укажите пароль и подтверждающий пароль и нажмите кнопку <b>[Восстановить]</b>
<br>
С уважением, <br>
Администрация CRSYS.TECH.");
    }

    public function ajax_changepassword(){
        //Получаем значение ПОСТ запроса
        $code = $this->input->post('code');
        $pass = $this->input->post('pass');

        echo $this->Auth->restore_password($code, $pass);
    }
}
