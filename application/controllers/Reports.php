<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Основная функция вызова контроллера
     */
    public function index()
    {
        //Если не было аторизировано, то отправляем на вторизацию
        if ($this->Auth->isAuth()==false){
            redirect('/auth/index');
        }else{
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Отчеты',
                'styles' => array('daterangepicker', 'select2.min', 'jquery.fileupload'),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('moment.min', 'daterangepicker', 'select2', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload', 'main.menu', 'reports.list',  'community', 'attaches'),
            );

            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            //Формируем данные для основной страницы
            $viewData = array();
            $this->load->view('reports/list', $viewData);

            $this->load->view('footer', $footerData);
        }
    }

    public function createReport(){
        //Если не было аторизировано, то отправляем на вторизацию
        if ($this->Auth->isAuth()==false){
            redirect('/auth/index');
        }else{
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Формирование нового отчета',
                'styles' => array('daterangepicker', 'select2.min'),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('moment.min', 'daterangepicker', 'select2', 'main.menu', 'reports.create'),
            );

            //Отображаем страницу
            $this->load->view('header', $headerData);
//            $this->load->view('main/desktop');
            //Формируем данные для основной страницы
            //Получаем ПОСТ значения
            $viewData = array();
            $viewData['clientList']    = $this->Auth->getUsersByRole(ROLE_CLIENT);
            $viewData['objectList']    = $this->Events->getObjectList($this->Auth->currentIdUser());
            $viewData['reportList']    = $this->Reports->getReportTypeList();
            $this->load->view('reports/createreport', $viewData);
            $this->load->view('footer', $footerData);
        }
    }


    /* =============== Отчеты ===============*/

    public function addReport(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_REPORT_BUILDER) == false){
            echo ACCESS_DENIED;
        } else {
            $idObject   = $this->input->post('idObject');
            $idDest     = $this->input->post('idDest');
            $dest       = $this->Auth->getUserById($idDest);
            $theme      = $this->input->post('theme');
            $message    = $this->input->post('message');
            $files      = $this->input->post('files');
            $displays   = $this->input->post('displays');
            $idAuthor   = $this->Auth->currentIdUser();
            $newId      = $this->Reports->addReport($idObject, $idDest, $idAuthor, $theme, $message);
            foreach ($files as $idx=>$file){
                $this->Attach->linkFile(TABLE_REPORTS, $newId, $idAuthor, $file, $displays[$idx]);
            }
            $this->Common->sendMail($dest, $theme, "Здравствуйте, <br> 
                Вам сформирован новый отчет с коментарием '{$theme} {$message}' <br>
                Для просмотра отчетов, зайдите в личный кабинет по <a href='".base_url()."'>этой ссылке</a> в раздел <b>Отчеты</b>.");
        }
    }

    /**
     * Функция формирования ежедневного отчета по магазинам
     */
    public function shops_day(){
        $this->load->library('pdf_generator');
        //Получение ПОСТ значений
        $idObject = $this->input->post('idObject');
        $nameObject = $this->input->post('nameObject');
        $pathname = $this->input->post('pathname');
        $filename = $this->input->post('filename');
        $dateStart = $this->input->post('dateStart');
        $dateFinish = $this->input->post('dateFinish');
        if ($dateStart == $dateFinish){
            $period="за ".$dateStart;
        }else{
            $period="с {$dateStart} по {$dateFinish}";
        }

        $pdf = $this->pdf_generator->beginReport();
        $pdf->AddPage("P", '', '', '', '', 0, 0, 0, 5, 0, 5);
        $data = array(
            'reportTitle' => 'Сводный ежедневный отчет<br> по видам нарушений',
            'nameObject' => $nameObject,
            'period' => $period,
            'currentDate' => $this->Common->strtodate(""),
            'currentUser' => $this->Auth->getUserName(),
        );
        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.first.php", $data, true));

        $events = $this->Events->getEventGroupList($this->Auth->currentIdUser());

        foreach ($events as $event) {
            $data = array(
                "data" => $this->Builder->getEventDay($idObject, $dateStart, $dateFinish, "'{$event['SYSNAME']}'"),
            );
            if (count($data['data'])>0) {
                $pdf->SetHTMLHeader($this->Builder->getHeader($event['GROUP_NAME']));
                $pdf->AddPage("P", '', '', '', '', 0, 0, 20, 15, 0, 5);
                $pdf->SetHTMLFooter($this->Builder->getFooter());

                $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.data.php", $data, true));
            }
        }

        if (count($events) > 0){
            $pdf->SetHTMLHeader($this->Builder->getHeader('Статистические данные'));
            $pdf->AddPage("P", '', '', '', '', 0, 0, 20, 15, 0, 5);
            $pdf->SetHTMLFooter($this->Builder->getFooter());
            foreach ($events as $event) {
                $data = array();
                $data2 = array();
                $dataAxis = array();

                $dataTemp = $this->Builder->getEventCnt($idObject, $dateStart, $dateFinish, "'{$event['SYSNAME']}'");
                foreach($dataTemp as $row){
                    $data[$row['ITEM_NAME']] = $row['QNT'];
                }

                $date = new DateTime($dateFinish);
                $date->modify('-6 month');

                $dataTemp = $this->Builder->getEventCntByMonth($idObject, $date->format(DATE_FORMAT), $dateFinish, "'{$event['SYSNAME']}'");
                foreach($dataTemp as $row){
                    $data2[] = $row['QNT'];
                    $dataAxis[] = $row['GROUPDATE'];
                }
                $data2 = array($event['GROUP_NAME'] => $data2);

                $pdf->WriteHTML("<table class='chart-table'><tr><td>");
                if ((count($data)>0) || (count($data2) > 0)) {
                    $pdf->WriteHTML("<h3>{$event['GROUP_NAME']}</h3>");
                }
                if (count($data2)>0){
                    $pdf->WriteHTML("<p>Статистика по количеству нарушений за последние 6 месяцев</p><br>");
                    $pdf->WriteHTML($this->Chart->lines("lines-{$event['SYSNAME']}", 700, 150, $data2, array(
                        'left_offset' => 30,
                         'min_value' => 0,
                         'max_value' => 3,
                        'xaxis' => $dataAxis,
                    )));
                    $pdf->WriteHTML("<br><br>");
                }
                if (count($data)>0){
                    $pdf->WriteHTML("<p>Статистика по количеству нарушений по ответственным</p><br>");
                    $pdf->WriteHTML($this->Chart->hystogram("hystogram-{$event['SYSNAME']}", 700, 0, $data));
                }
                $pdf->WriteHTML("</table>");
                $pdf->WriteHTML("<br><br>");
            }
        }


        $this->pdf_generator->endReport($pdf, $pathname, $filename);
    }

    public function shops_day_finance(){
        $this->load->library('pdf_generator');
        //Получение ПОСТ значений
        $idObject = $this->input->post('idObject');
        $nameObject = $this->input->post('nameObject');
        $pathname = $this->input->post('pathname');
        $filename = $this->input->post('filename');
        $dateStart = $this->input->post('dateStart');
        $dateFinish = $this->input->post('dateFinish');
        if ($dateStart == $dateFinish){
            $period="за ".$dateStart;
        }else{
            $period="с {$dateStart} по {$dateFinish}";
        }

        $pdf = $this->pdf_generator->beginReport();
        $pdf->AddPage("P", '', '', '', '', 0, 0, 0, 5, 0, 5);
        $data = array(
            'reportTitle' => 'Ежедневный отчет нарушений <br> экономического характера ритейла',
            'nameObject' => $nameObject,
            'period' => $period,
            'currentDate' => $this->Common->strtodate(""),
            'currentUser' => $this->Auth->getUserName(),
        );
        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.first.php", $data, true));

        $pdf->SetHTMLHeader($this->Builder->getHeader("Экономические нарушения", "", TRUE));
        $pdf->SetHTMLFooter($this->Builder->getFooter(), "", TRUE);
        $pdf->AddPage("P", '', '', '', '', 0, 0, 20, 15, 0, 5);

        $data = array(
            "data" => $this->Builder->getEventDay($idObject, $dateStart, $dateFinish, "'EVENT_FINANCE'"),
        );

        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.data.php", $data, true));

        $this->pdf_generator->endReport($pdf, $pathname, $filename);
    }

    public function shops_day_discipline(){
        $this->load->library('pdf_generator');
        //Получение ПОСТ значений
        $idObject = $this->input->post('idObject');
        $nameObject = $this->input->post('nameObject');
        $pathname = $this->input->post('pathname');
        $filename = $this->input->post('filename');
        $dateStart = $this->input->post('dateStart');
        $dateFinish = $this->input->post('dateFinish');
        if ($dateStart == $dateFinish){
            $period="за ".$dateStart;
        }else{
            $period="с {$dateStart} по {$dateFinish}";
        }

        $pdf = $this->pdf_generator->beginReport();
        $pdf->AddPage("P", '', '', '', '', 0, 0, 0, 5, 0, 5);
        $data = array(
            'reportTitle' => 'Ежедневный отчет нарушений <br> дисциплинарного характера ритейла',
            'nameObject' => $nameObject,
            'period' => $period,
            'currentDate' => $this->Common->strtodate(""),
            'currentUser' => $this->Auth->getUserName(),
        );
        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.first.php", $data, true));

        $pdf->SetHTMLHeader($this->Builder->getHeader("Дисциплинарные нарушения", "", TRUE));
        $pdf->SetHTMLFooter($this->Builder->getFooter(), "", TRUE);
        $pdf->AddPage("P", '', '', '', '', 0, 0, 20, 15, 0, 5);

        $data = array(
            "data" => $this->Builder->getEventDay($idObject, $dateStart, $dateFinish, "'EVENT_DISCIPLINE'"),
        );

        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.data.php", $data, true));

        $this->pdf_generator->endReport($pdf, $pathname, $filename);
    }

    public function shops_day_operations(){
        $this->load->library('pdf_generator');
        //Получение ПОСТ значений
        $idObject = $this->input->post('idObject');
        $nameObject = $this->input->post('nameObject');
        $pathname = $this->input->post('pathname');
        $filename = $this->input->post('filename');
        $dateStart = $this->input->post('dateStart');
        $dateFinish = $this->input->post('dateFinish');
        if ($dateStart == $dateFinish){
            $period="за ".$dateStart;
        }else{
            $period="с {$dateStart} по {$dateFinish}";
        }

        $pdf = $this->pdf_generator->beginReport();
        $pdf->AddPage("P", '', '', '', '', 0, 0, 0, 5, 0, 5);
        $data = array(
            'reportTitle' => 'Ежедневный отчет нарушений <br> операционного характера ритейла',
            'nameObject' => $nameObject,
            'period' => $period,
            'currentDate' => $this->Common->strtodate(""),
            'currentUser' => $this->Auth->getUserName(),
        );
        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.first.php", $data, true));

        $pdf->SetHTMLHeader($this->Builder->getHeader("Дисциплинарные нарушения", "", TRUE));
        $pdf->SetHTMLFooter($this->Builder->getFooter(), "", TRUE);
        $pdf->AddPage("P", '', '', '', '', 0, 0, 20, 15, 0, 5);

        $data = array(
            "data" => $this->Builder->getEventDay($idObject, $dateStart, $dateFinish, "'EVENT_OPERATION'"),
        );

        $pdf->WriteHTML($this->load->view("/reports/builder/shops_day.data.php", $data, true));

        $this->pdf_generator->endReport($pdf, $pathname, $filename);
    }

    /* =============== Фоновые запросы ===============*/

    /**
     * Функция заполнения списка пользователей, связанных с обектом
     */
    public function ajax_fillDest()
    {
        if ($this->Auth->isAuth() == false) {
            echo ACCESS_NOAUTH;
        } else{
            //Получает значения ПОСТ запросов
            $idObject = $this->input->post('idObject');
            $list = $this->Objects->getInfoUsers($idObject);
            foreach($list as $row){
                echo "<option value='{$row['ID_USER']}'>{$row['USERNAME']}</option>";
            }
        }
    }

    /**
     * Функция фонового выполнения, предназначена для отображения фильтра
     */
    public function ajax_getFilter(){
        $idUser = $this->Auth->currentIdUser();
        $viewData['authorList'] = $this->Reports->getAuthorList($idUser);
        $viewData['destList']   = $this->Reports->getDestList($idUser);
        $viewData['objectList'] = $this->Reports->getObjectList($idUser);
        $this->load->view('reports/filter', $viewData);
    }

    /**
     * Функция фонового выполнения, предназначена для получения списка событий
     */
    public function ajax_getList(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_REPORT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $idUser          = $this->Auth->currentIdUser();
            $authorFilter    = $this->input->post('authorFilter');
            $destFilter      = $this->input->post('destFilter');
            $objectFilter    = $this->input->post('objectFilter');
            $acceptFilter    = $this->input->post('acceptFilter');
            $communityFilter = $this->input->post('communityFilter');
            $textFilter      = $this->input->post('textFilter');
            $dateStart       = $this->input->post('dateStart');
            $dateFinish      = $this->input->post('dateFinish');
            //Получаем информацию об объекте
            $viewData['reportList']    = $this->Reports->getList($idUser, $authorFilter, $destFilter, $acceptFilter, $objectFilter, $communityFilter, $textFilter, $dateStart, $dateFinish);
            $viewData['currentIdUser'] = $idUser;
            //Отображаем информацию
            $this->load->view('reports/reportList', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для получения списка событий
     */
    public function ajax_details(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_REPORT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $idUser          = $this->Auth->currentIdUser();
            $keyItem         = $this->input->post('keyItem');
            //Получаем информацию об объекте
            $viewData['reportInfo']    = $this->Reports->getInfo($keyItem);
            $viewData['clientList']    = $this->Auth->getUsersByRole(ROLE_CLIENT);
            $viewData['objectList']    = $this->Objects->getList();
            $viewData['currentIdUser'] = $idUser;
            //Отображаем информацию
            $this->load->view('reports/details', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для изменения данных в таблице
     */
    public function ajax_update(){
        //Получаем ПОСТ значения
        $keyItem = $this->input->post('keyItem');
        $field   = $this->input->post('field');
        $value   = $this->input->post('value');

        $this->Common->update(TABLE_REPORTS, 'ID_REPORT', $keyItem, $field, $value, ACCESS_REPORT_EDIT);
    }

    /**
     * Функция фонового выполнения, предназначена для обновления одной строки отчета
     */
    public function ajax_rowRefresh(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_REPORT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keyItem         = $this->input->post('keyItem');
            //Получаем информацию об объекте
            $viewData['row']    = $this->Reports->getInfo($keyItem);
            //Отображаем информацию
            $this->load->view('reports/rowInfo', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для принятия отчета
     */
    public function ajax_accept(){
        $keyItem = $this->input->post('keyItem');
        $this->Common->update(TABLE_REPORTS, 'ID_REPORT', $keyItem, 'ACCEPTED', 1, ACCESS_REPORT_VIEW);
    }

    /**
     * Функция фонового выполнения, предназначена для удаления отчетов
     */
    public function ajax_remove(){
        $keys = explode(',', $this->input->post('keyItems'));
        foreach ($keys as $key){
            $this->Reports->remove($key, 1);
        }
    }
}
