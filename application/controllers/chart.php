<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth Описывает класс контроллера авторизации
 */
class Chart extends CI_Controller {

    /**
     * Базовая функция которая выполняется автоматически, при указании контроллера Auth
     */
    public function index()
    {
        //Формируем данные для заголовка
        $headerData = array(
            'title'=>'Проба графиков',
            'styles' => array(),
            'topmenu' => array()
        );
        //Формируем данные для подвала
        $footerData = array(
        );

        //Отображаем страницу
        echo $this->load->view('header', $headerData, true);
        echo "<br><br><br><br>";
        echo $this->Chart->lines('line', 1000, 300,
            array(
                "one"=>array(10, 4, 15, 20, 3),
                "one"=>array(3, 7.5, 1, 15 )
            ),
            array(
                'xaxis'=>array('one', 'two', 'three', 'four', 'five'),
                'css'=>'charts',
            )
        );
        echo $this->load->view('footer', $footerData, true);
    }

}
