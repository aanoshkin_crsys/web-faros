<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth Описывает класс контроллера авторизации
 */
class Events extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Базовая функция которая выполняется автоматически, при указании контроллера Events
     */
    public function index()
    {
        if($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Список событий',
                'styles' => array('daterangepicker', 'select2.min', 'jquery.fileupload'),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('moment.min', 'daterangepicker', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload', 'select2', 'main.menu', 'events.list', 'community', 'attaches'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            if ($this->Auth->userHasAccessRight(ACCESS_EVENT_VIEW)) {
                //Формируем данные для основной страницы
                $viewData[] = array();
                $this->load->view('events/list', $viewData);
            }else{
                echo ACCESS_DENIED;
            }
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }

    /**
     * Функция проверки событий
     */
    public function checkEvents()
    {
        if($this->Auth->isAuth()){
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Проверка событий',
                'styles' => array('daterangepicker', 'select2.min', 'jquery.fileupload'),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('moment.min', 'daterangepicker', 'main.menu', 'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload', 'select2', 'events.checklist', 'community', 'attaches'),
            );
            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop');
            if ($this->Auth->userHasAccessRight( ACCESS_EVENT_CONTROL)) {
                //Формируем данные для основной страницы

                $viewData[] = array();
                $this->load->view('events/checklist', $viewData);
            }else{
                echo ACCESS_DENIED;
            }
            $this->load->view('footer', $footerData);
        }else{
            redirect('/auth/index');
        }
    }

    /*===============================Фоновые процедуры========================================*/

    /**
     * Функция заполнения события универсальный
     */
    public function post_addEvent(){
        //Получаем ПОСТ значения
        $idObject    = $this->input->post('id_object');     //Идентификатор объекта
        $nameObject  = $this->input->post('name_object');   //Наименование объекта
        $ip          = $this->input->ip_address();                //IP адрес объекта
        $idRecord    = $this->input->post('id_record');     //Ключ записи на сервере
        $eventGroup  = $this->input->post('event_group');   //Наименование группы событий
        $eventType   = $this->input->post('event_type');    //Наименование события
        $eventText   = $this->input->post('event_text');    //Текст события
        $person      = $this->input->post('person');        //Персона совершившая событие
        $analitic    = $this->input->post('analitic');      //Аналитик, нашедший ошибку
        $jsonData    = $this->input->post('json_data');     //Содержимое события в JSON
        $viewData    = $this->input->post('view_data');     //Содержимое события для просмотра
        $dateAnalitic= $this->input->post('date_analitic'); //Дата когда аналитик нашел нарушение
        $dateEvent   = $this->input->post('date_event');    //Дата наршения
        //Сохраняем событие
        $this->Events->addEvent($idObject, $idRecord, $nameObject, $ip,$eventGroup, $eventType, $eventText, $person, $analitic, $dateAnalitic, $dateEvent, $viewData, $jsonData);
    }

    /**
     * Функция заполнения события от ККО
     */
    public function post_addEventKKO(){
        //Получаем ПОСТ значения
        $idObject    = $this->input->post('id_object');     //Идентификатор объекта
        $nameObject  = $this->input->post('name_object');   //Наименование объекта
        $ip          = $this->input->ip_address();                //IP адрес объекта
        $idRecord    = $this->input->post('id_record');     //Ключ записи на сервере
        $eventGroup  = $this->input->post('event_group');   //Наименование группы событий
        $eventType   = $this->input->post('event_type');    //Наименование события
        $eventText   = $this->input->post('event_text');    //Текст события
        $person      = $this->input->post('person');        //Персона совершившая событие
        $analitic    = $this->input->post('analitic');      //Аналитик, нашедший ошибку
        $checkNum    = $this->input->post('check_num');     //Номер чека
        $kassaNum    = $this->input->post('kassa_num');     //Номер Кассы
        $checkue     = $this->input->post('checkue');       //Содержимое чека
        $dateAnalitic= $this->input->post('date_analitic'); //Дата когда аналитик нашел нарушение
        $dateCheck   = $this->input->post('date_check');    //Дата чека
        //Преобразуем содержимое чека в таблицу, где поля разделены между собой ";", а строки Enter
        $checkue = str_replace(';', '</td><td>', $checkue);
        $checkue = str_replace(chr(13).chr(10), '</td></tr><tr><td>', $checkue);
        $checkue = '<table><tr><td>'.$checkue.'</td></tr></table>';

        //Формируем данные для JSON объекта
        $jsonData = array(
            JSON_CHECKNUM => $checkNum,
            JSON_KASSA => $kassaNum
        );
        //Кодируем JSON объект
        $jsonData = json_encode($jsonData);
        //Формируем представлеие чека
        $viewData = "Кассир:<b>{$person}</b><br>Касса №<b>{$kassaNum}</b><br>Чек №<b>{$checkNum}</b> от <b>".$this->Common->strtodatetime($dateCheck)."</b><br>{$checkue}";
        //Сохраняем событие
        $this->Events->addEvent($idObject, $idRecord, $nameObject, $ip,$eventGroup, $eventType, $eventText, $person, $analitic, $dateAnalitic, $dateCheck, $viewData, $jsonData);
    }

    /**
     * Фунция предназначена для заливки файлов на сервер
     */
    public function post_uploadAttach(){
        $idObject    = $this->input->post('id_object');     //Идентификатор объекта
        $nameObject  = $this->input->post('name_object');   //Наименование объекта
        $ip          = $this->input->ip_address();                //IP адрес объекта
        $idRecord    = $this->input->post('id_record');     //Ключ записи на сервере
        $tableName   = 'OBJECT_EVENTS';
        //Заполняем значения конфигураций
        $config['upload_path']      = $_SERVER["DOCUMENT_ROOT"]."/attachment/{$tableName}/".date('Ymd')."/";
        $config['allowed_types']	= '*';
        $config['max_size']	        = 20480;
        $config['encrypt_name']     = TRUE;
        //Создаем папку, если не создана
        if (!file_exists($config['upload_path'])){
            mkdir($config['upload_path'], 0777, true);
        }
        //Подключаем библиотеку для залития на сайт
        $this->load->library('upload', $config);
        //Записываем информацию по объекту
        $idObject   = $this->Objects->updateObjectInfo($idObject, $nameObject, $ip);
        //Определяем ключ записи, для которой произведется запись на сайт
        $keyItem = $this->Common->getSQL('select ID_OBJECT_EVENT from OBJECT_EVENTS where ID_OBJECT = ? and ID_RECORD = ?',
            array($idObject, $idRecord));
        //Если ключ не определен или не найден, то
        if (!((isset($keyItem)) &&($keyItem > 0))){
            echo "Не найдена пара Объект/Запись ({$idObject}, {$idRecord})";
        }else if ($this->upload->do_upload() == false) {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = $this->upload->data();
            echo json_encode($data);
            $this->Common->addAttach($tableName, $keyItem, date('Ymd')."/".$data['file_name'], $data['orig_name'], $data['file_size']);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для отображения фильтра
     */
    public function ajax_getFilter(){
        $idUser = $this->Auth->currentIdUser();
        $viewData['analiticList'] = $this->Events->getAnaliticList($idUser);
        $viewData['eventList']    = $this->Events->getEventList($idUser);
        $viewData['objectList']   = $this->Events->getObjectList($idUser);
        $this->load->view('events/filter', $viewData);
    }

    /**
     * Функция фонового выполнения, предназначена для получения списка событий
     */
    public function ajax_getList(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $idUser         = $this->Auth->currentIdUser();
            $idAnalitic     = $this->input->post('idAnalitic');
            $idEventGroups  = $this->input->post('idEventGroups');
            $idEvent        = $this->input->post('idEvent');
            $idObject       = $this->input->post('idObject');
            $control        = $this->input->post('control');
            $commFilter     = $this->input->post('commFilter');
            $viewText       = $this->input->post('viewText');
            $rangeStart     = $this->input->post('rangeStart');
            $rangeFinish    = $this->input->post('rangeFinish');
            //Получаем информацию об объекте
            $viewData['eventList'] = $this->Events->getList($idUser, $rangeStart, $rangeFinish, $idAnalitic, $idEventGroups, $idEvent, $idObject, $control, $commFilter, $viewText);
            $viewData['currentIdUser'] = $idUser;
            $viewData['eventTypes'] = $this->Common->getEventList();
            //Отображаем информацию
            $this->load->view('events/eventList', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для получения списка событий
     */
    public function ajax_getCheckList(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $idUser         = $this->Auth->currentIdUser();
            $idAnalitic     = $this->input->post('idAnalitic');
            $idEventGroups  = $this->input->post('idEventGroups');
            $idEvent        = $this->input->post('idEvent');
            $idObject       = $this->input->post('idObject');
            $control        = $this->input->post('control');
            $commFilter     = $this->input->post('commFilter');
            $viewText       = $this->input->post('viewText');
            $rangeStart     = $this->input->post('rangeStart');
            $rangeFinish    = $this->input->post('rangeFinish');
            //Получаем информацию об объекте
            $viewData['eventList'] = $this->Events->getList($idUser, $rangeStart, $rangeFinish, $idAnalitic, $idEventGroups, $idEvent, $idObject, $control, $commFilter, $viewText);
            $viewData['currentIdUser'] = $idUser;
            $viewData['eventTypes'] = $this->Common->getEventList();
            //Отображаем информацию
            $this->load->view('events/checkEventList', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для получения детальной информации по событию
     */
    public function ajax_details(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_VIEW) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keyItem     = $this->input->post('keyItem');
            //Получаем информацию об объекте
            $viewData['eventInfo']  = $this->Events->getInfo($keyItem);
            $viewData['eventList']  = $this->Common->getEventList();
            $viewData['attachList'] = $this->Attach->getList(TABLE_OBJECT_EVENTS, $keyItem);
            $viewData['canControl'] = $this->Auth->userHasAccessRight(ACCESS_EVENT_CONTROL);
            $viewData['currentIdUser'] = $this->Auth->currentIdUser();

            //Отображаем информацию
            $this->load->view('events/details', $viewData);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для установки контроля по событию
     */
    public function ajax_control(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_CONTROL) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keyItem = $this->input->post('keyItem');
            $idUser  = $this->Auth->currentIdUser();
            //Устанавливаем контроль
            $this->Events->control($idUser, $keyItem);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для снятия контроля по событию
     */
    public function ajax_cancelcontrol(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_CONTROL) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keyItem = $this->input->post('keyItem');
            //Снимаем контроль
            $this->Events->cancelControl($keyItem);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для замены кнопки Контроля
     */
    public function ajax_getControlInfo(){
        $toggle = $this->input->post('toggle');

        if ($toggle == 1) {
            if ($this->Auth->userHasAccessRight(ACCESS_EVENT_CONTROL) == true) {
                echo $this->Auth->currentAvatar('row-avatar')." ".$this->Auth->getUserName() . ", " .date(DATETIME_FORMAT)." <div id='do-cancel-control' class='btn btn-sm btn-outline-secondary cursor-pointer'>Отмена</div>";
            } else {
                echo $this->Auth->currentAvatar('row-avatar')." ".$this->Auth->getUserName() . ", " . date(DATETIME_FORMAT);
            }
        }else{
            if ($this->Auth->userHasAccessRight(ACCESS_EVENT_CONTROL) == true) {
                echo "Без контроля <div id='do-control' class='btn btn-sm btn-outline-success cursor-pointer'>Проверено</div>";
            } else {
                echo "Без контроля";
            }
        }
    }

    /**
     * Функция фонового выполнения, предназначена для замены кнопки Контроля
     */
    public function ajax_getControlTDInfo(){
        $toggle = $this->input->post('toggle');
        $userName = $this->Auth->getUserName();
        if ($toggle == 1) {
            echo "<span title='Контролирующий: {$userName} \n Дата: ".date(DATETIME_FORMAT)."' class='fa fa-lg text-success fa-check-circle-o align-middle'></span>";
        }else{
            echo "";
        }
    }

    /**
     * Функция фонового выполнения, предназначена для изменения данных в таблице
     */
    public function ajax_update(){
        //Получаем ПОСТ значения
        $keyItem = $this->input->post('keyItem');
        $field   = $this->input->post('field');
        $value   = $this->input->post('value');

        $this->Common->update(TABLE_OBJECT_EVENTS, 'ID_OBJECT_EVENT', $keyItem, $field, $value, ACCESS_EVENT_EDIT);
    }

    public function ajax_removeEvents(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if ($this->Auth->userHasAccessRight(ACCESS_EVENT_REMOVE) == false){
            echo ACCESS_DENIED;
        } else {
            //Получаем ПОСТ значения
            $keyItems = $this->input->post('keyItems');

            $this->Events->remove($keyItems);
        }
    }
}
