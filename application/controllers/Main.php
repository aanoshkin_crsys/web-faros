<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Main Описание класса основного окна
 */
class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Основная функция вызова контроллера
     */
    public function index()
    {
        //Если не было аторизировано, то отправляем на вторизацию
        if ($this->Auth->isAuth()==false){
            redirect('/auth/index');
        }else{
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Личный кабинет',
                'styles' => array(),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu'),
            );
            //Формируем данные для основной страницы
            $viewData = array();

            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop', $viewData);
            $this->load->view('footer', $footerData);
        }
    }

    public function test(){
        echo $this->Chart->hystogram('hysto', 500, 0, array('one'=>34, 'two'=>86, 'three'=>24));
    }

    /* =============== Фоновые запросы ===============*/

    /**
     * Функция сохранения информации во временную память
     */
    public function ajax_saveCache(){
        //Получаем ПОСТ запросы
        $id  = $this->input->post(COOKIE_LASTID);
        $url = $this->input->post(COOKIE_LASTURL);
        $menuItem = $this->input->post(COOKIE_LASTMENU);
        //Сохраняем полученные данные в куки
        $this->session->set_userdata(COOKIE_SAVECACHE, array(COOKIE_LASTMENU=>$menuItem, COOKIE_LASTID=>$id, COOKIE_LASTURL=>$url));
    }


}
