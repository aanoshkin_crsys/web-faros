<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //Считываем дополнительные помощники и библиотеки
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }

    /**
     * Основная функция вызова контроллера
     */
    public function index()
    {
        //Если не было аторизировано, то отправляем на вторизацию
        if ($this->Auth->isAuth()==false){
            redirect('/auth/index');
        }else{
            //Формируем данные для заголовка
            $headerData = array(
                'title'=>'Личный кабинет',
                'styles' => array(),
                'topmenu' => array()
            );
            //Формируем данные для подвала
            $footerData = array(
                'scripts' => array('main.menu'),
            );
            //Формируем данные для основной страницы
            $viewData = array();

            //Отображаем страницу
            $this->load->view('header', $headerData);
            $this->load->view('main/desktop', $viewData);
            $this->load->view('footer', $footerData);
        }
    }

    /* =============== Фоновые запросы ===============*/

    /**
     * Функция получения списка сообщений по записи (в фоновом режиме)
     */
    public function ajax_getCommunityList(){
        $access = $this->input->post('access');
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else {
            //Получаем ПОСТ значения
            $keyItem   = $this->input->post('keyItem');
            $tableName = $this->input->post('tableName');
            $canEdit   = ($access == '') || (($access != '') && ($this->Auth->userHasAccessRight($access)));

            $viewData['commList']      = $this->Community->getCommunityList($tableName, $keyItem);
            $viewData['currentIdUser'] = $this->Auth->currentIdUser();
            $viewData['canEdit']       = $canEdit;
            $this->load->view('common/community', $viewData);
            $this->Community->readCommunityList($tableName, $keyItem);
        }
    }

    /**
     * Функция отправки текста (в фоновом режиме)
     */
    public function ajax_sendCommunityText(){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else {
            //Получаем ПОСТ значения
            $keyItem   = $this->input->post('keyItem');
            $tableName = $this->input->post('tableName');
            $sendText  = $this->input->post('sendText');

            $this->Community->sendCommunityText($tableName, $keyItem, $sendText);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для получения списка прикрепленных файлов
     */
    public function ajax_getAttachList(){
        $access = $this->input->post('access');
        $images = $this->input->post('images');

        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else {
            //Получаем ПОСТ значения
            $keyItem   = $this->input->post('keyItem');
            $tableName = $this->input->post('tableName');
            $canEdit   = ($access == '') || (($access != '') && ($this->Auth->userHasAccessRight($access)));

            $viewData['attachList']    = $this->Attach->getList($tableName, $keyItem);
            $viewData['currentIdUser'] = $this->Auth->currentIdUser();
            $viewData['canEdit']       = $canEdit;
            $viewData['tableName']     = $tableName;
            $viewData['keyItem']       = $keyItem;
            if ($images == true){
                $this->load->view('common/attachimages', $viewData);
            }else {
                $this->load->view('common/attaches', $viewData);
            }
        }
    }

    /**
     * Функция фонового выполнения, предназначеная для удаления прикрепленных файлов
     */
    public function ajax_removeAttaches(){
        $access = $this->input->post('access');
        //Получаем ПОСТ значения
        $keyItem   = $this->input->post('keyItem');
        $this->Common->update(TABLE_ATTACHMENTS, 'ID_ATTACHMENT', $keyItem, 'DELETED', 1, $access);
    }

    /**
     * Фунция фонового выполнения, предназначена для заливки файлов на сервер
     */
    public function ajax_uploadAttach(){
        $keyItem   = $this->input->post('keyItem');
        $tableName = $this->input->post('tableName');
        $config['upload_path']      = $_SERVER["DOCUMENT_ROOT"]."/attachment/{$tableName}/".date('Ymd')."/";
        $config['allowed_types']	= '*';
        $config['max_size']	        = 20480;
        $config['encrypt_name']     = TRUE;

        if (!file_exists($config['upload_path'])){
            mkdir($config['upload_path'], 0777, true);
        }

        $this->load->library('upload', $config);

        if ($this->upload->do_upload() == false) {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = $this->upload->data();
            echo json_encode($data);
            $this->Common->addAttach($tableName, $keyItem, date('Ymd')."/".$data['file_name'], $data['orig_name'], $data['file_size']);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для заливки картинки на сервер
     */
    public function ajax_uploadImage(){
        $keyItem   = $this->input->post('keyItem');
        $tableName = $this->input->post('tableName');
        $config['upload_path']      = $_SERVER["DOCUMENT_ROOT"]."/attachment/{$tableName}/".date('Ymd')."/";
        $config['allowed_types']	= 'gif|jpg|png|bmp';
        $config['max_size']	        = 20480;
        $config['encrypt_name']     = TRUE;

        if (!file_exists($config['upload_path'])){
            mkdir($config['upload_path'], 0777, true);
        }

        $this->load->library('upload', $config);

        if ($this->upload->do_upload() == false) {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = $this->upload->data();
            echo json_encode($data);
            $this->Common->addAttach($tableName, $keyItem, date('Ymd')."/".$data['file_name'], $data['orig_name'], $data['file_size']);
        }
    }

    /**
     * Функция фонового выполнения, предназначена для получения комментария по ключу
     */
    public function ajax_getItemComment(){
        $keyItem = $this->input->post('keyItem');
        echo $this->Common->getItemComment($keyItem);
    }
}
