<?php

//Папка для сохранения файлов
define('OUT_DIR', $_SERVER['DOCUMENT_ROOT'].'/attachment/');

//Проверка на попытку прямого доступа
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Класс-библиотека CodeIgniter
class pdf_generator {

//Функция генерации
function beginReport()
{
    //Создаем объект класса mPDF
    require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
    return new \Mpdf\Mpdf([
        'mode' => 'utf-8',
        'format' => [210, 297],
        'orientation' => 'P',
        'default_font_size' => 10,
        'default_font' => 'Arial',
    ]);
}

function endReport($mpdf, $pathname,  $file_name)
{
    if (!file_exists(OUT_DIR.$pathname)){
        mkdir(OUT_DIR.$pathname, 0777, true);
    }
    //Сохраняем сам файл
    $mpdf->Output(OUT_DIR.$pathname.$file_name);
}

}