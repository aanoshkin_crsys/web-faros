<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE',  0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE',   0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE',  0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
======================================================================
   Виды групп для общего справочника
======================================================================
 */
defined('KIND_PERMISSIONS')  OR define('KIND_PERMISSIONS', '1');  //Вид групп - Полномочия
defined('KIND_PROPERTIES')   OR define('KIND_PROPERTIES',  '2');   //Вид групп - Свойства объектов
defined('KIND_EVENTS')       OR define('KIND_EVENTS',      '3');       //Вид групп - Виды событий

/*
======================================================================
    Настройка констант ролей пользователей
======================================================================
*/

defined('ROLE_ADMINISTRATOR')       OR define('ROLE_ADMINISTRATOR',         'ADMINISTRATOR');   //Роль администратора
defined('ROLE_CONTROLLER')          OR define('ROLE_CONTROLLER',            'CONTROLLER');      //Роль контроллера
defined('ROLE_ANALITIC')            OR define('ROLE_ANALITIC',              'ANALITIC');        //Роль аналитика
defined('ROLE_CLIENT')              OR define('ROLE_CLIENT',                'CLIENT');          //Роль клиента
defined('ROLE_SUPPORT')             OR define('ROLE_SUPPORT',               'SUPPORT');         //Роль техподдержки


/*
======================================================================
    Настройка констант прав доступа
======================================================================
*/

defined('ACCESS_GROUP_USERS')       OR define('ACCESS_GROUP_USERS',        'GROUP_USERS');      //Имеется право по группе Пользователи
defined('ACCESS_GROUP_OBJECTS')     OR define('ACCESS_GROUP_OBJECTS',      'GROUP_OBJECTS');    //Имеется право по группе Объекты
defined('ACCESS_GROUP_EVENTS')      OR define('ACCESS_GROUP_EVENTS',       'GROUP_EVENTS');     //Имеется право по группе События
defined('ACCESS_GROUP_ROLES')       OR define('ACCESS_GROUP_ROLES',        'GROUP_ROLES');      //Имеется право по группе Ролей
defined('ACCESS_GROUP_REPORTS')     OR define('ACCESS_GROUP_REPORTS',      'GROUP_REPORTS');    //Имеется право по группе Отчеты

defined('ACCESS_USER_VIEW_LIST')    OR define('ACCESS_USER_VIEW_LIST',     'USER_VIEW_LIST');   //Право Просмотр списка пользователей
defined('ACCESS_USER_VIEW')         OR define('ACCESS_USER_VIEW',          'USER_VIEW');        //Право Просмотр детальной информации по пользователю
defined('ACCESS_USER_EDIT')         OR define('ACCESS_USER_EDIT',          'USER_EDIT');        //Право Редактирование пользователей
defined('ACCESS_USER_OBJECTSET')    OR define('ACCESS_USER_OBJECTSET',     'USER_OBJECTSET');   //Право Присваивать пользователю объекты

defined('ACCESS_OBJECT_VIEW_LIST')  OR define('ACCESS_OBJECT_VIEW_LIST',   'OBJECT_VIEW_LIST'); //Право Просмотр списка объектов
defined('ACCESS_OBJECT_VIEW')       OR define('ACCESS_OBJECT_VIEW',        'OBJECT_VIEW');      //Право Просмотр детальной информации по объекту
defined('ACCESS_OBJECT_EDIT')       OR define('ACCESS_OBJECT_EDIT',        'OBJECT_EDIT');      //Право Редактирование объекта

defined('ACCESS_EVENT_VIEW_LIST')   OR define('ACCESS_EVENT_VIEW',         'EVENT_VIEW');       //Право Просмотр списка событий
defined('ACCESS_EVENT_EDIT')        OR define('ACCESS_EVENT_EDIT',         'EVENT_EDIT');       //Право Редактирование события
defined('ACCESS_EVENT_CONTROL')     OR define('ACCESS_EVENT_CONTROL',      'EVENT_CONTROL');    //Право Проверять события
defined('ACCESS_EVENT_REMOVE')      OR define('ACCESS_EVENT_REMOVE',       'EVENT_REMOVE');     //Право Удаления события

defined('ACCESS_ROLE_VIEW')         OR define('ACCESS_ROLE_VIEW',          'ROLE_VIEW');        //Право Просмотр списка ролей
defined('ACCESS_ROLE_EDIT')         OR define('ACCESS_ROLE_EDIT',          'ROLE_EDIT');        //Право Редактирование роли

defined('ACCESS_REPORT_VIEW')       OR define('ACCESS_REPORT_VIEW',        'REPORT_VIEW');      //Право Просмотр отчетов
defined('ACCESS_REPORT_EDIT')       OR define('ACCESS_REPORT_EDIT',        'REPORT_EDIT');      //Право Редактирование отчетов
defined('ACCESS_REPORT_BUILDER')    OR define('ACCESS_REPORT_BUILDER',     'REPORT_BUILDER');   //Право Формирования отчетов

defined('ACCESS_DENIED')            OR define('ACCESS_DENIED',             'У вас отсутствуют права доступа.');
defined('ACCESS_NOAUTH')            OR define('ACCESS_NOAUTH',             'Необходимо пройти автризацию.');
defined('DLG_NOSELECTION')          OR define('DLG_NOSELECTION',           'Записи не выбраны.');

/*
======================================================================
    Настройка констант для куки
======================================================================
 */
defined('COOKIE_LOGIN')            OR define('COOKIE_LOGIN',             'userLogin');           //Данные по логину текущего пользователя
defined('COOKIE_PASSWORD')         OR define('COOKIE_PASSWORD',          'userPassword');        //Данные по паролю текущего пользователя
defined('COOKIE_SAVECACHE')        OR define('COOKIE_SAVECACHE',         'saveCache');           //Данные по сохраненным данным
defined('COOKIE_LASTMENU')         OR define('COOKIE_LASTMENU',          'lastMenu');            //Данные по последнему выбранному пункту
defined('COOKIE_LASTID')           OR define('COOKIE_LASTID',            'lastId');              //Данные по последнему выбранному ключу
defined('COOKIE_LASTURL')          OR define('COOKIE_LASTURL',           'lastUrl');             //Данные по последней ссылке

defined('COOKIE_AUTHDATA')         OR define('COOKIE_AUTHDATA',          'userAuthData');        //Данные по текущему пользователю
defined('COOKIE_ACCESS_GROUPS')    OR define('COOKIE_ACCESS_GROUPS',     'accessGroups');        //Данные по группе полномочий текущего пользователя
defined('COOKIE_ACCESS_RIGHTS')    OR define('COOKIE_ACCESS_RIGHTS',     'accessRights');        //Данные по полномочиям текущего пользователя

/*
======================================================================
    Настройка вывода даты и времени
======================================================================
*/

defined('DATE_FORMAT')        OR define('DATE_FORMAT',     'd.m.Y');        //Настройка формата даты
defined('TIME_FORMAT')        OR define('TIME_FORMAT',     'H:i:s');        //Настройка формата времени
defined('DATETIME_FORMAT')    OR define('DATETIME_FORMAT', 'd.m.Y H:i:s');  //Настройка формата даты и времени

/*
======================================================================
    Настройка наименований таблиц
======================================================================
*/

defined('TABLE_OBJECT_EVENTS') OR define('TABLE_OBJECT_EVENTS', 'OBJECT_EVENTS');        //Наименование таблицы с событиями
defined('TABLE_REPORTS')       OR define('TABLE_REPORTS',       'REPORTS');              //Наименование таблицы с отчетами
defined('TABLE_USERS')         OR define('TABLE_USERS',         'USERS');                //Наименование таблицы с пользователями
defined('TABLE_OBJECTS')       OR define('TABLE_OBJECTS',       'OBJECTS');              //Наименование таблицы с объектами
defined('TABLE_OBJECT_GROUPS') OR define('TABLE_OBJECT_GROUPS', 'OBJECT_GROUPS');        //Наименование таблицы с группами объектов
defined('TABLE_ATTACHMENTS')   OR define('TABLE_ATTACHMENTS',   'ATTACHMENTS');          //Наименование таблицы с вложенными файлами

/*
======================================================================
    Ключи JSON пар
======================================================================
*/

defined('JSON_KASSA')    OR define('JSON_KASSA',   'KASSA');          //Касса
defined('JSON_CHECKNUM') OR define('JSON_CHECKNUM','CHECKNUM');       //Номер чека
defined('JSON_CHECKUE')  OR define('JSON_CHEKUE',  'CHECKUE');        //Содержимое чека

/*
======================================================================
    Значения по-умолчанию
======================================================================
*/
defined('DEF_USER_GROUP')      OR define('DEF_USER_GROUP',   6);          //Ключ группы "Новые пользователи"
defined('DEF_USER_ROLE')       OR define('DEF_USER_ROLE',    5);          //Ключ роли "Без прав"
defined('MAIL_FROM_EMAIL')     OR define('MAIL_FROM_EMAIL',  'crsys.tech.test@mail.ru');     //От кого письмо
defined('MAIL_FROM_NAME')      OR define('MAIL_FROM_NAME',   'Служба рассылки CRSYS');   //От кого письмо
