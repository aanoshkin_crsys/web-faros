<?php

class Event_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция дбавления события
     * @param $idObject     Уникальный код объекта
     * @param $idRecord     Ключ записи
     * @param $nameObject   Наименование объекта
     * @param $ip           IP адрес
     * @param $eventGroup   Наименование группы событий
     * @param $eventType    Наименование события
     * @param $eventText    Текст события
     * @param $person       ФИО пользователя, совершившего событие
     * @param $userName     Имя аналитика, нашедшего событие
     * @param $dateCreate   Дата нахождения нарушения
     * @param $dateEvent    Дата события
     * @param $viewData     Представление события
     * @param $jsonData     Данные в формате JSON
     */
    public function addEvent($idObject, $idRecord, $nameObject, $ip, $eventGroup, $eventType, $eventText, $person, $userName, $dateCreate, $dateEvent, $viewData, $jsonData){
        $idObject   = $this->Objects->updateObjectInfo($idObject, $nameObject, $ip);
        $idProp     = $this->Common->getPropId($eventType, $eventGroup, 'EVENTS');
        $idUser     = $this->Common->getUserId($userName);
        $data = array(
            'ID_OBJECT'     => $idObject,
            'ID_RECORD'     => $idRecord,
            'ID_USER'       => $idUser,
            'DATE_CREATE'   => $dateCreate,
            'ID_EVENT'      => $idProp,
            'MESSAGE'       => $eventText,
            'PERSON'        => $person,
            'DATE_EVENT'    => $dateEvent,
            'VIEW'          => $viewData,
            'DATA'          => $jsonData,
        );
        $this->db->insert('OBJECT_EVENTS', $data);
    }

    /**
     * Функция получения списка событий по объекту
     * @param $idUser int Текущий пользователь
     * @param $rangeStart string Период начала
     * @param $rangeFinish string Период окончания
     * @param $idAnalitic string Ключи аналитиков
     * @param $idEventGroups string Ключи группы событий
     * @param $idEvent string Ключи типов событий
     * @param $idObject string Ключи объектов
     * @param $control int Вид контроля
     * @param $commFilter int Фильтр по коммуникациям
     * @param $viewText string Содержимое текста
     * @param $completed int Фильтр по замечаниям 0 - нет, 1 - Замечания есть, 2 - Есть невыполненные замечания, 3 - Только выполненные замечания
     * @return mixed
     */
    public function getList($idUser, $rangeStart, $rangeFinish, $idAnalitic, $idEventGroups, $idEvent, $idObject, $control, $commFilter, $viewText, $completed = 0){
        $idTable = $this->Common->getTableId('OBJECT_EVENTS');
        $sql = "SELECT
                  OE.ID_OBJECT_EVENT, 
                  OG.ID_OBJECT_GROUP,
                  OG.`NAME` AS GROUP_NAME,
                  OE.ID_OBJECT,
                  O.`NAME` AS OBJECT_NAME, 
                  OE.ID_USER, 
                  U.`NAME` AS USER_NAME,
                  OE.ID_EVENT, 
                  OE.MESSAGE, 
                  OE.PERSON, 
                  OE.DATE_CREATE, 
                  OE.DATE_EVENT, 
                  OE.ID_CONTROLLER,
                  UEC.NAME AS CONTROLLER_USER_NAME,
                  OE.DATE_CONTROLL, 
                  EVENTS.`NAME` AS EVENT_NAME,
                  ( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = OE.ID_OBJECT_EVENT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) AS COMMUNITY,
                  OE.MESSAGE_CONTROL,
                  OE.COMPLETED
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  -- Справочник группы событий
                  INNER JOIN ITEM_GROUPS AS EG 
                    ON EG.ID_ITEM_GROUP = EVENTS.ID_ITEM_GROUP
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                  -- Контролирюущие
                  LEFT JOIN USERS AS UEC
                    ON OE.ID_CONTROLLER = UEC.ID_USER
                    AND UEC.DELETED = 0
                WHERE
                  -- Ограничение по группе или объекту (По контролирующему)
                  (
                   (OG.ID_CONTROLLER = {$idUser})OR
                   (EXISTS( SELECT 
                              * 
                            FROM 
                              OBJECT_ACCESS AS OA 
                            WHERE 
                              OA.ID_OBJECT = O.ID_OBJECT 
                              AND OA.ID_USER = {$idUser}))
                  )";

        if ($idAnalitic != ''){
            $sql = $sql." AND OE.ID_USER in ({$idAnalitic})";
        }

        if ($rangeStart != ''){
            $sql = $sql." AND DATE(OE.DATE_CREATE) >= STR_TO_DATE('{$rangeStart}', '%d.%m.%Y')";
        }

        if ($rangeFinish != ''){
            $sql = $sql." AND DATE(OE.DATE_CREATE) <= STR_TO_DATE('{$rangeFinish}', '%d.%m.%Y')";
        }

       if ($idEventGroups != ''){
            $sql = $sql." AND EG.ID_ITEM_GROUP in ({$idEventGroups})";
        }

       if ($idEvent != ''){
            $sql = $sql." AND OE.ID_EVENT in ({$idEvent})";
        }

        if ($idObject != ''){
            $sql = $sql." AND O.ID_OBJECT in ({$idObject})";
        }

        if ($viewText != ''){
            $sql = $sql." AND UPPER(OE.VIEW) LIKE UPPER('%{$viewText}%')";
        }

        if ($control == 0){
            $sql = $sql." AND OE.DATE_CONTROLL is null";
        }else if ($control == 1){
            $sql = $sql." AND OE.DATE_CONTROLL is not null";
        }

        if ($commFilter == 0){
            $sql = $sql." AND ( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = OE.ID_OBJECT_EVENT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) is null";
        }else if ($commFilter == 1){
            $sql = $sql." AND ( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = OE.ID_OBJECT_EVENT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) is not null";
        }else if ($commFilter == 2){
            $sql = $sql." AND (( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = OE.ID_OBJECT_EVENT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) <> {$idUser})";
        }

        if ($completed > 0){
            $sql = $sql." AND (IFNULL(OE.MESSAGE_CONTROL, '') <> '')";
        }

        if ($completed == 2){
            $sql = $sql." AND OE.COMPLETED = 0";
        }else if ($completed == 3){
            $sql = $sql." AND OE.COMPLETED = 1";
        }

        $sql = $sql." ORDER BY
                  OBJECT_NAME ASC,
                  OE.DATE_EVENT DESC";

        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
   }

    /**
     * Функция получения списка аналитиков для доступных событий
     * @param $idUser int Ключ пользователя
     * @return mixed
     */
    public function getAnaliticList($idUser){
        $sql = "SELECT DISTINCT
                  OE.ID_USER, 
                  U.`NAME` AS USER_NAME,
                  U.AVATARNAME
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник группы событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                WHERE
                  -- Ограничение по группе или объекту (По контролирующему)
                  (
                   (OG.ID_CONTROLLER = {$idUser})OR
                   (EXISTS( SELECT 
                              * 
                            FROM 
                              OBJECT_ACCESS AS OA 
                            WHERE 
                              OA.ID_OBJECT = O.ID_OBJECT 
                              AND OA.ID_USER = {$idUser}))
                  )";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения списка событий доступных по списку событий
     * @param $idUser int Ключ текущего пользователя
     * @return mixed
     */
    public function getEventList($idUser){
        $sql = "SELECT DISTINCT
                  IG.ID_ITEM_GROUP,
                  IG.NAME as GROUP_NAME,
                  OE.ID_EVENT, 
                  EVENTS.`NAME` AS EVENT_NAME
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник группы событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  INNER JOIN ITEM_GROUPS as IG 
                    ON IG.ID_ITEM_GROUP = EVENTS.ID_ITEM_GROUP
                  INNER JOIN ITEM_KINDS as IK 
                    ON IK.ID_KIND = IG.ID_KIND
                    AND IK.SYSNAME = 'EVENTS'
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                WHERE
                  -- Ограничение по группе или объекту (По контролирующему)
                  (
                   (OG.ID_CONTROLLER = {$idUser})OR
                   (EXISTS( SELECT 
                              * 
                            FROM 
                              OBJECT_ACCESS AS OA 
                            WHERE 
                              OA.ID_OBJECT = O.ID_OBJECT 
                              AND OA.ID_USER = {$idUser}))
                  )
                ORDER BY
                  IG.ROWORDER,
                  EVENTS.ROWORDER ";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения списка группы событий доступных по списку событий
     * @param $idUser int Ключ текущего пользователя
     * @return mixed
     */
    public function getEventGroupList($idUser){
        $sql = "SELECT DISTINCT
                  IG.ID_ITEM_GROUP,
                  IG.NAME as GROUP_NAME,
                  IG.SYSNAME
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник группы событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  INNER JOIN ITEM_GROUPS as IG 
                    ON IG.ID_ITEM_GROUP = EVENTS.ID_ITEM_GROUP
                  INNER JOIN ITEM_KINDS as IK 
                    ON IK.ID_KIND = IG.ID_KIND
                    AND IK.SYSNAME = 'EVENTS'
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                WHERE
                  -- Ограничение по группе или объекту (По контролирующему)
                  (
                   (OG.ID_CONTROLLER = {$idUser})OR
                   (EXISTS( SELECT 
                              * 
                            FROM 
                              OBJECT_ACCESS AS OA 
                            WHERE 
                              OA.ID_OBJECT = O.ID_OBJECT 
                              AND OA.ID_USER = {$idUser}))
                  )
                ORDER BY
                  IG.ROWORDER";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения списка объектов, доступных по списку событий
     * @param $idUser int Ключ текущего пользователя
     * @return mixed
     */
    public function getObjectList($idUser){
        $sql = "SELECT DISTINCT
                  OG.ID_OBJECT_GROUP,
                  OG.NAME as GROUP_NAME,
                  OE.ID_OBJECT,
                  O.`NAME` AS OBJECT_NAME
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник группы событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                WHERE
                  -- Ограничение по группе или объекту (По контролирующему)
                  (
                   (OG.ID_CONTROLLER = {$idUser})OR
                   (EXISTS( SELECT 
                              * 
                            FROM 
                              OBJECT_ACCESS AS OA 
                            WHERE 
                              OA.ID_OBJECT = O.ID_OBJECT 
                              AND OA.ID_USER = {$idUser}))
                  )
                ORDER BY
                  OG.ROWORDER,
                  OBJECT_NAME";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения информации по событию
     * @param $idEvent int Ключ события
     * @return mixed
     */
    public function getInfo($idEvent){
        $sql = "SELECT
                  OE.ID_OBJECT_EVENT, 
                  OG.ID_OBJECT_GROUP,
                  OG.`NAME` AS GROUP_NAME,
                  OE.ID_OBJECT,
                  O.`NAME` AS OBJECT_NAME, 
                  OE.ID_USER, 
                  U.`NAME` AS USER_NAME,
                  U.AVATARNAME AS USER_AVATARNAME,
                  OE.ID_EVENT, 
                  OE.MESSAGE, 
                  OE.PERSON, 
                  OE.DATE_CREATE, 
                  OE.DATE_EVENT, 
                  OE.ID_CONTROLLER,
                  UEC.NAME AS CONTROLLER_USER_NAME,
                  UEC.AVATARNAME AS CONTROLLER_AVATARNAME,
                  OE.DATE_CONTROLL, 
                  EVENTS.`NAME` AS EVENT_NAME,
                  OE.VIEW,
                  OE.MESSAGE_CONTROL,
                  OE.COMPLETED
                FROM
                  -- События объекто
                  OBJECT_EVENTS AS OE
                  -- Справочник объектов
                  INNER JOIN OBJECTS AS O 
                    ON OE.ID_OBJECT = O.ID_OBJECT
                    AND O.DELETED = 0
                    AND OE.DELETED = 0
                  -- Группа объектов
                  INNER JOIN OBJECT_GROUPS AS OG 
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                  -- Справочник группы событий
                  INNER JOIN ITEMS AS EVENTS 
                    ON OE.ID_EVENT = EVENTS.ID_ITEM
                    AND EVENTS.DELETED = 0
                  -- Аналитики
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                  -- Контролирюущие
                  LEFT JOIN USERS AS UEC
                    ON OE.ID_CONTROLLER = UEC.ID_USER
                    AND UEC.DELETED = 0
                WHERE
                  OE.ID_OBJECT_EVENT = {$idEvent}";

        $rows = $this->db->query($sql, array())->row_array();
        return $rows;
    }

    /**
     * Функция установки признака Контроля для события
     * @param $idUser int Ключ текущего пользователя
     * @param $keyItem int Ключ события
     */
    public function control($idUser, $keyItem){
        $sql = "UPDATE OBJECT_EVENTS SET ID_CONTROLLER = ?, DATE_CONTROLL = NOW() WHERE ID_OBJECT_EVENT in ({$keyItem})";
        $this->db->query($sql, array($idUser));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция снятия признака Контроля для события
     * @param $keyItem int Ключ события
     */
    public function cancelControl($keyItem){
        $sql = "UPDATE OBJECT_EVENTS SET ID_CONTROLLER = null, DATE_CONTROLL = null WHERE ID_OBJECT_EVENT = ?";
        $this->db->query($sql, array($keyItem));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция логического удаления события
     * @param $keyItems string Список ключей через запятую
     */
    public function remove($keyItems){
        $sql = "UPDATE OBJECT_EVENTS SET DELETED = 1 WHERE ID_OBJECT_EVENT in ({$keyItems})";
        $this->db->query($sql, array());
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }
}