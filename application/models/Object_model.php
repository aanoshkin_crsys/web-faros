<?php

class Object_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция обновления информации по объекту
     * @param $idObject Идентификатор объекта
     * @param $objectName Наименование объекта
     * @param $ipAdress IP адрес объекта
     * @return mixed Ключ объекта
     */
    public function updateObjectInfo($idObject, $objectName, $ipAdress){
        //Если переданное наименование отличается от того, что находится в БД
        if (($objectName != '')&&($objectName != $this->Common->getSQL('select NAME from OBJECTS where GUID like ?', array($idObject)))){
            //Обновляем наименование
            $data=array('NAME' => $objectName);
            $this->db->update('OBJECTS', $data, array('GUID'=>$idObject));
        }
        //Устанавливаем свойство IP адреса
        $this->setProperty($idObject, '', 'NET_IP', $ipAdress, $ipAdress);
        //Получаем ключ объекта по его идентификатору
        $idObject = $this->Common->getSQL('select ID_OBJECT from OBJECTS where GUID like ?', array($idObject));

        return $idObject;
    }

    /**
     * Функция заполнения свойств объекта
     * @param $idObject Ключ объекта
     * @param $propGroup Наименование группы свойств
     * @param $propName Наименование свойства
     * @param $propValue Значение свойства
     * @param $comment Коментарий
     */
    public function setProperty($idObject, $propGroup, $propName, $propValue, $comment){

        //Получаем ключ свойства, по его наименованию
        $idProp  = $this->Common->getPropId($propName, $propGroup, 'PROPERTIES');
        //Если нет записей, по указанным признакам, то
        if ($this->Common->getSQL('select COUNT(*) from OBJECT_PROPERTIES where ID_OBJECT = ? and ID_PROPERTY = ? and COMMENT = ?', array($idObject, $idProp, $comment)) == 0){
            //Добавляем новую запись
            $data = array(
                'ID_OBJECT'   => $idObject,
                'ID_PROPERTY' => $idProp,
                'VALUE'       => $propValue,
                'COMMENT'     => $comment,
            );
            $this->db->insert('OBJECT_PROPERTIES', $data);
        }else{
            //Обновляем существующую запись
            $data = array(
                'VALUE'=>$propValue,
            );
            $this->db->update('OBJECT_PROPERTIES', $data, array('ID_OBJECT' => $idObject, 'ID_PROPERTY'=>$idProp, 'COMMENT'=>$comment));
        }
    }

    /**
     * Функция получения списка объектов, разбитых на группы
     * @return mixed
     */
    public function getList(){
        $sql = "SELECT
                  OG.ID_OBJECT_GROUP, 
                  OG.`NAME` GROUP_NAME, 
                  IFNULL(OG.ID_CONTROLLER,'') AS ID_CONTROLLER, 
                  IFNULL(CONTROLLER.`NAME`, '') AS CONTROLLER_NAME, 
                  IFNULL(O.ID_OBJECT,'') AS ID_OBJECT, 
                  O.`NAME` AS OBJECT_NAME, 
                  O.`COMMENT`
                FROM
                  OBJECT_GROUPS AS OG
                  LEFT JOIN USERS AS CONTROLLER 
                    ON OG.ID_CONTROLLER = CONTROLLER.ID_USER
                  LEFT JOIN OBJECTS AS O 
                    ON OG.ID_OBJECT_GROUP = O.ID_OBJECT_GROUP
                    AND O.DELETED = 0
                ORDER BY
                  OG.ROWORDER,
                  OBJECT_NAME";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения списка группы объектов
     * @return mixed
     */
    public function getGroupList(){
        $sql = 'select ID_OBJECT_GROUP, NAME from OBJECT_GROUPS ORDER BY ROWORDER';
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция смены группы
     * @param $idObjs str Список ключей объектов
     * @param $newId int Ключ группы
     */
    public function changeGroup($idObjs, $newId){
        $sql = "UPDATE OBJECTS SET ID_OBJECT_GROUP = ? WHERE ID_OBJECT in ({$idObjs})";
        $this->db->query($sql, array($newId));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция логического удаления объектов
     * @param $idObjs str Список ключей объектов
     */
    public function remove($idObjs){
        $sql = "UPDATE OBJECTS SET DELETED = 1 WHERE ID_OBJECT in ({$idObjs})";
        $this->db->query($sql, array());
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция получения информации по объекту
     * @param $idObject int Ключ объекта
     * @return mixed
     */
    public function getInfo($idObject){
        $sql = 'select * from OBJECTS Where ID_OBJECT = ?';
        $rows = $this->db->query($sql, array($idObject))->row_array();
        return $rows;
    }

    /**
     * Функция получения информации по свойствам объекта
     * @param $idObject int Ключ объекта
     * @return mixed
     */
    public function getInfoProps($idObject){
        $sql = "SELECT 
                  IG.ID_ITEM_GROUP, 
                  IG.`NAME` AS GROUP_NAME,
                  I.`NAME` AS PROPERTY_NAME,
                  OP.`VALUE`, 
                  OP.`COMMENT`, 
                  OP.LAST_UPDATE
                FROM
                  OBJECT_PROPERTIES AS OP
                  INNER JOIN ITEMS AS I 
                    ON OP.ID_PROPERTY = I.ID_ITEM 
                    AND I.DELETED = 0
                    AND OP.ID_OBJECT = ?
                  INNER JOIN ITEM_GROUPS AS IG 
                    ON I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP
                ORDER BY
                  IG.ROWORDER,
                  I.ROWORDER";
        $rows = $this->db->query($sql, array($idObject))->result_array();
        return $rows;
    }

    /**
     * Функция получения информации по пользователям, которые связаны с объектом
     * @param $idObject int Ключ объекта
     * @return mixed
     */
    public function getInfoUsers($idObject){
        $sql = "SELECT 
                  U.ID_USER,
                  U.AVATARNAME,
                  IF(U.`NAME` IS NULL, U.LOGIN, CONCAT(U.`NAME`,' (', U.LOGIN, ')')) AS USERNAME,
                  U.ENABLED
                FROM
                  OBJECT_ACCESS AS OA
                  INNER JOIN USERS AS U 
                    ON OA.ID_USER = U.ID_USER
                    AND U.DELETED = 0
                    AND OA.ID_OBJECT = ?";
        $rows = $this->db->query($sql, array($idObject))->result_array();
        return $rows;
    }

    /**
     * Функция получения информации по группе
     * @param $idGroup int Ключ группы
     * @return mixed
     */
    public function getInfoGroup($idGroup){
        $sql = 'select * from OBJECT_GROUPS where ID_OBJECT_GROUP = ?';
        $rows = $this->db->query($sql, array($idGroup))->row_array();
        return $rows;
    }

    /**
     * Функция перемещения группы вверх
     * @param $keyItem int Ключ группы
     */
    public function groupMoveUp($keyItem){
        $sql = "select ROWORDER from OBJECT_GROUPS where ID_OBJECT_GROUP = ?";
        $CurrentOrder = $this->db->query($sql, array($keyItem))->row_array()['ROWORDER'];
        $sql = "select ID_OBJECT_GROUP, ROWORDER from OBJECT_GROUPS where ROWORDER < ? order by ROWORDER desc limit 1";
        $PrevData =  $this->db->query($sql, array($CurrentOrder))->row_array();

        if (sizeof($PrevData) > 0){
            $sql = "update OBJECT_GROUPS set ROWORDER = ? where ID_OBJECT_GROUP = ?";
            $this->db->query($sql, array($PrevData['ROWORDER'], $keyItem));
            $this->db->query($sql, array($CurrentOrder, $PrevData['ID_OBJECT_GROUP']));
        }
    }

    /**
     * Функция перемещения группы вниз
     * @param $keyItem int Ключ группы
     */
    public function groupMoveDown($keyItem){
        $sql = "select ROWORDER from OBJECT_GROUPS where ID_OBJECT_GROUP = ?";
        $CurrentOrder = $this->db->query($sql, array($keyItem))->row_array()['ROWORDER'];
        $sql = "select ID_OBJECT_GROUP, ROWORDER from OBJECT_GROUPS where ROWORDER > ? order by ROWORDER asc limit 1";
        $nextData =  $this->db->query($sql, array($CurrentOrder))->row_array();

        if (sizeof($nextData) > 0){
            $sql = "update OBJECT_GROUPS set ROWORDER = ? where ID_OBJECT_GROUP = ?";
            $this->db->query($sql, array($nextData['ROWORDER'], $keyItem));
            $this->db->query($sql, array($CurrentOrder, $nextData['ID_OBJECT_GROUP']));
        }
    }

    /**
     * Функция удаления группы
     * @param $keyItem int Ключ группы для удаления
     * @return string
     */
    public function removeGroup($keyItem){
        $sql = "select count(*) as Cnt from OBJECTS where ID_OBJECT_GROUP = ? AND DELETED = 0";
        if ($this->db->query($sql, array($keyItem))->row_array()['Cnt'] > 0){
            return 'В группе находятся не удаленные объекты.';
        }else{
            $sql = "delete from OBJECT_GROUPS where ID_OBJECT_GROUP = ?";
            $this->db->query($sql, array($keyItem));
            if (mysql_error() != ''){
                echo mysql_Error();
            }
            return '';
        }
    }

    /**
     * Добавление новой группы
     * @return string
     */
    public function newGroup(){
        $sql = "select IFNULL(max(ROWORDER), 0)+1 as mo from OBJECT_GROUPS";
        $maxOrder = $this->db->query($sql, array())->row_array()['mo'];

        $sql = "insert into OBJECT_GROUPS (NAME, ROWORDER) VALUES('Новая группа', ?)";
        $this->db->query($sql, array($maxOrder));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

}