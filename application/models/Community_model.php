<?php

class Community_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция получения списка сообщений
     * @param $tableName string Наименование таблицы
     * @param $keyItem int Ключ записи
     * @return mixed
     */
    public function getCommunityList($tableName, $keyItem){
        $sql = "SELECT  
                  C.ID_COMMUNITY, 
                  C.ID_AUTHOR, 
                  U.`NAME` as USERNAME,
                  U.AVATARNAME,
                  C.DATE_CREATE, 
                  C.MESSAGE, 
                  CR.DATE_READ
                FROM
                  COMMUNITY AS C
                  INNER JOIN `TABLES` AS T 
                    ON C.ID_TABLE = T.ID_TABLE
                    AND C.DELETED = 0
                    AND T.`NAME` = ?
                    AND C.ID_RECORD = ?
                  LEFT JOIN USERS AS U 
                    ON C.ID_AUTHOR = U.ID_USER
                  LEFT JOIN COMMUNITY_READS AS CR 
                    ON C.ID_COMMUNITY = CR.ID_COMMUNITY
                    AND CR.ID_USER = ?
                ORDER BY
                  C.DATE_CREATE ASC";
        $idUser = $this->Auth->currentIdUser();

        $rows = $this->db->query($sql, array($tableName, $keyItem, $idUser))->result_array();
        return $rows;
    }

    /**
     * Функция отметки прочтения сообщений как прочитанных
     * @param $tableName string Наименование таблицы
     * @param $keyItem int Ключ записи
     */
    public function readCommunityList($tableName, $keyItem){
        $idUser = $this->Auth->currentIdUser();
        $sql = "INSERT INTO COMMUNITY_READS (ID_USER, ID_COMMUNITY)
                SELECT
                  {$idUser},
                  C.ID_COMMUNITY
                FROM
                  COMMUNITY AS C
                  INNER JOIN `TABLES` AS T 
                    ON C.ID_TABLE = T.ID_TABLE
                    AND C.DELETED = 0
                    AND T.`NAME` = ?
                    AND C.ID_RECORD = ?
                  LEFT JOIN COMMUNITY_READS AS CR 
                    ON C.ID_COMMUNITY = CR.ID_COMMUNITY
                    AND CR.ID_USER = ?
                WHERE
                  CR.ID_COMMUNITY IS NULL";

        $this->db->query($sql, array($tableName, $keyItem, $idUser));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция отправки сообщения по записи
     * @param $tableName string Наименование таблицы
     * @param $keyItem int Ключ записи
     * @param $sendText string Текст для отправки
     */
    public function sendCommunityText($tableName, $keyItem, $sendText){
        $idUser = $this->Auth->currentIdUser();
        $idTable = $this->Common->getTableId($tableName);
        $sql = "INSERT INTO COMMUNITY (ID_TABLE, ID_RECORD, ID_AUTHOR, MESSAGE) VALUES(?, ?, ?, ?)";
        $this->db->query($sql, array($idTable, $keyItem, $idUser, $sendText));
        if (mysql_error() != ''){
            echo mysql_Error();
        }else {
            $newItem = $this->db->insert_id();
            $sql = "INSERT INTO COMMUNITY_READS (ID_USER, ID_COMMUNITY) VALUES(?, ?)";
            $this->db->query($sql, array($idUser, $newItem));
            if (mysql_error() != ''){
                echo mysql_Error();
            }
        }
    }

    /**
     * Удаление записей
     * @param $tableName Наименование таблицы
     * @param $keyItem Ключ записи
     */
    public function remove($tableName, $keyItem){
        $idTable = $this->Common->getTableId($tableName);
        $this->db->where('ID_TABLE', $idTable)->where('ID_RECORD', $keyItem)->delete('COMMUNITY');
    }
}