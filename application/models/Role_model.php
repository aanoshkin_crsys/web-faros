<?php

class Role_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция получения списка ролей, отсортированных по имени
     * @return mixed
     */
    public function getList(){
        $sql = 'select * from USER_ROLES order by NAME';
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;          
    }

    /**
     * Функция получения деталей по роли
     * @param $idRole int Ключ роли
     * @return array Массив, состоящий из GROUP_NAME => array()
     */
    public function getRoleRights($idRole){
        $groups = $this->getGroups();
        $result = array();
        foreach($groups as $group){
            $result[$group['NAME']] = $this->getRights($idRole, $group['ID_ITEM_GROUP']);
        }
        return $result;
    }

    /**
     * Функция получения списка групп полномочий
     * @return mixed
     */
    public function getGroups(){
        $sql = 'select * from ITEM_GROUPS where ID_KIND = ? order by ROWORDER';
        $rows = $this->db->query($sql, array(KIND_PERMISSIONS))->result_array();
        return $rows;   
    }

    /**
     * Функция получения списка активности полномочий у определенной роли и определенной группы
     * @param $idRole int Ключ роли
     * @param $idGroup int Ключ группы полномочий
     * @return mixed Масссив значений
     */
    public function getRights($idRole, $idGroup){
        $sql = 'select distinct
                    I.ID_ITEM,
                    I.NAME,
                    IF(RP.ID_PERMISSION is NULL, 0, 1) AS HASPERMISSION
                from 
                    ITEM_GROUPS as IG 
                    inner join ITEMS as I 
                        on I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP 
                        and IG.ID_ITEM_GROUP = ?
                    left join ROLE_PERMISSIONS as RP
                        on RP.ID_PERMISSION = I.ID_ITEM
                        and RP.ID_USER_ROLE = ?
                        and I.DELETED = 0
                ORDER BY 
                    I.ROWORDER';
        $rows = $this->db->query($sql, array($idGroup, $idRole))->result_array();
        return $rows;   
    }

    /**
     * Функция установки полномочия для роли
     * @param $idRole int Ключ роли
     * @param $idRight int Ключ полномочия
     */
    public function setRight($idRole, $idRight){
        $sql = "INSERT INTO ROLE_PERMISSIONS (ID_USER_ROLE, ID_PERMISSION) VALUES(?, ?)";
        $rows = $this->db->query($sql, array($idRole, $idRight));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция снятия полномочия с роли
     * @param $idRole int Ключ роли
     * @param $idRight int Ключ полномочия
     */
    public function unsetRight($idRole, $idRight){
        $sql = "DELETE FROM ROLE_PERMISSIONS WHERE ID_USER_ROLE = ? and ID_PERMISSION = ?";
        $rows = $this->db->query($sql, array($idRole, $idRight));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }
    
}