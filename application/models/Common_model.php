<?php

class Common_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');

    }

    function getArrayValue($data, $key, $defaultValue){
        if (array_key_exists($key, $data)){
            return $data[$key];
        }else{
            return $defaultValue;
        }
    }

    //Перевод строки в дату с нужным форматом
    public function strtodate($source){
        return date(DATE_FORMAT, strtotime($source));
    }
    //Перевод строки во время с нужным форматом
    public function strtotime_($source){
        return date(TIME_FORMAT, strtotime($source));
    }
    //Перевод строки в дату и время с нужным форматом
    public function strtodatetime($source){
        if (date("Y", strtotime($source)) == 1970){
            return null;
        }else{
            return date(DATETIME_FORMAT, strtotime($source));
        }
    }

    public function echoImg($src){
        return $src."?sum=".md5_file(base_url($src));
    }

    /**
     * Функция получения ключа таблицы по ее наименованию
     * @param $tableName string Наименование таблицы
     * @return mixed Ключ таблицы
     */
    public function getTableId($tableName){
        $sql = "SELECT
                  ID_TABLE
                FROM
                  `TABLES`
                WHERE
                  NAME = ?";

        return $this->db->query($sql, array($tableName))->row_array()['ID_TABLE'];
    }

    /**
     * Фунция получения видов событий
     * @return mixed
     */
    public function getEventList(){
        $sql = "SELECT
                  IG.ID_ITEM_GROUP,
                  IG.NAME as GROUP_NAME,
                  I.ID_ITEM as ID_EVENT,
                  I.NAME as EVENT_NAME
                FROM
                  -- Справочник группы событий
                  ITEMS as I
                  INNER JOIN ITEM_GROUPS as IG 
                    ON IG.ID_ITEM_GROUP = I.ID_ITEM_GROUP
                  INNER JOIN ITEM_KINDS as IK 
                    ON IK.ID_KIND = IG.ID_KIND
                    AND IK.SYSNAME = 'EVENTS'
                WHERE  
                  I.DELETED = 0
                ORDER BY
                  IG.ROWORDER,
                  I.ROWORDER";

        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция обновления полей в указанной таблице
     * @param $tableName string Наименование таблицы
     * @param $keyField string Наименование ключевого поля
     * @param $keyValue mixed Значение ключевого поля
     * @param $field string Наименованине поля для обновления
     * @param $value mixed Значение поля, которое нужно установить
     * @param $access string Значение права для проверки
     * В случае ошибки выведет информацию об ошибке на экран
     */
    public function update($tableName, $keyField, $keyValue, $field, $value, $access){
        if($this->Auth->isAuth() == false){
            echo ACCESS_NOAUTH;
        } else if (($access != '') && ($this->Auth->userHasAccessRight($access) == false)){
            echo ACCESS_DENIED;
        } else {
            $data = array($field => $value);
            if (!$this->db->where($keyField, $keyValue)->update($tableName, $data)){
                echo $this->db->error()['message'];
            }
        }
    }

    /**
     * Функция прикрепления файлов к записи
     * @param $tableName string Наименование таблицы
     * @param $keyItem int Ключ записи
     * @param $fileName string Имя файла на сервере
     * @param $origName origName Оригинальное имя файла
     * @param $fileSize int размер файла в КБ
     */
    public function addAttach($tableName, $keyItem, $fileName, $origName, $fileSize){
        $data = array(
            'ID_TABLE' => $this->getTableId($tableName),
            'ID_RECORD' => $keyItem,
            'ID_AUTHOR' => $this->Auth->currentIdUser(),
            'FILENAME' => $fileName,
            'FILESIZE' => $fileSize,
            'DISPLAYNAME' =>$origName,
        );

        $this->db->insert(TABLE_ATTACHMENTS, $data);
    }

    /**
     * Функция отправки почтового сообщения
     * @param $dest mixed Список получателей через запятую либо массивом
     * @param $theme string Тема письма
     * @param $message string Тело письма
     */
    public function sendMail($dest, $theme, $message){
        $config['wordwrap'] = FALSE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from(MAIL_FROM_EMAIL, MAIL_FROM_NAME);
        $this->email->to($dest);

        $this->email->subject($theme);
        $this->email->message($message);

        $this->email->send();
    }

    /**
     * Функция генерации случайной строки указанной длины
     * @param int $length длина строки
     * @return string
     */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Функция полученя комментария, по ключу записи
     * @param $keyItem Ключ записи
     * @return string
     */
    public function getItemComment($keyItem){
        return $this->db->where('ID_ITEM', $keyItem)->get('ITEMS')->row_array()['COMMENT'];
    }

    /**
     * Функция получения скалярного значения из запроса
     * @param $sql Текст запроса
     * @param array $params список параметров
     * @return mixed Значение первой колонки первой записи
     */
    public function getSQL($sql, $params=array()){
        $row = $this->db->query($sql, $params)->row_array();
        return array_shift($row);
    }


    /**
     * Функция получения ключа пользователя, если пользователь не найден, то он создается
     * @param $userName Имя пользователя
     * @return int|mixed Ключ найденного пользователя
     */
    public function getUserId($userName){
        $idUser = $this->getSQL('select ID_USER from USERS where NAME like ?', array($userName));
        if (!isset($idUser)){
            $data = array(
               'NAME' => $userName,
            );
            $this->db->insert('USERS', $data);
            $idUser = $this->db->insert_id();
        }
        return $idUser;
    }

    /**
     * Функция получения ключа свойства, при этом если свойства не было, то происходит добавление свойства
     * @param $propName Имя свойства
     * @param $propGroup Группа свойства
     * @param $kindName Вид свойства
     * @return int Полученный ключ
     */
    public function getPropId($propName, $propGroup, $kindName)
    {
        //Получаем ключ свойства, по его наименованию
        $idProp = $this->Common->getSQL('select ID_ITEM from ITEMS where NAME like ? or SYSNAME like ?', array($propName, $propName));
        //Если свойство не найдено
        if (!isset($idProp)) {
            //Получаем ключ группы, куда должно входить свойство
            $idGroup = $this->Common->getSQL('select ID_ITEM_GROUP from ITEM_GROUPS where NAME like ? or SYSNAME like ?', array($propGroup, $propGroup));
            //Если группа не найдена
            if (!isset($idGroup)) {
                //Плолучаем ключ вида записей "СВОЙСТВА"
                $idKind = $this->db->where('SYSNAME', $kindName)->get('ITEM_KINDS')->row_array()['ID_KIND'];
                //Получаем очередной порядковый номер для группы
                $maxOrder = $this->Common->getSQL('select IFNULL(MAX(ROWORDER),0)+1 FROM ITEM_GROUPS WHERE ID_KIND = ?', array($idKind));
                //Формируем данные ...
                $data = array(
                    'ID_KIND' => $idKind,
                    'NAME' => $propGroup,
                    'ROWORDER' => $maxOrder,
                );
                //Вставляем данные в таблицу
                $this->db->insert('ITEM_GROUPS', $data);
                //Получаем новый вставленный ключ - это будет ключ группы
                $idGroup = $this->db->insert_id();
            }
            //Получаем очередной порядковый номер для свойства
            $maxOrder = $this->Common->getSQL('select IFNULL(MAX(ROWORDER),0)+1 FROM ITEMS WHERE ID_ITEM_GROUP = ?', array($idGroup));
            //Формируем данные...
            $data = array(
                'ID_ITEM_GROUP' => $idGroup,
                'NAME' => $propName,
                'ROWORDER' => $maxOrder,
            );
            //Вставляем данные в таблицу
            $this->db->insert('ITEMS', $data);
            //Получаем новый вставленный ключ - это будет ключ свойства
            $idProp = $this->db->insert_id();
        }
        return $idProp;
    }
}