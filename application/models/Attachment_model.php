<?php

class Attachment_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция получения списка прикрепленных файлов
     * @param $tableName string Системное имя таблицы
     * @param $keyItem int Ключ записи
     * @return mixed
     */
    public function getList($tableName, $keyItem)
    {
        $sql = "SELECT
                  A.ID_ATTACHMENT, 
                  CONCAT('/attachment/', T.`NAME`,'/', A.FILENAME) as FNAME,
                  A.FILESIZE, 
                  A.DISPLAYNAME,
                  A.ID_AUTHOR, 
                  U.`NAME` AS AUTHOR_NAME
                FROM
                  ATTACHMENTS AS A
                  INNER JOIN `TABLES` AS T 
                    ON A.ID_TABLE = T.ID_TABLE
                    AND T.`NAME` = ?
                    AND A.ID_RECORD = ?
                    AND A.DELETED = 0
                  LEFT JOIN USERS AS U 
                    ON A.ID_AUTHOR = U.ID_USER";
        $rows = $this->db->query($sql, array($tableName, $keyItem))->result_array();
        return $rows;
    }

    public function linkFile($tableName, $newId, $idAuthor, $file, $displayName)
    {
        if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/attachment/{$tableName}/{$file}")) {
            $tableId = $this->Common->getTableId($tableName);

            $data = array(
                "ID_TABLE" => $tableId,
                "ID_RECORD" => $newId,
                "ID_AUTHOR" => $idAuthor,
                "FILENAME" => $file,
                "FILESIZE" => filesize($_SERVER["DOCUMENT_ROOT"] . "/attachment/{$tableName}/{$file}") / 1024, //Размер в килобайтах
                "DISPLAYNAME" => $displayName,
                "DELETED" => 0,
            );

            $this->db->insert("ATTACHMENTS", $data);
        }
    }

    public function remove($tableName, $keyItem){
        $list = $this->getList($tableName, $keyItem);
        foreach ($list as $item){
            if (file_exists($_SERVER["DOCUMENT_ROOT"].$item['FNAME'])){
                unlink($_SERVER["DOCUMENT_ROOT"].$item['FNAME']);
            }
        }
    }

}