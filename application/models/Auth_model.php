<?php
/**
 * Описание модели авторизации
 */
class Auth_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция проверки наличия авторизации
     * @return bool ИСТИНА, если пользователь авторизован, ЛОЖЬ, в противном случае
     */
    public function isAuth(){
        return $this->session->has_userdata('userAuthData');
    }

    public function currentIdUser(){
        if ($this->isAuth()){
            return $this->session->userdata(COOKIE_AUTHDATA)['ID_USER'];
        }else{
            return 0;
        }
    }

    public function getUserById($keyItem){
        return $this->db->select('LOGIN')->
                    where('ID_USER', $keyItem)->
                    get('USERS', 1)->
                    row_array()['LOGIN'];
    }

    public function getUserIdByLogin($login){
        return $this->db->select('ID_USER')->
                    where('LOGIN', $login)->
                    get('USERS', 1)->
                    row_array()['ID_USER'];
    }

    public function getAvatar($idUser, $avatarName, $class){
       if (($avatarName == '')||(!file_exists($_SERVER["DOCUMENT_ROOT"]."/avatars/{$idUser}/{$avatarName}"))){
           return "<img class='avatar {$class}' src='/avatars/avatar.png'/>";
       } else {
           return "<img class='avatar {$class}' src='/avatars/{$idUser}/{$avatarName}?sum=".md5_file(base_url("/avatars/{$idUser}/{$avatarName}"))."'/>";
       }
    }

    public function currentAvatar($class){
        if ($this->isAuth() && (file_exists($_SERVER["DOCUMENT_ROOT"]."/avatars/".$this->session->userdata(COOKIE_AUTHDATA)['ID_USER']."/".$this->session->userdata(COOKIE_AUTHDATA)['AVATARNAME']))){
            return "<img class='avatar {$class}' src='/avatars/".$this->session->userdata(COOKIE_AUTHDATA)['ID_USER']."/".$this->session->userdata(COOKIE_AUTHDATA)['AVATARNAME']."'/>";
        }else{
            return "<img class='avatar {$class}' src='/avatars/avatar.png'/>";
        }
    }

    /**
     * Функция получения списка групп полномочий, которые имеет указанный пользователь
     * @param $showDescription bool Также выводить в массив Описание группы
     * @return void Возвращает список групп в виде массива (SYSNAME)
     */
    public function getUserPermissionGroups($showDescription = false){
        //Инициализируем результат
        $data = array();
        //Выбираем если, авторизация прошла успешно
        if ($this->isAuth()){
            //Формируем запрос для выборки групп по известной роли
            $sql = 'select distinct
                        IG.SYSNAME,
                        IG.NAME
                    from 
                        ITEM_GROUPS as IG 
                        inner join ITEMS as I 
                            on I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP 
                            and IG.ID_KIND = ? 
                        inner join ROLE_PERMISSIONS as RP
                            on RP.ID_PERMISSION = I.ID_ITEM
                            and RP.ID_USER_ROLE = ?
                            and I.DELETED = 0
                    ORDER BY 
                        IG.ROWORDER';
            //Получаем ключ роли из куки
            $idRole =  $this->session->userdata(COOKIE_AUTHDATA)['ID_USER_ROLE'];
            //Выбираем данные из БД
            $rows = $this->db->query($sql, array(KIND_PERMISSIONS, $idRole))->result_array();
            //Формируем результат массив
            foreach($rows as $row){
                if ($showDescription == TRUE) {
                    $data[] = array('SYSNAME' => $row['SYSNAME'], 'NAME' => $row['NAME']);
                }else{
                    $data[] = $row['SYSNAME'];
                }
            }
        }
        //Возвращаем массив значений
        return $data;
    }

    /**
     * Функция получает список полномочий, назначенных на текущего пользователя
     * @return array Возвращает список, состоящий из SYSNAME полномочий
     */
    public function getUserPermissions(){
        //Инициализируем результат
        $data = array();
        //Выбираем если, авторизация прошла успешно
        if ($this->isAuth()){
            //Формируем запрос для выборки полномочий по известной роли
            $sql = 'select distinct
                        I.SYSNAME
                    from 
                        ITEMS as I 
                        inner join ROLE_PERMISSIONS as RP
                            on RP.ID_PERMISSION = I.ID_ITEM
                            and RP.ID_USER_ROLE = ?
                            and I.DELETED = 0
                    order by
                        I.ROWORDER';
            //Получаем ключ роли из куки
            $idRole =  $this->session->userdata(COOKIE_AUTHDATA)['ID_USER_ROLE'];
            //Выбираем данные из БД
            $rows = $this->db->query($sql, array($idRole))->result_array();
            //Формируем результат массив
            foreach($rows as $row){
                $data[] = $row['SYSNAME'];
            }
        }
        //Возвращаем массив значений
        return $data;
    }

    /**
     * Функция авторизации пользователя на сайте
     * @param $inLogin string Логин пользователя
     * @param $inPassword string Пароль для проверки
     * @return bool Возвращает ИСТИНА, если указанная пара логин/пароль существует и верна, ЛОЖЬ в противном случае
     */
    public function doAuth($inLogin, $inPassword){
        //Выбираем все записи, из таблицы Пользователей, активных и имеющих указанный пароль
        $sql = 'select * from USERS WHERE LOGIN = ? and (PASSWORD = ? or PASSWORD is NULL ) and ENABLED = 1 and DELETED = 0';
        //Выбираем из базы данных одну строку результата
        $row = $this->db->query($sql, array( $this->db->escape_like_str($inLogin), sha1($inPassword)))->row_array();
        //Если строка существует, то
        if (isset($row)){
            //Сохраняем строку в куки
            $this->session->set_userdata(COOKIE_AUTHDATA, $row);
            //Получаем список разрешенных групп полномочий
            $data = $this->getUserPermissionGroups();
            $this->session->set_userdata(COOKIE_ACCESS_GROUPS, $data);
            //Получаем список разрешенных полномочий
            $data = $this->getUserPermissions();
            $this->session->set_userdata(COOKIE_ACCESS_RIGHTS, $data);
            //Сохраняем данные для автоматической вставке в дальнейшем
            set_cookie(COOKIE_LOGIN, $inLogin, 2592000);
            set_cookie(COOKIE_PASSWORD, $inPassword, 2592000);
            $this->update(0, 'LAST_VISIT', "'".date('Y-m-d H:i:s')."'");
            return true;
        }else{
            //Если строки не существует, т.е. не имеется корректной пары Логин/Пароль
            return false;
        }
    }

    /**
     * Функция получения списка доступных пользователей, с указанной ролью
     * @param $roleName str Наименование роли (сиситемное название)
     * @return mixed
     */
    public function getUsersByRole($roleName){
        $sql = "SELECT 
                  U.ID_USER,
                  IF(U.`NAME` IS NULL, U.LOGIN, U.`NAME`) AS USER_NAME
                FROM
                  USERS AS U
                  INNER JOIN USER_ROLES AS UR 
                    ON U.ID_USER_ROLE = UR.ID_USER_ROLE 
                    AND U.DELETED = 0 
                    AND U.ENABLED = 1
                    AND UR.SYSNAME = ?
                ORDER BY
                  USER_NAME";
        $rows = $this->db->query($sql, array($roleName))->result_array();
        return $rows;
    }

    /**
     * Функция получения списка групп пользователей
     * @return mixed
     */
    public function getUserGroups(){
        $sql = 'SELECT
                  *
                FROM
                  USER_GROUPS
                ORDER BY
                  NAME';
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция определения наличия указанной группы у текущего пользователя
     * @param $inName string Системное имя группы, для проверки
     * @return bool Возвращает ИСТИНА, если указанная группа у текущего пользователя доступна, ЛОЖЬ в противном случае
     */
    public function userHasAccessGroup($inName){
        if ($this->isAuth()){
            return in_array($inName, $this->session->userdata(COOKIE_ACCESS_GROUPS));
        }else{
            return false;
        }
    }

    /**
     * Функция определения наличия указанного полномочия у текущего пользователя
     * @param $inName string Системное имя полномочия, для проверки
     * @return bool Возвращает ИСТИНА, если указанное полномочие у текущего пользователя доступна, ЛОЖЬ в противном случае
     */
    public function userHasAccessRight($inName){
        if ($this->isAuth()){
            return in_array($inName, $this->session->userdata('accessRights'));
        }else{
            return false;
        }
    }

    /**
     * Функция получения полного имени пользователя (зависит от наличия поля NAME и авторизованности пользователя)
     * @return string Полное имя пользователя
     */
    public function getDisplayName(){
        //Получаем имя и логин
        $fio   = $this->session->userdata('userAuthData')['NAME'];
        $login = $this->session->userdata('userAuthData')['LOGIN'];
        if (!$this->isAuth()){
            //Если авторизации не было
            $displayName = "Не известно";
        }else if (isset($fio) && ($fio != '')){
            //Если было заполнено ФИО
            $displayName = "$fio ($login)";
        }else{
            $displayName = "$login";
        }
        return $displayName;
    }

/**
     * Функция получения полного имени пользователя (зависит от наличия поля NAME и авторизованности пользователя)
     * @return string Полное имя пользователя
     */
    public function getUserName(){
        //Получаем имя и логин
        $fio   = $this->session->userdata('userAuthData')['NAME'];
        $login = $this->session->userdata('userAuthData')['LOGIN'];
        if (!$this->isAuth()){
            //Если авторизации не было
            $displayName = "Не известно";
        }else if (isset($fio) && ($fio != '')){
            //Если было заполнено ФИО
            $displayName = "$fio";
        }else{
            $displayName = "$login";
        }
        return $displayName;
    }

    /**
     * Функция выхода пользователя из системы
     */
    public function doLogout(){
        //Удаляем все данные из куки
        $this->session->unset_userdata('userAuthData');
        //Очищаем куки для атоматического входа
        set_cookie('userPass', '', 0);
    }

    /**
     * Функция получения информации по указанному сотруднику
     * @param $idUser int Ключ сотрудника
     * @return array строка из таблицы USERS
     */
    public function getUserInfo($idUser){
        $sql = 'SELECT
                  U.ID_USER, 
                  U.ID_USER_GROUP, 
                  U.LOGIN, 
                  U.`NAME`, 
                  U.ID_USER_ROLE, 
                  U.PHONE, 
                  U.`COMMENT`, 
                  U.ENABLED, 
                  U.DELETED, 
                  UG.`NAME` AS USER_GROUP, 
                  UR.`NAME` AS USER_ROLE, 
                  UR.`COMMENT` AS USER_ROLE_COMMENT,
                  U.LAST_VISIT,
                  U.DATE_REGISTER,
                  U.AVATARNAME
                FROM
                  USERS AS U
                  INNER JOIN USER_GROUPS AS UG 
                    ON U.ID_USER_GROUP = UG.ID_USER_GROUPS
                  INNER JOIN USER_ROLES AS UR 
                    ON U.ID_USER_ROLE = UR.ID_USER_ROLE
                WHERE
                  U.ID_USER = ?';
        $rows = $this->db->query($sql, array($idUser))->row_array();
        return $rows;
    }

    /**
     * Функция перемещения пользователей в другую группу
     * @param $idUsers str Ключи пользователей, через запятую
     * @param $newId int Кллюч новой группы
     */
    public function changeGroup($idUsers, $newId){
        $sql = "UPDATE USERS SET ID_USER_GROUP = ? WHERE ID_USER in ({$idUsers})";
        $this->db->query($sql, array($newId));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция логического удаления пользователей
     * @param $idUsers str Список пользователей, через запятую
     */
    public function remove($idUsers){
        $sql = "UPDATE USERS SET DELETED = 1 WHERE ID_USER in ({$idUsers})";
        $this->db->query($sql, array());
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция обновления полей в таблице Пользователей
     * @param $idUser int Ключ пользователя
     * @param $field string Наименованине поля для обновления
     * @param $value mixed Значение поля, которое нужно установить
     * В случае ошибки выведет информацию об ошибке на экран
     */
    public function update($idUser, $field, $value){
        $this->Common->update(TABLE_USERS, 'ID_USER', $idUser, $field, $value, '');
    }

    /**
     * Функция смены пароля у указанного пользователя
     * @param $idUser int Ключ пользователя
     * @param $newPassword string Значение нового пароля
     * В случае ошибки выведет информацию об ошибке на экран
     */
    public function changePassword($idUser, $newPassword){
        $sql = "UPDATE USERS SET PASSWORD = ? WHERE ID_USER = ?";
        $this->db->query($sql, array(sha1($newPassword), $idUser));
        if (mysql_error() != ''){
         echo mysql_Error();
        }
    }

    /**
     * Функция получения списка пользователей, сортированных по группам и алфавитному наименованию
     * @return mixed
     */
    public function getList(){
        $sql = "select 
                    UG.ID_USER_GROUPS, 
                    UG.`NAME` AS GROUP_NAME,
                    U.ID_USER, 
                    IFNULL(U.`NAME`,'') AS USER_NAME,
                    IFNULL(U.LOGIN,'') AS LOGIN,
                    U.ENABLED,
                    IFNULL(UR.`NAME`,'') AS ROLE_NAME,
                    U.AVATARNAME
                from 
                    USER_GROUPS AS UG
                    LEFT JOIN USERS AS U 
                        ON UG.ID_USER_GROUPS = U.ID_USER_GROUP
                        AND U.DELETED = 0
                    LEFT JOIN USER_ROLES AS UR 
                        ON U.ID_USER_ROLE = UR.ID_USER_ROLE
                ORDER BY 
                    GROUP_NAME,
                    USER_NAME";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция получения списка объектов, связанных с пользователем
     * @param $idUser int Ключ пользователя
     * @return mixed Список связанных объектов
     */
    public function getLinkedObjects($idUser){

        $sql = "SELECT
                  OG.ID_OBJECT_GROUP, 
                  OG.`NAME` AS GROUP_NAME,
                  O.ID_OBJECT AS ID_OBJECT, 
                  O.`NAME` AS OBJECT_NAME, 
                  IF(OA.ID_OBJECT IS NULL, 0, 1) AS HASACCESS  
                FROM
                  OBJECT_GROUPS AS OG 
                  INNER JOIN OBJECTS AS O
                    ON O.ID_OBJECT_GROUP = OG.ID_OBJECT_GROUP
                    AND O.DELETED = 0
                  LEFT JOIN OBJECT_ACCESS AS OA 
                    ON O.ID_OBJECT = OA.ID_OBJECT 
                    AND OA.ID_USER = ?
                ORDER BY
                  OG.ROWORDER,
                  OBJECT_NAME";
        $rows = $this->db->query($sql, array($idUser))->result_array();
        return $rows;
    }

    /**
     * Функция добавления объекта к пользователю
     * @param $idUser int  Ключ пользователя
     * @param $idObject int Ключ объекта
     */
    public function addLinkedObject($idUser, $idObject){
        $sql = "INSERT INTO OBJECT_ACCESS (ID_USER, ID_OBJECT) VALUES({$idUser}, {$idObject})";
        $this->db->query($sql, array());
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция удаления объекта от пользователя
     * @param $idUser int Ключ пользователя
     * @param $idObject int Ключ объекта
     */
    public function removeLinkedObject($idUser, $idObject){
        $sql = "DELETE FROM OBJECT_ACCESS WHERE ID_USER = ? AND ID_OBJECT = ?";
        $this->db->query($sql, array($idUser, $idObject));
        if (mysql_error() != ''){
            echo mysql_Error();
        }
    }

    /**
     * Функция добавления нового пользователя
     * @param $login string Логин пользователя
     * @param $fio string Фамилия, Имя Отчество
     * @param $phone string Телефон пользователя
     * @param $comment string Примечание
     * @param $pass string Пароль
     * @return string Возвращает ошибку
     */
    public function register($login, $fio, $phone, $comment, $pass){
        //Проверяем наличие логина
        $sql = "select count(*) as Cnt from USERS where LOGIN like ?";
        $cnt = $this->db->query($sql, array($login))->row_array()['Cnt'];
        if ($cnt == 0){
            $data=array(
                'LOGIN' => $login,
                'ID_USER_GROUP' => DEF_USER_GROUP,
                'PASSWORD' => sha1($pass),
                'NAME' => $fio,
                'COMMENT' => $comment,
                'PHONE' => $phone,
                'ENABLED' => 0,
                'DELETED' => 0,
                'ID_USER_ROLE' => DEF_USER_ROLE,
            );
            if (!$this->db->insert('USERS', $data)){
                return $this->db->error()['message'];
            }else{
                $this->Common->sendMail($login, 'Регистрация нового пользователя',
                    "
<b>Уважаемый {$fio}!</b><br>
От вашего имени был зарегистрирован пользователь в системе CRSYS.TECH. <br>
Если вы не производили никаких действий, вы можете обратиться к администраторам системы для сообщения о возможном мошеничестве на электронный адрес: support@crsys.tech. <br>
В настоящий момент специалисты проиводят обработку ваших данных. Когда Ваша учетная запись будет активирована, вам придет соответствующее письмо. <br>
<br>
С уважением,<br>
Администрация CRSYS.TECH.
");
            }
        }else{
            return "Пользователь с логином [{$login}] уже зарегистрирован в системе.";
        }
    }

    /**
     * Функция создания письма для восстановления пароля
     * @param $login string Логин
     * @param $randomStr string Случайная строка
     * @return string Возможная ошибка
     */
    public function restore($login, $randomStr){
        //Проверяем наличие логина
        $sql = "select count(*) as Cnt from USERS where LOGIN like ?";
        $cnt = $this->db->query($sql, array($login))->row_array()['Cnt'];
        if ($cnt == 0){
            return "Логин [{$login}] не найден.";
        }else{
            $sql = "UPDATE USERS SET RESTORE = ? WHERE LOGIN like ?";
            $this->db->query($sql, array($randomStr, $login));
        }
    }

    /**
     * Функция смены пароля по известному коду
     * @param $code string Секретный код
     * @param $pass string Новый пароль
     * @return string Возможная ошибка
     */
    public function restore_password($code, $pass){
        if ($code == ''){
            return "Требуется ввести секретный код.";
        }else if ($pass == ''){
            return "Требуется указать пароль";
        }else{
            //Проверяем наличие логина
            $sql = "select count(*) as Cnt from USERS where RESTORE like ?";
            $cnt = $this->db->query($sql, array($code))->row_array()['Cnt'];
            if ($cnt == 0){
                return "Нет логина с указанным секретным кодом.";
            }else{
                //Проверяем наличие логина
                $sql = "UPDATE USERS SET PASSWORD = ?, RESTORE = null WHERE RESTORE = ?";
                $this->db->query($sql, array(sha1($pass), $code));
            }
        }
    }
}