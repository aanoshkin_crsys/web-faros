<?php

class Report_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Функция получения списка ролей, отсортированных по имени
     * @return mixed
     */
    public function getList($idUser, $idAuthor, $idDest, $accepted, $objectFilter, $communityFilter, $textFilter, $dateStart, $dateFinish){
        $idTable = $this->Common->getTableId('REPORTS');
        $sql = "SELECT
                  R.ID_REPORT, 
                  R.ID_USER, 
                  D.`NAME` AS USER_NAME,
                  D.AVATARNAME as USER_AVATARNAME, 
                  R.ID_AUTHOR, 
                  U.`NAME` AS AUTHOR_NAME,
                  U.AVATARNAME as AUTHOR_AVATARNAME, 
                  R.ID_OBJECT, 
                  O.`NAME` AS OBJECT_NAME,
                  R.DATE_CREATE, 
                  R.THEME, 
                  R.ACCEPTED,
                  ( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = R.ID_REPORT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) AS COMMUNITY
                FROM
                  REPORTS AS R
                  INNER JOIN USERS AS U 
                    ON R.ID_AUTHOR = U.ID_USER
                  INNER JOIN USERS AS D 
                    ON R.ID_USER = D.ID_USER
                  INNER JOIN OBJECTS AS O 
                    ON R.ID_OBJECT = O.ID_OBJECT
                WHERE
                  (R.ID_USER = {$idUser} OR
                   R.ID_AUTHOR = {$idUser})";
        if ($objectFilter != ''){
            $sql = $sql." AND R.ID_OBJECT in ({$objectFilter})";
        }
        if ($textFilter != ''){
            $sql = $sql." AND (R.THEME like '%{$textFilter}%' OR R.MESSAGE like '%{$textFilter}%')";
        }
        if ($dateStart != ''){
            $sql = $sql." AND DATE(R.DATE_CREATE) >= STR_TO_DATE('{$dateStart}', '%d.%m.%Y')";
        }
        if ($dateFinish != ''){
            $sql = $sql." AND DATE(R.DATE_CREATE) >= STR_TO_DATE('{$dateFinish}', '%d.%m.%Y')";
        }
        if ($idAuthor != ''){
            $sql = $sql." AND R.ID_AUTHOR in ({$idAuthor})";
        }
        if ($idDest != ''){
            $sql = $sql." AND R.ID_USER in ({$idDest})";
        }
        if ($accepted == 0){
            $sql = $sql." AND R.ACCEPTED = 0";
        }else if ($accepted == 1){
            $sql = $sql." AND R.ACCEPTED = 1";
        }
        if ($communityFilter == 0){
            $sql = $sql." AND (( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = R.ID_REPORT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) is null)";
        }else if ($communityFilter == 1) {
            $sql = $sql." AND (( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = R.ID_REPORT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) is not null)";
        }else if ($communityFilter == 2) {
            $sql = $sql." AND (( SELECT 
                      IF(CR.ID_USER IS NULL, ID_AUTHOR, {$idUser}) 
                    FROM 
                      COMMUNITY AS C 
                      LEFT JOIN COMMUNITY_READS AS CR
                        ON CR.ID_COMMUNITY = C.ID_COMMUNITY
                        AND CR.ID_USER = {$idUser}
                    WHERE 
                      C.ID_TABLE = {$idTable} 
                      AND C.ID_RECORD = R.ID_REPORT 
                      AND C.DELETED = 0 
                    ORDER BY 
                      C.DATE_CREATE DESC 
                    LIMIT 1) <> {$idUser})";
        }

        $sql=$sql." ORDER BY R.DATE_CREATE DESC";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    public function getInfo($idReport){
        $sql = "SELECT
                  R.ID_REPORT, 
                  R.ID_USER, 
                  D.`NAME` AS USER_NAME,
                  D.AVATARNAME as USER_AVATARNAME, 
                  R.ID_AUTHOR, 
                  U.`NAME` AS AUTHOR_NAME,
                  U.AVATARNAME as AUTHOR_AVATARNAME, 
                  R.ID_OBJECT, 
                  O.`NAME` AS OBJECT_NAME,
                  R.DATE_CREATE, 
                  R.THEME, 
                  R.ACCEPTED,
                  R.MESSAGE
                FROM
                  REPORTS AS R
                  INNER JOIN USERS AS U 
                    ON R.ID_AUTHOR = U.ID_USER
                  INNER JOIN USERS AS D 
                    ON R.ID_USER = D.ID_USER
                  INNER JOIN OBJECTS AS O 
                    ON R.ID_OBJECT = O.ID_OBJECT
                WHERE
                  R.ID_REPORT = ?";
        $rows = $this->db->query($sql, array($idReport))->row_array();
        return $rows;
    }

    /**
     * Функция получения списка авторов доступных отчетов
     * @param $idUser int Ключ пользователя
     * @return mixed
     */
    public function getAuthorList($idUser){
        $sql = "SELECT DISTINCT
                  R.ID_AUTHOR as ID_ITEM, 
                  U.`NAME` as ITEM_NAME
                FROM
                  REPORTS AS R
                  INNER JOIN USERS AS U 
                    ON R.ID_AUTHOR = U.ID_USER
                WHERE
                  R.ID_USER = ? OR
                  R.ID_AUTHOR = ? 
                ORDER BY 
                  ITEM_NAME";
        $rows = $this->db->query($sql, array($idUser, $idUser))->result_array();
        return $rows;
    }

    /**
     * Функция получения списка получателей доступных отчетов
     * @param $idUser int Ключ пользователя
     * @return mixed
     */
    public function getDestList($idUser){
        $sql = "SELECT DISTINCT
                  R.ID_USER as ID_ITEM, 
                  U.`NAME` as ITEM_NAME
                FROM
                  REPORTS AS R
                  INNER JOIN USERS AS U 
                    ON R.ID_USER = U.ID_USER
                WHERE
                  R.ID_USER = ? OR
                  R.ID_AUTHOR = ? 
                ORDER BY
                  ITEM_NAME";
        $rows = $this->db->query($sql, array($idUser, $idUser))->result_array();
        return $rows;
    }

    /**
     * Функция получения списка объектов доступных отчетов
     * @param $idUser int Ключ пользователя
     * @return mixed
     */
    public function getObjectList($idUser){
        $sql = "SELECT DISTINCT
                  R.ID_OBJECT as ID_ITEM, 
                  O.`NAME` AS ITEM_NAME,
                  OG.NAME as GROUP_NAME
                FROM
                  REPORTS AS R
                  INNER JOIN OBJECTS AS O 
                    ON R.ID_OBJECT = O.ID_OBJECT
                  INNER JOIN OBJECT_GROUPS as OG 
                    ON OG.ID_OBJECT_GROUP = O.ID_OBJECT_GROUP
                WHERE
                  R.ID_USER = ? OR
                  R.ID_AUTHOR = ? 
                ORDER BY
                  OG.ROWORDER,
                  ITEM_NAME";
        $rows = $this->db->query($sql, array($idUser, $idUser))->result_array();
        return $rows;
    }

    /**
     * Функция получения списка видов отчетов
     * @return mixed
     */
    public function getReportTypeList(){
        $sql = "SELECT DISTINCT
                  RB.ID_REPORT_BUILDER, 
                  RB.`NAME`,
                  RB.LINK,
                  RBG.NAME as GROUP_NAME
                FROM
                  REPORT_BUILDER as RB
                  INNER JOIN REPORT_BUILDER_GROUPS as RBG 
                    ON RBG.ID_BUILDER_GROUP = RB.ID_BUILDER_GROUP
                ORDER BY
                  RBG.ROWORDER,
                  RB.ROWORDER";
        $rows = $this->db->query($sql, array())->result_array();
        return $rows;
    }

    /**
     * Функция добавления отчета в БД
     * @param $idObject ключ объекта
     * @param $idDest Ключ клиента
     * @param $idAuthor Ключ автора
     * @param $theme Тема отчета
     * @param $message Сопроводительный текст
     * @return int Ключ нового добавленного отчета
     */
    public function addReport($idObject, $idDest, $idAuthor, $theme, $message){
        $data = array(
            "ID_USER" => $idDest,
            "ID_AUTHOR" => $idAuthor,
            "ID_OBJECT" => $idObject,
            "THEME" => $theme,
            "MESSAGE" => $message,
            "DELETED" => 0,
            "ACCEPTED" => 0,
        );

        $this->db->insert("REPORTS", $data);

        return $this->db->insert_id();
    }

    /**
     * Функция удаления отчета
     * @param $idReport Ключ отчета
     * @param int $removeAttahced Удалять также прикрепленные файлы
     */
    public function remove($idReport, $removeAttahced = 1){
        if ($removeAttahced == 1){
            $this->Attach->remove(TABLE_REPORTS, $idReport);
        }
        $this->Community->remove(TABLE_REPORTS, $idReport);

        $this->db->where('ID_REPORT', $idReport)->delete(TABLE_REPORTS);
    }
}