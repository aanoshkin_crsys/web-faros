<?php

class Chart_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    function getConfigValue($data, $key, $defaultValue){
        if (array_key_exists($key, $data)){
            return $data[$key];
        }else{
            return $defaultValue;
        }
    }

    /**
     * Функция для формирования линейной диаграммы в формате SVG
     * @param $name string Наименование блока
     * @param $width float Ширина диаграммы
     * @param $height float Высота диаграммы
     * @param $data mixed Данные по диаграмме
     *      array(
     *          "one"=>array(10, 4, 15, 20, 3),
     *          "two"=>array(3, 7.5, 1, 15 )
     *      ),
     * @param array $config Настройка диаграммы
     *   Возможные параметры
     *      min_value float - минимальное значение по У (ищется автоматически)
     *      max_value float - максимальное значение по У (ищется автоматически)
     *      show-value-point bool - Отображать ли точки с значениями (true)
     *      left_offset float - Отступ слева до диаграммы (40)
     *      right_offset float - Отступ справа до диаграммы (40)
     *      top_offset float - Отступ сверху до диаграммы (5)
     *      bottom_offset float - Отступ снизу до диаграммы (20)
     *      xaxis array - Список подписей по оси Х
     *
     * @return string Изображение в формате SVG
     */
    public function lines($name, $width, $height, $data, $config = array()){
        $colors=array('FireBrick', 'DarkViolet');

        $res = "";
        //Находим максимальные значения и минимальные
        $first = true;
        $min   = 0;
        $max   = 0;
        foreach($data as $key=>$row){
            foreach($row as $value){
                if ($first){
                    $min = $value;
                    $max = $value;
                    $first = false;
                }else if ($value > $max){
                    $max = $value;
                }else if ($value < $min){
                    $min = $value;
                }
            }
        }

        $min = $this->getConfigValue($config, 'min_value', $min);
        $max = $this->getConfigValue($config, 'max_value', $max);
        $showValuePoint =$this->getConfigValue($config, 'show-value-point', true);
        //Отступ слева для первого значения
        $leftOffset = $this->getConfigValue($config, 'left_offset', 40);
        //Отступ справа для первого значения
        $rightOffset= $this->getConfigValue($config, 'right_offset', 40);
        //Отступ справа для первого значения
        $topOffset= $this->getConfigValue($config, 'top_offset', 5);
        //Отступ справа для первого значения
        $bottomOffset= $this->getConfigValue($config, 'bottom_offset', 20);
        //Получаем значения подписей по оси Х
        $xTexts = $this->getConfigValue($config, 'xaxis', array());
        //Добавляем пустые подписи для максимального значения подписей
        foreach($data as $key=>$row){
            if (count($xTexts) < count($row)){
                for($idx = count($xTexts); $idx < count($row); $idx++){
                    $xTexts[] = '';
                }
            }
        }
        //Основное полотно
        $res.="<svg id='{$name}' xmlns='http://www.w3.org/2000/svg' version='1.1' width='{$width}' height='{$height}'>";
        //меньшаем для подписи снизу
        $height = $height - $bottomOffset;
        $yKoeff = ($height- $topOffset) / ($max - $min);
        //Фон
        $res.="<rect x='0' y='0' width='{$width}' height='{$height}' stroke='gray' stroke-width = '1px' fill = 'white' />";
        //Вертикальные линии
        $x = $leftOffset;
        //Шаг приращения
        $xStep = ($width - $leftOffset - $rightOffset) / (count($xTexts)-1);
        foreach($xTexts as $xText){
            $res.="<line x1='{$x}' y1='0' x2='{$x}' y2='{$height}' stroke='lightgray' stroke-width='1px' stroke-dasharray='10px'/>";
            $x = $x + $xStep;
        }
        $res.="<line x1='{$x}' y1='0' x2='{$x}' y2='{$height}' stroke='lightgray' stroke-width='1px' stroke-dasharray='10px' />";
        //Рисуем линии
        $classNumber = 0;
        foreach($data as $key=>$row){
            $valuePoints = '';
            $res.="<g><polyline stroke='{$colors[$classNumber]}' stroke-width='3px' fill='none' points='";
            $x = $leftOffset;
            foreach($row as $value){
                $y = $value;
                if ($y > $max){
                    $y = $max;
                }else if ($y < $min){
                    $y = $min;
                }
                $y = $height - (($y - $min) * $yKoeff);
                $res.="{$x},{$y} ";

                if ($showValuePoint){
                    $valuePoints .= "<circle cx='{$x}' cy='{$y}' stroke='white' stroke-width='2px' r='6px' fill='{$colors[$classNumber]}' cursor='pointer'><title>{$value}</title></circle>";
                }

                $x = $x + $xStep;
            }
            $res.="' />{$valuePoints}</g>";
            $classNumber++;
        }
        //Рисуем подписи
        $x = $leftOffset;
        $y = $height + $bottomOffset - 3;
        foreach($xTexts as $xText){
            $res.="<text text-anchor='middle' x='{$x}' y='{$y}' fill='gray' font-size='10pt'>{$xText}</text>";
            $x = $x + $xStep;
        }
        $res.="</svg>";

        return $res;
    }

    /**
     * Функция построения гистограммы
     * @param $name string Наименование блока
     * @param $width float Ширина гистограммы
     * @pram $height float Высота гистограммы
     * @param $data mixed данные в формате Показатель => Значение
     * @param array $config Настройка диаграммы
     *      left_offset float - отсутп от диаграммы слева (1/3 ширины)
     *      right_offset float - отступ от диаграммы справа (30)
     *      block_offset float - расстояние между блоками (5)
     *      block_height float - высота блока (25)
     *      text_offset float - Отсутп между текстом и диаграммой (5)
     *      show-value-point bool - Отображать ли точки с значениями (true)
     * @return string Изобраение в формаае SVG
     */
    public function hystogram($name, $width, $height, $data, $config = array()){
        $colors=array('FireBrick', 'DarkViolet');
        $left_offset = $this->getConfigValue($config, 'left_offset', $width / 3);
        $rigth_offset = $this->getConfigValue($config, 'right_offset', 30);
        $block_offset = $this->getConfigValue($config, 'block_offset', 5);
        $block_height = $this->getConfigValue($config, 'block_height', 25);
        $text_offset = $this->getConfigValue($config, 'text_offset', 5);
        $showValuePoint =$this->getConfigValue($config, 'show-value-point', true);

        $max = 0;
        $first = True;
        //Ищем максимальное значение
        foreach ($data as $key=>$value){
            if ($first){
                $max = $value;
                $first = false;
            }else if ($max < $value){
                $max = $value;
            }
        }
        //Коэффициент по Х
        if ($max != 0){
            $koefX = ($width - $left_offset - $rigth_offset - $text_offset) / $max;
        }else{
            $koefX = 0;
        }

        if ($height == 0){
            $height = $block_height * count($data) + $block_offset * (count($data) - 1);
        }else if (count($data) != 0){
            $block_height = ($height - $block_offset * (count($data) - 1)) / count($data);
        }else{
            $block_height = 0;
        }

        //Основное полотно
        $res ="<svg id='{$name}' xmlns='http://www.w3.org/2000/svg' version='1.1' width='{$width}' height='{$height}'>";
        $top = $block_height / 2 + 5;
        $topValue = $block_height / 2;
        $topBlock = 0;
        foreach ($data as $key=>$value){
            $blockX = $left_offset + $text_offset;
            $blockWidth = $value * $koefX;
            $res.="<text text-anchor='end' x='{$left_offset}' y='{$top}' fill='black' font-size='10pt'>{$key}</text>";
            $res.="<rect x='{$blockX}' y='{$topBlock}' width='{$blockWidth}' height='{$block_height}' stroke='FireBrick' stroke-width = '0px' fill = 'FireBrick' />";
            if ($showValuePoint){
                $blockX = $blockX + $blockWidth;
                $res .= "<circle cx='{$blockX}' cy='{$topValue}' stroke='white' stroke-width='2px' r='5px' fill='Gray' cursor='pointer'><title>{$value}</title></circle>";
                $blockX = $blockX + 10;
                $res.="<text text-anchor='start' x='{$blockX}' y='{$top}' fill='gray' font-size='10pt'>{$value}</text>";
            }

            $top = $top + $block_offset + $block_height;
            $topBlock = $topBlock + $block_offset + $block_height;
            $topValue = $topValue + $block_offset + $block_height;
        }
        $res.="</svg>";

        return $res;
    }
}