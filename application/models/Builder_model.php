<?php

class Builder_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getHeader($caption){
        return "
            <div class='data-header-left'>
                {$caption}
            </div>
            <div class='data-header-right'>
                <img src='/assets/images/reports/logo.png'/>
            </div>
        ";
    }

    public function getFooter(){
        return "
            <div class='data-footer-left' height='5mm'>
            </div>
            <div class='data-footer-right1' heigh='1cm'>
                {PAGENO}
            </div>
            <div class='data-footer-right2' height='5mm'>
            </div>
         ";
    }

    public function getEventDay($idObject, $dateStart, $dateFinish, $eventGroup){
        $sql = "SELECT
                  OE.ID_OBJECT_EVENT, 
                  OE.ID_OBJECT, 
                  OE.ID_USER, 
                  OE.DATE_CREATE, 
                  OE.ID_EVENT, 
                  OE.MESSAGE, 
                  OE.PERSON, 
                  OE.DATE_EVENT, 
                  OE.`VIEW`, 
                  OE.`DATA`, 
                  OE.ID_CONTROLLER, 
                  OE.DATE_CONTROLL, 
                  U.`NAME` AS USER_NAME, 
                  I.`NAME` AS EVENT_NAME, 
                  IG.`NAME` AS GROUP_EVENT_NAME
                FROM
                  OBJECT_EVENTS AS OE
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                  INNER JOIN ITEMS AS I 
                    ON OE.ID_EVENT = I.ID_ITEM
                  INNER JOIN ITEM_GROUPS AS IG 
                    ON I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP
                WHERE
                  OE.ID_OBJECT = ?
                  AND OE.DELETED = 0
                  AND OE.DATE_CONTROLL is not null";

        if ($eventGroup !='')
                  $sql = $sql." AND IG.SYSNAME in ({$eventGroup})";
        if ($dateStart != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) >= STR_TO_DATE('{$dateStart}', '%d.%m.%Y')";
        }

        if ($dateFinish != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) <= STR_TO_DATE('{$dateFinish}', '%d.%m.%Y')";
        }

        $sql = $sql." ORDER BY
                  IG.ROWORDER,
                  OE.DATE_EVENT";
        $rows = $this->db->query($sql, array($idObject))->result_array();
        return $rows;
    }

    public function getEventCnt($idObject, $dateStart, $dateFinish, $eventGroup){
        $sql = "SELECT
                  OE.`PERSON` AS ITEM_NAME, 
                  COUNT(1) AS QNT
                FROM
                  OBJECT_EVENTS AS OE
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                  INNER JOIN ITEMS AS I 
                    ON OE.ID_EVENT = I.ID_ITEM
                  INNER JOIN ITEM_GROUPS AS IG 
                    ON I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP
                WHERE
                  OE.ID_OBJECT = ?
                  AND OE.DELETED = 0
                  AND OE.DATE_CONTROLL is not null";

        if ($eventGroup !='')
            $sql = $sql." AND IG.SYSNAME in ({$eventGroup})";
        if ($dateStart != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) >= STR_TO_DATE('{$dateStart}', '%d.%m.%Y')";
        }

        if ($dateFinish != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) <= STR_TO_DATE('{$dateFinish}', '%d.%m.%Y')";
        }

        $sql = $sql."
                  GROUP BY
                    OE.`PERSON`
                  ORDER BY
                    IG.ROWORDER,
                    QNT DESC";
        $rows = $this->db->query($sql, array($idObject))->result_array();
        return $rows;
    }

    public function getEventCntByMonth($idObject, $dateStart, $dateFinish, $eventGroup){
        $sql = "SELECT
                  COUNT(1) as QNT,
                  date_format(OE.DATE_EVENT, '%m.%Y') as GROUPDATE
                FROM
                  OBJECT_EVENTS AS OE
                  INNER JOIN USERS AS U 
                    ON OE.ID_USER = U.ID_USER
                  INNER JOIN ITEMS AS I 
                    ON OE.ID_EVENT = I.ID_ITEM
                  INNER JOIN ITEM_GROUPS AS IG 
                    ON I.ID_ITEM_GROUP = IG.ID_ITEM_GROUP
                WHERE
                  OE.ID_OBJECT = ?
                  AND OE.DELETED = 0
                  AND OE.DATE_CONTROLL is not null";

        if ($eventGroup !='')
            $sql = $sql." AND IG.SYSNAME in ({$eventGroup})";
        if ($dateStart != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) >= STR_TO_DATE('{$dateStart}', '%d.%m.%Y')";
        }

        if ($dateFinish != ''){
            $sql = $sql." AND DATE(OE.DATE_EVENT) <= STR_TO_DATE('{$dateFinish}', '%d.%m.%Y')";
        }

        $sql = $sql."
                  GROUP BY
                    date_format(OE.DATE_EVENT, '%Y-%m')
                  ORDER BY
                    date_format(OE.DATE_EVENT, '%Y-%m')";
        $rows = $this->db->query($sql, array($idObject))->result_array();
        return $rows;
    }
}